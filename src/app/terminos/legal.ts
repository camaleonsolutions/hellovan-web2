export const legal = {
  html: `
      <h5>1. INTRODUCCIÓN Y OBJETO.</h5>
        <p>HelloVan Solutions S.L. es una compañía de nacionalidad española, con domicilio en C/ Chile N.27 CP 21440, Huelva,
          en adelante: “HelloVan”, que actúa en los ámbitos de la prestación de servicios; y es propietaria de varias APPs
          (en adelante, las “APPs” o “aplicaciones”) y plataformas webs (en adelante, la “página web” o el “sitio web”,
          a través del cual pone determinada información a disposición de sus usuarios. Las APPs son accesibles desde
          dispositivos móviles IOS y ANDROID. El sitio web es accesible mediante cualquier sistema informático con acceso a Internet. </p>
        <p>HelloVan desarrolla su servicio de forma independiente a IKEA IBERICA SA o cualquiera de sus empresas relacionadas,
          conformándose como una plataforma colaborativa entre usuarios y clientes de dicha compañía que los ayuda a encontrar
          transportistas profesionales para sus envíos y montajes.</p>
        <p>A través de las APPs y del sitio web, HelloVan facilita a sus usuarios el acceso y la utilización de diversos servicios
          y contenidos (en adelante, los "Servicios") puestos a disposición por HelloVan o por terceros. Los presentes términos
          y condiciones tienen por objeto establecer y regular el acceso general a las APPs y al sitio web, así como a los
          distintos contenidos y servicios prestados por HelloVan o por terceros desde y/o a través de los mismos.</p>
        <p>HelloVan ofrece sus servicios de atención al cliente de forma exclusiva a través de la dirección de correo electrónico
          info@hellovan.es, Whatsapp y redes sociales que se encuentren expresadas y publicitadas en las plataformas. La respuesta
          a los clientes se realizará a la mayor brevedad posible.</p>
        <p>Tanto el acceso como la utilización general de las APPs y del sitio web atribuyen la condición de usuario (en adelante,
          el "Usuario"), implicando necesariamente la aceptación plena y sin reservas de todas y cada una de las disposiciones
          incluidas en los términos y condiciones generales de acceso que aquí se expresan.</p>
        <p>El Usuario queda expresamente apercibido y acepta que estas condiciones podrán experimentar variaciones en el futuro
          a libre criterio de HelloVan y sin obligación alguna de aviso previo por su parte. Será de la sola responsabilidad
          del Usuario verificar los términos y condiciones vigentes en cada una de las ocasiones en que se proponga utilizar las APPs y el sitio web.</p>
        <h5>2. DEFINICIONES.</h5>
        <p>A efectos de los presentes términos y condiciones de uso los siguientes términos tendrán el sentido que se indica a continuación:</p>
        <ul>
          <li>“Condiciones”: referirá a los presentes Términos y Condiciones Generales de uso, incluyendo las Condiciones Particulares que en su caso pudieran resultar de aplicación y las Normas de Buena Conducta que se encuentren vigentes en cada momento.</li>
          <li>"Usuario": referirá a cualquier persona, física o jurídica, que acceda por cualquier medio a las APPs o sitio web.</li>
          <li>"Vanner": referirá a la persona que, utilizando las APPs o sitio web, ofrece sus servicios de envío y montaje a otros usuarios que demandan estos servicios. Los vanners serán considerados a todos los efectos de las presentes condiciones como usuarios.</li>
          <li>“Responsable de tienda”: hará referencia a aquella persona que, trabajando en la tienda o establecimiento en los cuales el usuario realiza la adquisición de sus compras, es la encargada o responsable de las mismas.</li>
          <li>“Tienda” o “establecimiento”: referirá cualquier centro de venta en el que HelloVan desarrolle sus servicios de transporte y montaje.</li>
          <li>"Servicio": se refiere a cualquier servicio ofrecido demandado a través de la página web o aplicaciones.</li>
          <li>“Servicio Express”: se refiere a un servicio específico ofrecido por HelloVan, caracterizado por la
            rapidez en su desarrollo.</li>
          <li>"Cuenta del Usuario": referirá a una cuenta virtual con los datos de registro del usuario con la que este, mediante la utilización de su correo electrónico o nombre de usuario y una palabra clave de acceso, podrá acceder a los servicios ofrecidos por las APPs y por el sitio web.</li>
          <li>“Van”: referirá al vehículo utilizado por un Vanner para realizar el servicio al usuario demandante.</li>
          <li>“Compras”: referirá a los objetos adquiridos por el usuario a un tercero y cuyo servicio de transporte
            requiere mediante el acceso a las APPs y al sitio web.</li>
          <li>“Nombre de usuario”: referirá a aquel conjunto de letras y números expresado por el Usuario al crear su Cuenta de Usuario a efectos de poder ser identificado por el resto de usuarios de las aplicaciones y del sitio web.</li>
          <li>“Palabra clave de acceso”: referirá a aquel conjunto de letras, números o símbolos expresado por el Usuario al crear su Cuenta de Usuario que deberá de escribir para poder acceder a las y al sitio web.</li>
          <li>“web” o “sitio web”: referirá a aquella plataforma web en la cual HelloVan tenga presencia de forma principal. A la fecha de actualización de las presentes condiciones la plataforma referida es www.hellovan.es.</li>
          <li>“APPs” o “aplicaciones”: referirá a aquellos programas informáticos desarrollados por HelloVan en los que implementen sus servicios. A la fecha de actualización de las presentes condiciones las aplicaciones referidas son “Hellovan” y “Vanner”, ambas accesibles desde dispositivos móviles IOS y Android.</li>
          <li>Plataformas”: referirá, de forma conjunta, a las definiciones expresadas en lo anterior como “web”, “sitio web”, “APPs” y “aplicaciones”.</li>

        </ul>

        <h5>3. NIVELES DE ACCESO Y REGISTRO DE USUARIOS. </h5>
        <p>Con carácter general el acceso a las APPs y al sitio web será libre. No obstante, HelloVan condiciona el acceso a determinadas áreas y a la utilización de todos sus servicios a la previa cumplimentación del correspondiente formulario de registro de Usuario, que dará como efecto la creación de la Cuenta del Usuario correspondiente.</p>
        <p>A todos los efectos, el acceso a las APPs a través de identificación de Usuario por medio de redes sociales se considerará registro de Usuario y conllevará necesariamente la aceptación sin reserva u objeción alguna de los presentes términos y condiciones generales.</p>
        <p>HelloVan podrá poner a disposición de los usuarios determinados servicios para cuya utilización podrá requerir la cumplimentación de registros adicionales y/o satisfacción de determinadas contraprestaciones dinerarias. En su caso, dicho registro se efectuará en la forma expresamente indicada en el propio servicio o en las condiciones particulares que lo regulen.</p>
        <p>La condición de usuario de HelloVan requiere mayoría de edad legal. El usuario manifiesta y declara en todo caso contar con al menos 18 años de edad. Si el usuario tiene menos de 18 años necesitará permiso de sus tutores legales para utilizar HelloVan y para aceptar las condiciones presentes.</p>
        <p>Los usuarios no podrán acceder a HelloVan ni utilizar los servicios de las APPs ni del sitio web si, por motivos legales o por otras razones, han sido privados del derecho a recibir o utilizar el servicio en virtud de la legislación del país en el que residan o desde el que accedan a las plataformas. </p>
        <p>HelloVan se reserva el derecho de negar el alta en las APPs o en el sitio web de forma concreta para determinadas personas o grupos de usuarios. Esta negativa no comportará derecho a indemnización alguna.</p>
        <p>Una vez se complete satisfactoriamente el proceso de registro, HelloVan dará de alta al Usuario en su base de datos de Usuarios registrados. La cuenta de usuario será generada con los datos de dirección de correo electrónico, nombre de usuario y palabra clave de acceso que el usuario haya expresado en el momento del registro.</p>
        <p>HelloVan se reserva el derecho a expulsar temporalmente o definitivamente a cualquier usuario por razones que obedezcan a sus políticas de empresa sin derecho a indemnización ni reclamación alguna por parte del usuario, así como a eliminar todas las cuentas de usuario que puedan estar relacionadas con los mismos.</p>

        <h5>4. OBLIGACIONES Y DEBERES DE LOS USUARIOS.</h5>
        <p>Serán obligaciones del Usuario, sin perjuicio de otras que se puedan encontrar en las presentes condiciones, en otras generadas por HelloVan o en la normativa legal correspondiente, las siguientes:</p>
        <ul>
          <li>Aceptar los presentes términos y condiciones en el momento de alta como usuario registrado, sometiéndose a todas y cada una de las obligaciones que en dicho documento se expresan.
          </li>
          <li>Garantizar la veracidad y exactitud de los datos de registro introducidos en los sistemas de información de HelloVan y sus plataformas y llevar a cabo cuantas actualizaciones de los mismos fueran necesarias. En todo caso el usuario será el único responsable de las manifestaciones falsas o inexactas que realice y de los perjuicios que cause a HelloVan o a terceros por la información que facilite, comprometiéndose expresamente a eximir a HelloVan y/o a cualesquiera terceros por los daños y perjuicios que se pudieran derivar de la inexactitud o falta de veracidad de dichos datos.</li>
          <li>Custodiar el nombre de usuario y la palabra clave de acceso facilitados por HelloVan al usuario con diligencia, impidiendo el acceso de terceras personas no autorizados a los mismos y asegurando su seguridad y confidencialidad.</li>
          <li>Notificar a HelloVan con carácter inmediato sobre cualquier indicio de la existencia de una violación en la seguridad de las plataformas, de usos inapropiados o prohibidos de los servicios prestados desde las mismas, o de otros fallos de seguridad de cualquier índole.</li>
          <li>Hacer buen uso de los contenidos, información y servicios prestados desde o a través de las APPs o sitio web, siempre conforme a la ley, la buena fe y a las buenas costumbres generalmente aceptadas.</li>
          <li>Abstenerse de realizar prácticas o usos de los servicios con fines ilícitos, fraudulentos, lesivos de derechos o intereses de HelloVan, de sus usuarios o de terceros.</li>
          <li>Abstenerse de infringir las normas contenidas en las presentes condiciones o en las condiciones particulares que en su caso regulen cualquiera de los servicios prestados desde o a través de las APPs o sitio web.</li>
          <li>Abstenerse de mantener varias cuentas de usuario para una única identidad y abstenerse igualmente de suplantar a ningún otro usuario en las plataformas.</li>
          <li>Abstenerse de crear o mantener cuentas de usuario con datos falsos o ficticios.<></li>
          <li>Proteger y salvaguardar la información personal de otros usuarios, absteniéndose de transmitirla a terceros y de utilizarla con fines distintos a los necesarios para la realización de los servicios ofrecidos en las plataformas.</li>
          <li>Comportarse de forma razonable y educada en el desarrollo de los servicios.</li>
          <li>Abstenerse de entrar en cualquier zona de transporte de los establecimientos donde adquiera las compras relacionadas con los servicios. Cualquier consulta con el responsable de tienda se realizará directamente por teléfono. </li>
          <li>Consentir a que el Vanner pueda realizar labores de revisión en relación a sus compras, a efectos de comprobar el correcto estado tanto en materia de daños como de omisiones de las mismas.</li>
          <li>Observar y cumplir toda la normativa que resulte de aplicación por la realización de los servicios prestados, y en particular con las contenidas en el Código Civil español y en su Código de Circulación y Leyes de Seguridad Vial.</li>
          <li>Abstenerse de realizar cualquier tipo de acción que pueda inutilizar, sobrecargar o dañar los sistemas, equipos o servicios accesibles directa o indirectamente a través de las APPs o sitio web.</li>
          <li>Respetar los derechos de propiedad intelectual e industrial de HelloVan y de terceros sobre los contenidos, información y servicios prestados desde o a través de las APPs o sitio web, absteniéndose de copiar, distribuir, reproducir o comunicar en forma alguna los mismos a terceros, de no mediar autorización expresa y por escrito de HelloVan o de los titulares de dichos derechos.</li>

        </ul>

        <h5>5. OBLIGACIONES Y DEBERES ESPECÍFICOS DE LOS VANNERS.</h5>
        <p>Serán obligaciones y deberes específicos de los Vanners, sin perjuicio de otras que se puedan encontrar en las presentes condiciones, en otras generadas por HelloVan o en la normativa legal correspondiente, las siguientes:</p>
        <ul>
          <li>Aceptar los presentes términos y condiciones en el momento de alta como Vanner registrado, sometiéndose a todas y cada una de las obligaciones que en dicho documento se expresan</li>
          <li>Obtener y disponer de toda la documentación necesaria para desarrollar su actividad, con pleno respeto a la normativa vigente y sin poner en riesgo la seguridad propia y de las compras del usuario.</li>
          <li>Desarrollar la realización de los servicios con la diligencia profesional exigible, cuidando de las compras de los usuarios demandantes y respetando y protegiendo sus intereses</li>
          <li>Comportarse de forma cortés, educada y razonable en el trato con el resto de usuarios y en el desarrollo de los servicios. Los Vanners procurarán tratar al cliente siempre de forma positiva, sin entrar en riñas ni discusiones, haciendo caso omiso de palabras o comentarios con carácter negativo y esforzándose por trasladar una buena imagen de marca al usuario adquirente de sus servicios.</li>
          <li>Trabajar en parejas, manteniendo un número mínimo de dos personas en cada Van, y realizando los servicios siempre de este modo. Bajo ningún concepto se podrá dejar solo a una persona ni se podrán realizar trabajos de forma individual.</li>
          <li>Llamar al usuario solicitante de los servicios en el mismo momento en el que estos queden adquiridos a efectos de concretar con el cliente la hora y sitio de envío o montaje.</li>
          <li>Atender las peticiones de los usuarios en la medida de lo posible y respetar el horario acordado de entrega solicitado por los mismos.</li>
          <li>Tener un trato con el cliente positivo, no reparando en ningún aspecto negativo y poniendo solución a todo siempre con una sonrisa.</li>
          <li>Ir debidamente vestido con el uniforme de trabajo indicado por HelloVan y mantener unos estándares correctos de imagen y aseo en el desarrollo de los servicios. HelloVan podrá dar directrices generales o indicaciones concretas que deberán de ser cumplidas y cuya omisión podrá dar lugar a la expulsión de las plataformas.
          </li>
          <li>No consumir ningún tipo de substancias que pudieran afectar a su capacidad con anterioridad a la realización de los servicios o durante el desarrollo de los mismos.</li>
          <li>Cumplir con los tiempos de entrega, avisando con suficiente antelación a los usuarios demandantes de sus servicios en caso de retrasos de cualquier tipo.</li>
          <li>Entregar el documento de conformidad al usuario al finalizar el servicio a efectos de que este sea firmado y devuelto al Vanner.</li>
          <li>Abstenerse de intercambiar con otros Vanners servicios de usuarios procedentes de HelloVan y adquiridos por sus plataformas. La presente condición podrá ser eximida de cumplimiento en situaciones concretas por consentimiento expreso por parte de HelloVan.</li>
          <li>Abstenerse de aceptar cobros en efectivo o de cualquier otro modo por medios que sean distintos a los de las plataformas, salvo consentimiento expreso y por escrito del responsable de tienda.</li>
          <li>Abstenerse de ofrecer a usuarios de HelloVan los servicios expresados en sus plataformas de forma externa a las mismas.</li>
          <li>Abstenerse de ofrecer a usuarios de HelloVan información privada de contacto a efectos de que los usuarios puedan solicitarle los servicios de forma externa a las plataformas.</li>
          <li>Cumplir escrupulosamente con las normas del Código Civil, del Código de Circulación, de las Leyes de Seguridad Vial y de toda aquella normativa de tráfico y transporte en vigor que resulte de aplicación en aquellos territorios donde se realice el servicio, así como toda la normativa europea, nacional, autonómica y local que sea correspondiente.</li>
          <li>Mantener su Van utilizado para la realización de los servicios correctamente y de acuerdo a las normas del Código Civil, del Código de Circulación, de las Leyes de Seguridad Vial y de toda aquella normativa de tráfico y transporte en vigor que resulte de aplicación en aquellos territorios donde se realice el servicio, así como toda la normativa europea, nacional, autonómica y local que sea correspondiente.</li>
          <li>En caso de recogida de muebles usados, depositar los mismos siempre en aquellos puntos considerados como “Puntos Limpios” según la normativa autonómica o local que corresponda.</li>
          <li>Abstenerse de entregar o transportar ninguna sustancia u objeto peligroso o ilegal</li>

        </ul>


        <h5>6. CÓDIGO DE BUENA CONDUCTA.</h5>

        <p>Abstenerse de entregar o transportar ninguna sustancia u objeto peligroso o ilegal</p>
        <p> HelloVan proporciona a los usuarios las siguientes cuentas de correo en las que los mismos, mediante identificación de su nombre de usuario y email de registro, podrán informarnos de cualquier infracción del presente código de buena conducta o de otras estipulaciones de las presentes condiciones que puedan apreciar con respecto al resto de usuarios.</p>
        <p>Para reclamaciones de usuarios en relación al incumplimiento del presente código de conducta por parte de Vanners: info@hellovan.es.
        </p>
        <p>Para reclamaciones de Vanners en relación al incumplimiento del presente código de conducta por parte de usuarios adquirentes de sus servicios o de otros Vanners: vanners@hellovan.es</p>
        <p>La utilización de los servicios referidos obliga de forma automática a los usuarios y a los Vanners a la completa aceptación sin reserva alguna del presente CÓDIGO DE BUENA CONDUCTA, que contiene las siguientes obligaciones recíprocas.
        </p>
        <p>Desarrollar el servicio de forma diligente y bajo las condiciones acordadas entre las propias partes.</p>
        <p>Atender las peticiones de la otra parte en la medida en que sea posible y siempre y cuando estas no sean exageradas ni excesivas.
        </p>
        <p>Mantener a la otra parte debidamente informada en el caso de cualquier inconveniente o noticia que pueda ser relevante para el desarrollo de los servicios.</p>
        <p>Revisar las compras adecuadamente en el momento en el que las reciba en el establecimiento de venta, poniendo especial atención a cualquier daño o pérdida que pueda afectar a las mismas o a cualquiera de sus componentes. El Vanner será directamente responsable de cualquier daño u omisión que se produzca después de realizar esta revisión.</p>
        <p>Comportarse de forma cortés, educada y razonable en el trato en las conversaciones con la otra parte y en el resto del desarrollo de los servicios.</p>
        <p>No engañar ni proporcionar información falseada de ningún tipo.</p>
        <p>Presentar de inmediato su documentación correspondiente en el caso de que se lo solicite la contraparte, así como toda la necesaria para ser correctamente identificados. Para el Vanner, esta documentación consistirá en la propia del vehículo de servicios, del certificado de seguro, certificado de autónomo y tarjeta de transporte, o en su caso, del billete de transporte público en el que realizará el trayecto. Para un usuario, la documentación consistirá de su justificante de compra.</p>
        <p>No pactar ni acordar la realización o desarrollo de los mismos o futuros servicios de forma externa a las plataformas proporcionadas por HelloVan.</p>
        <p>No transmitir por la aplicación de Chat de HelloVan ni por ningún otro medio información identificativa de cualquier tipo a efectos de contactar fuera de las plataformas. Se prohíbe de modo expreso toda transmisión de números privados de teléfonos, direcciones de correos electrónicos o direcciones postales que se realice a estos efectos.</p>
        <p>Disponer de un seguro de responsabilidad civil a efectos de cubrir los posibles daños o pérdidas en las compras por un valor mínimo de seis mil euros (30.000€).</p>
        <p>Cumplir la legalidad vigente en el territorio de realización de los servicios, y en especial para el caso español toda la normativa local, autonómica, estatal y europea que pueda estar relacionada con el desarrollo de los mismos.</p>


        <h5>7. DESARROLLO DEL SERVICIO</h5>
        <p>El usuario que se encuentre interesado en la adquisición de los servicios de un Vanner deberá, a través de cualquiera de las plataformas ofrecidas por HelloVan, seguir los pasos de contratación y abonar el precio que le sea presupuestado.</p>
        <p>Salvo acuerdo expreso en contrario y adquisición por el usuario mediante cualquiera de las plataformas, el servicio a desarrollar por el Vanner será el de transporte de las compras y posterior montaje de las mismas en el domicilio del usuario adquirente. Servicios añadidos como los de devolución de las compras o entrega de bienes usados en puntos limpios deberán de ser adquiridos y abonados por el usuario a través de las plataformas para poder ser disfrutados.</p>
        <p>La elección del Vanner será realizada por el usuario de entre las posibilidades que le ofrecerá la plataforma, siendo el único responsable de la buena o mala elección del mismo. A efectos de obtener una mejor experiencia de usuario, HelloVan ofrece mecanismos de valoraciones y publicación de comentarios que ayudan a la elección del Vanner pero que en ningún caso pueden ser considerados como una recomendación a seguir de forma obligatoria.</p>
        <p>HelloVan proporcionará a los usuarios distintos medios de pago de entre varios posibles, pudiendo el usuario elegir de entre ellos el que sea de su interés. Las posibles comisiones u otros gastos financieros que terceros repercutan sobre el usuario por la realización del pago serán de cuenta exclusiva del mismo, no pudiendo reclamar ninguna cantidad en este concepto a HelloVan ni al Vanner.</p>
        <p>Desde el momento en el que el usuario realice el pago del servicio habrá adquirido el mismo, estando sujeto a la política de devoluciones que se puede encontrar en el punto 10 de las presentes condiciones.</p>
        <p>Comunicado el pago, el Vanner desarrollará a la mayor brevedad posible el servicio atendiendo a las obligaciones que se encuentran en las presentes condiciones y poniendo especial cuidado de respetar el Código de Buena Conducta referido en el punto anterior.</p>
        <p>En el caso del servicio Express, el Vanner tendrá obligación de avisar con antelación suficiente al responsable de tienda a efectos de informar sobre todas las condiciones que sean necesarias para
          desarrollar el servicio con la suficiente rapidez. Dichos avisos se realizarán tanto al llegar al establecimiento como al salir del mismo.</p>
        <p>Solo podrán realizar el servicio Express aquellos Vanners que se encuentren en cualquiera de los parkings de establecimientos homologados por HelloVan junto a su compañero, su Van y en disposición de entregar las compras a la mayor brevedad posible. HelloVan dará directrices a los Vanners bajo petición expresa de los mismos de aquellos parkings que tendrán validez suficiente a estos efectos.</p>
        <p>Realizado el servicio, el Vanner mostrará al usuario las compras de forma previa a transmitirlas y expedirá el documento de conformidad a efectos de que pueda ser firmado. Será requisito obligatorio para la entrega de las compras la firma del documento de conformidad de forma válida y veraz.</p>
        <p>El usuario tendrá derecho, bajo petición expresa dirigida al Vanner, a comprobar el buen estado de las compras durante el periodo de un minuto, tras lo cual deberá de decidir entre firmar el documento de conformidad o no hacerlo y no recibir las compras realizadas. El cliente será el único responsable de revisar las compras y de asegurarse que están en buen estado y sin errores, fallos o faltas en las piezas.</p>
        <p>El documento de conformidad correctamente firmado servirá como justificante y prueba de la correcta entrega de las compras y del consentimiento y aceptación del usuario con el buen hacer del servicio. En ningún caso podrá el usuario podrá reclamar al Vanner el incorrecto desarrollo al servicio de forma posterior a la firma del documento de conformidad.
        </p>
        <p>En caso de que se observen daños, errores o pérdidas en las compras o en algunos de los componentes de forma posterior a la adquisición del servicio de HelloVan los efectos serán los siguientes:</p>
        <ul>
          <li>Si los daños, errores o pérdidas vienen causados por la manipulación del Vanner o por cualquier otro error imputable al mismo HelloVan abrirá una incidencia a efectos de investigar y resolver el conflicto, reservándose todas las acciones sancionatorias y judiciales que puedan corresponder para cada uno de los usuarios.</li>
          <li>Si los daños, errores o pérdidas vienen causados por defectos de fábrica u otros originarios que nada tengan que ver con el servicio desarrollado por Vanners, el usuario afectado será el único responsable de realizar las reclamaciones que estime conveniente. En este sentido, HelloVan promoverá una política de ayuda y soporte al usuario a estos efectos, sin que ello pueda conllevar asunción alguna de culpa por los daños originados.</li>
        </ul>
        <p>El servicio se considerará como finalizado a todos los efectos una vez el usuario firme el documento de conformidad y el Vanner, siempre posteriormente, entregue las compras a este.</p>
        <h5>8.TRANSMISIÓN DE INFORMACIÓN PERSONAL ENTRE USUARIOS. </h5>
        <p>Los usuarios y Vanners se comprometen a facilitar la información personal que sea necesaria a únicos efectos de desarrollar el servicio. Esta información personal, que podrá abarcará datos de contacto ordinarios como el domicilio, teléfono o dirección de correo electrónico, podrá ser utilizado única y estrictamente para facilitar la realización del servicio por ambas partes.</p>
        <p>Tanto los Vanners como los demás usuarios se comprometen a proteger y salvaguardar la información personal de otros usuarios, absteniéndose de transmitirla a terceros y de utilizarla con fines distintos a los necesarios para la realización de los servicios ofrecidos en las plataformas. Los Vanners y demás usuarios serán únicos responsables de cualquier vulneración particular de la legalidad vigente en materia de transmisión o protección de datos personales.</p>

        <h5>9.INFORMACIÓN PUBLICADA EN LAS PLATAFORMAS EN RELACIÓN AL DESARROLLO DE LOS SERVICIOS.</h5>
        <p>El servicio de valoración de HelloVan será utilizado por las plataformas a fin de mejorar la experiencia de uso del usuario de las aplicaciones y del sitio web. HelloVan anima a sus usuarios a publicar sus comentarios y/o valoraciones sobre los Vanners con los que hayan tenido alguna relación de servicio por medio de las plataformas. Sin embargo, no está permitido que los Vanners comenten o publiquen
          valoraciones entre sí, ni que los usuarios comenten o valoren a Vanners con los que no hayan realizado ningún servicio. Todas las opiniones se podrán ver y se publicarán en las plataformas de forma inmediata, una vez quede comprobado que cumplen las presentes condiciones y las políticas de empresa propias de HelloVan. Mediante la aceptación de las presentes condiciones todos los usuarios se obligan a lo siguiente en relación a dicho servicio de valoración:</p>
        <ul>
          <li>Utilizar los servicios de valoración de forma correcta y adecuada, respetando la imagen y honor del resto de usuarios y comportándose de modo diligente y con buena fe en la publicación de los comentarios.</li>
          <li>Abstenerse de publicar ningún tipo de difamación o información ofensiva que dañe o que pueda dañar a terceros. HelloVan eliminará a su solo criterio cualquier información que sea contraria a esta política tan pronto como tenga conocimiento de la existencia de contenidos que resulten infractores de esta norma.</li>
          <li>Abstenerse de publicar ningún tipo de información falsa o falseada en relación al desarrollo de los servicios o al comportamiento de cualquier otro usuario.</li>
          <li>Abstenerse de publicar información privada de cualquiera de los usuarios. Se entiende por información privada toda aquella que no sea pública y visible desde el propio perfil de cada usuario.</li>
          <li>Aceptar que el resto de usuarios podrán emitir valoraciones sobre ellos y consentir a que dichas valoraciones, sean positivas o negativas, serán publicadas en las plataformas.</li>
          <li>Aceptar a que su nivel de experiencia será calculado, establecido y/o publicado en el sitio web o en las APPs de acuerdo a los criterios que de forma autónoma decida HelloVan.</li>
          <li>Consentir a que sus comentarios y valoraciones puedan ser denegados y no publicados por HelloVan en todos aquellos casos en los que por cualquier razón autónoma se decida.</li>
          <li>Aceptar que todas las valoraciones, niveles de experiencia o reputación que se extraigan por HelloVan puedan ser compartidas con terceros.</li>
          <li>HelloVan se reserva el derecho a eliminar cualquier información o publicación que considere oportuna por infracción de la normativa legal o de sus políticas de empresa. Del mismo modo, HelloVan se reserva el derecho a expulsar de las plataformas de modo temporal o definitivo a todos aquellos Vanners y demás usuarios que tengan una valoración negativa por parte de los restantes. En todo caso se entenderá como valoración negativa aquella que sea inferior al dos con cincuenta en una escala de cinco (5,00)puntos.
          </li>

        </ul>

        <h5>10.EXCLUSIONES DE RESPONSABILIDAD.</h5>
        <p>La concertación de acuerdos entre Usuarios de las APPs para ofrecer o demandar servicios es el resultado de acuerdos libres previos celebrados entre personas mayores de edad (Vanners y usuarios), que actúan bajo su plena y sola responsabilidad. HelloVan mediará para resolver cualquier tipo de consecuencias derivadas de los acuerdos celebrados entre los mismos, pero nunca responderá por cualquier perjuicio causado tanto a usuarios como a Vanners por el mal desarrollo de los servicios.</p>
        <p>De forma especial, HelloVan se excluye de forma expresa e irrechazable de toda responsabilidad por las causas que a continuación se exponen. Los Vanners y demás usuarios consienten de manera
          expresa con la aceptación de las presentes Condiciones a que dichas exclusiones sean completamente efectivas, y se comprometen a no reclamar a HelloVan ningún tipo de responsabilidad de forma extrajudicial ni judicial en estos casos</p>
        <ul>
          <li>HelloVan no se hace responsable del mal desarrollo de los servicios por parte de los Vanners ni del resto de los usuarios. La función de HelloVan es la de puesta en contacto de terceros profesionales (Vanners) con otros usuarios que puedan requerir la utilización de sus servicios, sin aceptación de responsabilidad alguna por cualquier daño o perjuicio que pueda derivarse de esta relación contractual. En ningún caso HelloVan actuará como juez o parte en cualquier procedimiento extrajudicial o judicial relacionado con la reclamación de responsabilidades por la relación contractual entre Vanner y usuario.</li>
          <li>HelloVan no se hace responsable de los daños o perjuicios a las compras de cualquier usuario. Todas las obligaciones de diligencia profesional de los Vanners son estrictamente individuales y propias según se desprende de las presentes Condiciones, y podrán ser reclamadas de forma extrajudicial o judicial solo ante los mismos.</li>
          <li>HelloVan no se hace responsable de los daños o perjuicios personales que puedan ser causados a cualquier usuario, Vanner u otro tercero en el desarrollo de los servicios.</li>
          <li>HelloVan no puede garantizar ni garantiza el correcto desarrollo de los servicios por parte de los Vanners. Toda reclamación por el indebido desarrollo de estos servicios (incluyéndose daños, retrasos, errores y demás defectos en la realización de cualquier tipo) deberán de ser dirigidos de forma exclusiva e individual al Vanner causante de los mismos.</li>
          <li>HelloVan no se hace responsable de los comentarios y valoraciones publicados en las plataformas. En caso de apreciarse delitos, insultos o difamaciones HelloVan procurará la eliminación de dichos textos, pero en ningún caso la no eliminación de los mismos podrá considerarse como una asunción de responsabilidad en este sentido.</li>
          <li>HelloVan no se hace responsable de los daños y perjuicios que puedan ser causados a los sistemas informáticos de acceso a las plataformas por el mal uso de los mismos.</li>
          <li>HelloVan no se hace responsable de cualquier daño o perjuicio causado a cualquier usuario que pueda derivar de la realización del mismo de conductas delictivas o consideradas ilegales en base a la normativa legal del Reino de España.</li>
          <li>HelloVan no responderá en ningún caso y en ninguna medida, ni por daños directos ni indirectos, ni por daño emergente ni por lucro cesante, por los eventuales perjuicios derivados del uso de la información y contenidos de las plataformas o accesibles desde o a través de las mismas.</li>
          <li>HelloVan excluye, con toda la extensión permitida por la Ley, cualquier responsabilidad de cualquier clase por fallos de seguridad en las plataformas y las consecuencias que de ellos pudieran derivarse.</li>
          <li>HelloVan excluye, con toda la extensión permitida por la Ley, cualquier responsabilidad de cualquier clase derivada de la interrupción temporal en la disponibilidad de las aplicaciones o del sitio web.</li>
        </ul>
        <h5>11. POLÍTICA DE DEVOLUCIONES.</h5>
        <p>En su condición de intermediario, HelloVan a través de sus plataformas proporciona a los usuarios
          mecanismos para la adquisición sencilla y efectiva de los servicios de los Vanners, de entre los cuales se encuentra la obtención de las cantidades económicas que procedan por parte de los usuarios para el posterior pago de dichos servicios.</p>
        <p>En dicha condición de intermediario, y con fundamento en la mejor calidad de los servicios proporcionados por los Vanners y demás usuarios en cada una de las plataformas, HelloVan proporciona la siguiente política de devoluciones.</p>
        <p>HelloVan devolverá al usuario que adquiera los servicios y que lo solicite el importe que corresponda en caso de que el Vanner desarrolle los mismos de manera defectuosa (entrega tardía, pérdidas de compras, daños en las compras, etcétera).
        </p>
        <p>HelloVan devolverá al usuario que adquiera los servicios y que lo solicite el importe en su totalidad en caso de que este no sea finalmente realizado o preparado en el momento en que sea requerido.
        </p>
        <p>HelloVan nunca devolverá las cantidades que sean abonadas en relación a servicios correctamente efectuados.</p>
        <p>Para realizar una solicitud de devolución el usuario deberá de escribir a la dirección de correo electrónico info@hellovan.es con su nombre de usuario, correo electrónico y justificante de pago. HelloVan abrirá un expediente de devolución en el cual evaluará las condiciones del servicio realizado y si se cumplen los requisitos para proceder a la devolución. En todo caso HelloVan será el único decisor y tendrá la última palabra sobre la resolución de cada uno de los expedientes de devolución.</p>
        <p>En ningún caso se llevarán a cabo devoluciones sobre servicios cuyo documento de conformidad haya sido debidamente firmado por el adquirente de los mismos.</p>

        <h5>12. CANCELACIONES</h5>
        <p>El usuario podrá cancelar otros servicios tales como mudanzas y servicios de entregas de mercancía.</p>
        <p>Las cancelaciones se deberán efectuar con una antelación mínima de 24 horas respecto al inicio del servicio si se quiere recibir el 100% del pago.</p>
        <p>En caso que se cancele el servicio en un plazo menor de 24 horas sólo se devolverá el 50% del importe.</p>

        <h5>13. SEGURO DE RESPONSABILIDAD CIVIL PARA LAS COMPRAS DE LOS USUARIOS EN CASO DE ACCIDENTES DE TRÁFICO.</h5>
        <p>En el desarrollo de su actividad como intermediario, HelloVan dispone de un seguro de responsabilidad civil que cubre en caso de accidente de tráfico los daños causados en las compras de los usuarios hasta la cantidad de seis mil euros (6.000€) por servicio de transporte adquirido.</p>
        <p>HelloVan se compromete a proporcionar, bajo petición expresa y por escrito de los usuarios a la dirección de correo electrónico info@hellovan.es con su nombre de usuario, correo electrónico y justificante de pago, el seguro de responsabilidad civil por el que podrán reclamar los daños en sus compras.</p>
        <p>El seguro de responsabilidad civil y por tanto HelloVan cubrirán únicamente los daños causados en las

          compras de los usuarios como consecuencia de accidentes de tráfico, no abarcando nunca daños personales, morales o patrimoniales de cualquier otro tipo, ni aquellos que afecten a las compras pero no deriven directamente de accidentes de tráfico. Los daños que serán cubiertos de forma exclusiva serán aquellos que, cumpliendo el resto de requisitos de la presente cláusula, afecten directamente a compras de bienes nuevos que sean presentadas con ticket de compra y que hayan sido debidamente embaladas. Se excluyen en todo caso bienes usados de cualquier tipo y los cristales y materiales de fragilidad semejante tanto de estos como de los nuevos.</p>
        <p>En ningún caso el seguro de responsabilidad civil ni HelloVan cubrirán daños o extravíos en las compras causados por robos, pérdidas, incendios, inundaciones o humedades, golpes que no deriven directamente de accidentes de tráfico, explosiones, fenómenos naturales ni cualquier daño causado por los propios Vanners o terceros que no derive directamente de accidentes de tráfico.</p>
        <p>En ningún caso el seguro de responsabilidad civil ni HelloVan cubrirán daños personales a Vanners, usuarios u otros terceros.
        </p>
        <h5>14.  PROPIEDAD INTELECTUAL E INDUSTRIAL.</h5>
        <p>Todo el software y los contenidos de las APPs y sitio web, incluidos los códigos de desarollo, textos, animaciones, imágenes, diseños, documentos que se encuentren relacionados con aquellos, así como las marcas, nombres comerciales y/o signos distintivos mostrados en los mismos son propiedad de HelloVan o de terceros y están protegidos por las leyes nacionales e internacionales de propiedad intelectual e industrial.</p>
        <p>Cualquier uso ajeno a las APPs, incluida la reproducción, modificación, distribución, transmisión, reedición, arreglos o representación de cualesquiera elementos de las mismas está estrictamente prohibido salvo consentimiento expreso por escrito de HelloVan.</p>
        <h5>15. EXCLUSIÓN DE GARANTÍAS.</h5>
        <p>El acceso al sitio web o a las APPs no implica ningún tipo de garantía hacia el usuario, quedando la misma expresamente rehusada respecto a la calidad, veracidad, exactitud, licitud, actualidad o vigencia del sitio web o de las APPs, así como la utilidad o adecuación a finalidad alguna de quien acceda a ellas. Tanto el acceso al sitio web o a las APPs como el uso que pueda hacerse de la información y contenidos incluidos en los mismos o que sean accesibles desde los mismos, se efectúan bajo la exclusiva responsabilidad del Usuario.</p>
        <p>Hellovan NO garantiza en modo alguno la calidad, veracidad, exactitud, licitud, actualidad o vigencia, así como la utilidad o adecuación a finalidad alguna de los contenidos que integran el sitio web y las APPs. HelloVan excluye, con toda la extensión permitida por la Ley, cualquier responsabilidad de cualquier clase por la calidad, veracidad, exactitud, licitud, actualidad, vigencia o utilidad/adecuación a finalidad alguna que el Usuario pudiera haber atribuido a los contenidos que integran el sitio web y las APPs.</p>

        <h5>16. SEGURIDAD INFORMÁTICA Y COOKIES.</h5>
        <p>HelloVan empleará razonablemente los medios a su alcance para proveer de sistemas de seguridad que protejan de forma razonable sus sistemas y los datos contenidos en los mismos contra ataques deliberados, software malignos, etc. No obstante, el Usuario entiende y acepta los aspectos que comporta la prestación de servicios a través de Internet dado el carácter abierto, descentralizado y
          global de esta red de comunicaciones. Por este motivo, HelloVan NO garantiza la inexpugnabilidad de sus sistemas de seguridad ni la privacidad de la información alojada en los mismos.</p>
        <p>Las plataformas de HelloVan utiliza cookies a efectos de mejorar la experiencia del usuario y la calidad de los servicios ofrecidos. Dichas cookies, que pueden encontrarse tanto en el sitio web como en las aplicaciones, se instalan en los dispositivos de los usuarios a fin de poder personalizar y mejorar la calidad de las plataformas. De forma concreta, HelloVan podrá utilizar cualquiera de estas cookies:</p>
        <ul>
          <li>Cookies técnicas: Son aquéllas que permiten al usuario la navegación a través de una página web, plataforma o aplicación y la utilización de las diferentes opciones o servicios que en ella existan como, por ejemplo, controlar el tráfico y la comunicación de datos, identificar la sesión, acceder a partes de acceso restringido, recordar los elementos que integran un pedido, realizar el proceso de compra de un pedido, realizar la solicitud de inscripción o participación en un evento, utilizar elementos de seguridad durante la navegación, almacenar contenidos para la difusión de videos o sonido o compartir contenidos a través de redes sociales.</li>
          <li>Cookies de personalización: Son aquéllas que permiten al usuario acceder al servicio con algunas características de carácter general predefinidas en función de una serie de criterios en el terminal del usuario como por ejemplo serian el idioma, el tipo de navegador a través del cual accede al servicio, la configuración regional desde donde accede al servicio, etc.</li>
          <li>Cookies de análisis: Son aquéllas que bien tratadas por nosotros o por terceros, nos permiten cuantificar el número de usuarios y así realizar la medición y análisis estadístico de la utilización que hacen los usuarios del servicio ofertado. Para ello se analiza su navegación en nuestra página web con el fin de mejorar la oferta de productos o servicios que le ofrecemos.</li>
          <li>Cookies publicitarias: Son aquéllas que, bien tratadas por nosotros o por terceros, nos permiten gestionar de la forma más eficaz posible la oferta de los espacios publicitarios que hay en la página web, adecuando el contenido del anuncio al contenido del servicio solicitado o al uso que realice de nuestra página web. Para ello podemos analizar sus hábitos de navegación en Internet y podemos mostrarle publicidad relacionada con su perfil de navegación.
          </li>
          <li>Cookies de publicidad comportamental: Son aquéllas que permiten la gestión, de la forma más eficaz posible, de los espacios publicitarios que, en su caso, el editor haya incluido en una página web, aplicación o plataforma desde la que presta el servicio solicitado. Estas cookies almacenan información del comportamiento de los usuarios obtenida a través de la observación continuada de sus hábitos de navegación, lo que permite desarrollar un perfil específico para mostrar publicidad en función del mismo.</li>
          <li>Cookies de terceros: Son aquellas que recopilaran información con fines estadísticos, de uso del sitio web y de las aplicaciones por parte del usuario y para la prestación de otros servicios relacionados con la actividad de las plataformas.</li>
        </ul>
        <p>En particular, este sitio Web utiliza Google Analytics, un servicio analítico de web prestado por Google, Inc. con domicilio en los Estados Unidos con sede central en 1600 Amphitheatre Parkway, Mountain View, California 94043. Para la prestación de estos servicios, estos utilizan cookies que recopilan la información, incluida la dirección IP del usuario, que será transmitida, tratada y almacenada por Google en los términos fijados en la Web Google.com. Incluyendo la posible transmisión de dicha información a terceros por razones de exigencia legal o cuando dichos terceros procesen la información por cuenta de Google.
        </p>

        <h5>17.INTERRUPCIONES Y SUSPENSIONES DE LAS PLATAFORMAS.</h5>
        <p>Con carácter general las aplicaciones y el sitio web estarán disponibles ininterrumpidamente en Internet. Sin embargo, el Usuario queda apercibido de que HelloVan NO garantiza en modo alguno esta continuidad debido a la naturaleza del medio a través del que se prestan.</p>
        <p>Así por ejemplo, a título enunciativo no exhaustivo, a continuación se recogen algunas de las situaciones que pueden interrumpir temporalmente la prestación de dichos servicios:</p>
        <ul>
          <li>Tareas de mantenimiento en los servidores y/o líneas de datos</li>
          <li>Averías en los servidores y/o líneas de datos</li>
          <li>Ataques deliberados contra la seguridad e integridad del sistema</li>
        </ul>
        <p>HelloVan excluye, con toda la extensión permitida por la Ley, toda responsabilidad de cualquier clase derivada de la interrupción temporal en la disponibilidad de las aplicaciones o del sitio</p>
        <p>El usuario queda apercibido de que HelloVan podrá en cualquier momento, a su solo criterio y sin obligación alguna de aviso previo, suspender con carácter definitivo su presencia en la red de Internet y por tanto de los servicios prestados desde o a través de las plataformas.</p>
        <p>HelloVan queda eximida de cualquier tipo de responsabilidad de cualquier clase derivada de las consecuencias que pudieran producirse por la suspensión definitiva o temporal en la prestación de dichos servicios
        </p>

        <h5>18. BAJA VOLUNTARIA DE LOS USUARIOS.</h5>
        <p>Cualquier Usuario podrá solicitar la baja, en cualquier momento, como usuario de las APPs o del sitio web. HelloVan dispondrá de un plazo de 30 días para examinar la situación de dicho Usuario para, en su caso, emitir factura por todos aquellos consumos que a la recepción de la petición de baja se hallaren pendientes de cobro.</p>
        <p>De no existir importe alguno pendiente de liquidación y/o cobro, la baja se llevará a efecto con carácter inmediato. En caso contrario, la baja quedará supeditada al cobro efectivo de dichos importes por parte de HelloVan.
        </p>

        <h5>19. EFECTOS DEL INCUMPLIMIENTO DE LAS OBLIGACIONES.</h5>
        <p>Mediante la aceptación de las presentes condiciones todos los usuarios se comprometen a respetarlas, defenderlas y cumplirlas mientras se comporten como usuarios de cualquiera de las plataformas proporcionadas por HelloVan. El no cumplimiento de cualquiera de las obligaciones referidas en las presentes Condiciones podrá dar lugar, de forma indistinta y acumulada, a los siguientes efectos:</p>
        <ul>
          <li>Suspensión temporal o definitiva de la cuenta de usuario y del acceso a cualquiera de las plataformas ofrecidas por HelloVan.</li>
          <li>Pérdida temporal o definitiva de la condición de usuario en relación a cualquiera de las plataformas ofrecidas por HelloVan.</li>
          <li>Retenciones de pagos que hubieran podido ser realizados por los usuarios en relación a cualquiera de los servicios ofrecidos en las plataformas.</li>
          <li>Reclamaciones judiciales por indemnizaciones que puedan corresponder ante los tribunales competentes por cualquier razón que sea suficiente en base a derecho.</li>
          <li>Denuncias de carácter administrativo, laboral o penal o puesta en disposición de las mismas ante las autoridades competentes por cualquier razón que sea suficiente en base a derecho.</li>
          <li>Transmisión de información de carácter privado ante las autoridades que puedan requerirlo por fundamentos en la comisión de delitos u otras infracciones de carácter administrativo, laboral o penal.</li>

        </ul>
        <p>El Usuario será el único responsable frente a cualquier reclamación, acción legal, judicial o extrajudicial, iniciada por terceras personas tanto contra otro Usuario como contra HelloVan, basada en usos de los servicios contrarios a la ley, la moral, la buena fe y a las buenas costumbres generalmente aceptadas.</p>
        <h5>20. POLÍTICA DE PRIVACIDAD.</h5>
        <p>En cumplimiento de lo establecido en la Ley Orgánica 15/1999, de 13 de diciembre, de Protección de Datos de Carácter Personal, los usuarios aceptan que, mediante la realización del registro de su cuenta de usuario en cualquiera de las plataformas de HelloVan, sus datos personales quedarán incorporados y serán tratados en un fichero automatizado con el único fin de mejorar su experiencia de usuario.
        </p>
        <p>Con la aceptación de las presentes condiciones el usuario consiente de manera expresa a permitir a HelloVan utilizar sus datos personales con fines publicitarios, pudiendo ser utilizados y publicados a la propia conveniencia de la empresa en sus campañas de email marketing, redes sociales, prioridad orgánica y de pago en buscadores de internet, y en cualesquiera otras que considere oportuno.</p>
        <p>Exceptuando lo anterior, HelloVan se compromete a tratar de forma confidencial los datos de carácter personal facilitados y a no comunicar o ceder dicha información a terceros salvo en aquellos casos en los que sea requerido por las autoridades judiciales competentes.</p>
        <p>Todos los usuarios tienen posibilidad de ejercer los derechos de acceso, rectificación, cancelación y oposición de sus datos de carácter personal mediante comunicación por vía postal o telemática a la dirección info@hellovan.es con demostración suficiente de su identificación.</p>
        <h5>21. NOTIFICACIONES.</h5>
        <p>A los efectos de practicar las oportunas notificaciones, HelloVan designa como domicilio de contacto el especificado como propio en el encabezamiento de los presentes términos y condiciones.</p>
        <p>El correo electrónico facilitado por el Usuario durante el proceso de registro en el sitio web y en las APPs será el utilizado por HelloVan a efectos de practicar notificaciones al Usuario. A discreción de HelloVan también podrá ser utilizado el domicilio físico facilitado por el Usuario.</p>
        <p>El Usuario está obligado a mantener debidamente actualizados los datos que a efectos de notificaciones se referencian en la presente cláusula.</p>
        <p>Todas las Notificaciones que efectúe HelloVan al Usuario se considerarán válidamente efectuadas si se han realizado empleando los datos y a través de los medios anteriormente señalados. HelloVan no se responsabiliza de cualquier perjuicio que pudiere producirse por la vulneración del Usuario de su obligación de mantenimiento actualizado de sus datos de contacto.</p>

        <h5>22. OTRAS DISPOSICIONES.</h5>
        <p>La declaración de nulidad o invalidez de cualquier estipulación contenida en los presentes términos y condiciones no afectará a la validez y eficacia de las demás cláusulas.</p>
        <p>Los presentes términos y condiciones de los servicios prestados por HelloVan a través de las APPs o sitio web constituyen el acuerdo total entre las partes, considerándose de rango superior a cualquier acuerdo previo escrito o verbal, entendimiento, afirmación, representación, negociación o propósito de acuerdo en relación con esta materia.</p>
        <p>Los presentes términos y condiciones se regirán e interpretarán en todos y cada uno de sus extremos por la Ley Española.</p>
        <p>Tanto HelloVan como el Usuario se comprometen a intentar resolver de manera amistosa cualquier desacuerdo que pudiera surgir en el desarrollo de las relaciones mercantiles que les son propias. Para la solución de cuantas cuestiones litigiosas pudieran derivarse del desarrollo de las relaciones mercantiles que les son propias, ambas partes acuerdan someterse a la jurisdicción de los Juzgados y Tribunales de la ciudad de Huelva (España), a salvo -en su caso- sumisión legal obligatoria a fuero de parte.</p>
        <p>Las presentes condiciones serán vigentes y efectivas mientras se encuentren publicadas en las plataformas de HelloVan, pudiendo ser en cualquier momento modificadas o eliminadas junto a los efectos que de ellas derivan.</p>
        <p>Vigente desde el 1 de OCTUBRE de 2.017</p>
      `
}
