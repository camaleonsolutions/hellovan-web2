import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';
import {NgForm} from '@angular/forms';
import {AuthService} from '../services/auth/auth.service';
import {TranslateService} from '@ngx-translate/core';
import {GlobalService} from '../services/shared/global.service';
declare function init_login();
declare function initCustom();
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['login.component.css']
})
export class LoginComponent implements OnInit {

  recuerdame: boolean;
  email: string;
  language: string;
  constructor( public router: Router,
               public authService: AuthService,
               public translateService: TranslateService,
               public globalService: GlobalService) {
    this.recuerdame = false;

  }

  setLanguage() {
    if (localStorage.getItem('language')) {
      this.globalService.language = localStorage.getItem('language');
    } else {
      this.globalService.language = this.translateService.getBrowserLang();
      localStorage.setItem('language', this.globalService.language);
    }
    this.translateService.setDefaultLang(this.globalService.language);
  }

  ngOnInit() {
    initCustom();
    init_login();

    this.email = localStorage.getItem('email') || '';
    if ( this.email.length > 1) {
      this.recuerdame = true;
    }
  }

  loginWithEmail( forma: NgForm) {
    console.log(forma.valid);
    console.log(forma.value);

    if (forma.invalid) {
      return;
    }

    this.authService.loginWithEmail(forma.value.email, forma.value.password, forma.value.recuerdame).then( (res) => {
      console.log(res);
      this.router.navigate(['/dashboard']);
    }).catch( (error) => {
      console.log(error);
    });
  }

  loginWithFacebook () {
    this.authService.loginWithFacebook().then( (user) => {
      console.log('User');
    }).catch( (error) => {

    });
  }

  reset(forma: NgForm) {
    console.log(forma.value.email);
  }

}

