import {Component, OnInit, ViewChild} from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {SwalComponent} from '@toverux/ngx-sweetalert2';
import {AuthService} from '../services/service.index';
import {Usuario} from '../interfaces/usuario.interface';
import {UsuarioService} from '../services/service.index';
import {Router} from '@angular/router';
import {legal} from '../terminos/legal';
import {TranslateService} from '@ngx-translate/core';
import {GlobalService} from '../services/shared/global.service';
declare function initCustom();

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['../login/login.component.css']
})
export class RegisterComponent implements OnInit {
  @ViewChild('condicionesSwal') private condicionesSwal: SwalComponent;
  @ViewChild('errorSwal') private errorSwal: SwalComponent;
  @ViewChild('successSwal') private successSwal: SwalComponent;
  errorMessage = '';
  forma: FormGroup;
  errorPasswordLength: boolean;

  legal: string;

  constructor( private authService: AuthService,
               public _usuarioService: UsuarioService,
               public router: Router,
               public translateService: TranslateService,
               public globalService: GlobalService) {
  }

  sonIguales(campo1: string, campo2: string) {
    return (group: FormGroup) => {
      const pass1 = group.controls[campo1].value;
      const pass2 = group.controls[campo2].value;
      const iguales: any = null;

      this.legal = legal.html;

      if (pass1 === pass2) {
        return null;
      }
      return {
        sonIguales: true,
      };

    };
  }

  ngOnInit() {

    initCustom();

    this.forma = new FormGroup({
      nombre: new FormControl( null, Validators.required),
      apellidos: new FormControl(null, Validators.required),
      correo: new FormControl(null, [Validators.required, Validators.email]),
      phone: new FormControl(null),
      password: new FormControl(null, [Validators.required]),
      password2: new FormControl(null, Validators.required),
      condiciones: new FormControl(false),
    }, { validators:
      this.sonIguales( 'password', 'password2')
     });
  }

  registrarUsuario() {
    this.errorPasswordLength = false;

    if (this.forma.invalid) {
      return;
    }
    if (!this.forma.value.condiciones) {
      this.condicionesSwal.show().then();
      return;
    }

    if (this.forma.value.password.length < 7 || this.forma.value.password.length > 12) {
      this.errorPasswordLength = true;
      return;
    }

    this.authService.registerWithEmail(this.forma).then( (result) => {
      let self = this;
      const usuario: Usuario = {
        name: `${this.forma.value.nombre} ${this.forma.value.apellidos}`,
        first_name: this.forma.value.nombre,
        last_name: this.forma.value.apellidos,
        gender: null,
        email: this.forma.value.correo,
        phoneNumber: this.forma.value.phone,
        photoUrl: null,
        provider: 'email',
        key: result.uid,
        emailVerified: result.emailVerified,

        // TODO IMPLEMENTAR EL CÓDIGO TELEFONÓNICO PARA LOS USUARIOS
        //phoneCode: this._usuarioService.phoneCode
      };
      this._usuarioService.saveUserFromEmail(result, usuario).then( () => {
        self.successSwal.show().then();
      }).catch( (error) => {
        self.showError(error.code, this.errorSwal);
      });
    }).catch( (error) => {
        this.showError(error.code, this.errorSwal);
    });

  }

  showError(errorcode: string, swal) {
    let message = '';
    switch (errorcode) {
      case 'auth/email-already-in-use':
        message = 'REGISTER.EMAIL_ALREADY_IN_USE_ERROR';
        break;
      case 'auth/invalid-email':
        message = 'REGISTER.INVALID_EMAIL_ERROR';
        break;
      default:
        message = 'REGISTER.UNKNOWN_ERROR';
    }
    this.translateService.get(['GENERAL.ERROR', message, 'GENERAL.OK']).subscribe( t => {
      this.errorMessage = t[message];
      swal.text = this.errorMessage;
      swal.show().then();
    });

  }

  confirmSuccess() {
    this.router.navigate(['/login']);
  }
}
