import { Injectable } from '@angular/core';
import {Pedido} from '../../interfaces/pedido.interface';
import {CentroService} from '../centro/centro.service';
import * as moment from 'moment';
import {UsuarioService} from '../usuario/usuario.service';
import {AngularFireAction, AngularFireDatabase} from 'angularfire2/database';
import {Observable} from 'rxjs/Observable';
import * as firebase from 'firebase';
import DataSnapshot = firebase.database.DataSnapshot;

@Injectable()
export class PedidoService {

  numberPeddosActivos: number;

  pedido: Pedido = {
    tipo: 0,
    servido: false,
    importeCompraFactor: 0,
    montajeOption : false,
    precioMontaje : 0,
    precioPax: 0,
    reciclajeOption : false,
    precioDefault : 23,
    precioFinal : 23,
    colchon: false,
    colchon_cantidad: 1,
    sofa: false,
    sofa_cantidad: 1,
    electrodomestico: false,
    electrodomestico_cantidad: 1,
    peatonal: false,
    ascensor: false,
    electricidad: false,
    espacio: false,

    precioFinalTransporte: 23,
    precioFinalMontaje: 0,
    precioFinalReciclaje: 0,
    precioFinalMontajeCocina: 0,
    precioFinalReciclajeCocina: 0,

    distancia: 0,
    precioDistancia: 0,
    fechaReserva: null,
    vanner: null,
    charge: null,
    direccion: null,
    detalleDireccion: null,
    fullDireccion: null,

    montajeSofa: false,
    montajeSillon: false,
    montajeComplemento: false,
    montajeComplemento_cantidad: 1,
    montajeSillon_cantidad: 1,
    montajeSofa_cantidad: 1,
    cocina_medicion: false,
    metros_metod: 0,
    metros_knoxhult: 0,
    metros_cocina_usada: 0
  };

  rangoHoras: string[] = [
    'De 10:00h a 13:00h', 'De 13:00h a 16:00h', 'De 16:00h a 19:00h', 'De 19:00h a 22:00h'
  ];
  datosUbicacionPedido: any;
  opcionSoloMontaje: boolean;

  constructor(public _centroService: CentroService,
              public _userService: UsuarioService,
              public af: AngularFireDatabase) {
    this.pedido.precioFinalTransporte = this.pedido.precioDefault;
    this.numberPeddosActivos = 0;
  }

  /**
   * Inicializa los datos iniciales del pedido
   */
  inicializarPedido() {
    this.opcionSoloMontaje = false;
    let precioDistancia = 0;
    const precioDefault = 23;
    let precioFinal = 0;
    let distancia: number;
    let precio: number;
    let postalcode: string;

    distancia = this.datosUbicacionPedido.ruta.legs[0].distance.value;
    postalcode = this.datosUbicacionPedido.ubicacion.fullDireccion.postalCode;
    precio = this._centroService.centro.postalcodes[this._centroService.currentZonaKey][postalcode];

    precioDistancia = precio;
    precioFinal = precioDistancia;

    console.log(distancia);
    console.log(postalcode);
    console.log(precio);
    console.log( this.datosUbicacionPedido.ubicacion.tipo);
    console.log( this._centroService.currentZonaKey);

    this.pedido = {
      tipo: 0,
      servido: false,
      importeCompraFactor: 1,
      montajeOption: false,
      precioMontaje: 0,
      precioPax: 0,
      reciclajeOption: false,
      precioDefault: precioDefault,
      precioFinal: precioFinal,
      colchon: false,
      sofa: false,
      mpeq: false,
      mgrande: false,
      electrodomestico: false,
      colchon_cantidad: 1,
      sofa_cantidad: 1,
      mpeq_cantidad: 1,
      mgrande_cantidad: 1,
      electrodomestico_cantidad: 1,
      peatonal: false,
      ascensor: false,
      electricidad: false,
      espacio: false,
      precioFinalTransporte: 23,
      precioFinalMontaje: 0,
      precioFinalMontajeCocina: 0,
      precioFinalReciclaje: 0,
      precioFinalReciclajeCocina: 0,
      distancia: distancia,
      precioDistancia: precioDistancia,
      fechaReserva: moment().unix(),
      vanner: null,
      charge: null,
      usuario: this._userService.userProfile,
      direccion: this.datosUbicacionPedido.ubicacion.direction,
      detalleDireccion : this.datosUbicacionPedido.ubicacion.detalleDirection,
      fullDireccion: this.datosUbicacionPedido.ubicacion.fullDireccion,
      montajeSofa: false,
      montajeSillon: false,
      montajeComplemento: false,
      montajeComplemento_cantidad: 1,
      montajeSillon_cantidad: 1,
      montajeSofa_cantidad: 1,
      centro_key: this._centroService.centro.key,
      centro_code: this._centroService.centro.code,
      cocina_medicion: false,
      metros_metod: 0,
      metros_knoxhult: 0,
      metros_cocina_usada: 0
    };

    console.log(this.pedido);
  }

  updatePedido() {
    const factorMontaje = Math.ceil(this.pedido.precioMontaje / 100);
    const factorPax = Math.ceil(this.pedido.precioPax / 100);
    const importeMontaje = 0;
    let precioFinalMontaje = 0;

    if (this.pedido.montajeOption === true) {
      console.log('montajeOption == true');
      //PAX
      if (factorPax >= 0 && factorPax <= 5) {
        precioFinalMontaje +=  factorPax * 12;
      } else if (factorPax > 5 && factorPax <= 10) {
        precioFinalMontaje += 60 + (factorPax - 5) * 11;
      } else if (factorPax > 10 && factorPax <= 15) {
        precioFinalMontaje += 115 + (factorPax - 10) * 10;
      } else if (factorPax > 15) {
        precioFinalMontaje += 165 + (factorPax - 15) * 9;
      }
      console.log('Precio final montaje (PAX)', precioFinalMontaje);

      //MONTAJE
      if (factorMontaje >= 0 && factorMontaje <= 5) {
        precioFinalMontaje +=  factorMontaje * 16;
      } else if (factorMontaje > 5 && factorMontaje <= 10) {
        precioFinalMontaje += 80 + (factorMontaje - 5) * 13;
      } else if (factorMontaje > 10 && factorMontaje <= 20) {
        precioFinalMontaje += 145 + (factorMontaje - 10) * 9;
      } else if (factorMontaje > 20) {
        precioFinalMontaje += 235 + (factorMontaje - 20) * 7;
      }
      console.log('Precio final montaje (PAX + otros muebles)', precioFinalMontaje);

      const importeMontajeSofa = this._centroService.centro.precios.montaje.sofa;
      const importeMontajeSillon = this._centroService.centro.precios.montaje.sillon;
      const importeMontajeComplemento = this._centroService.centro.precios.montaje.complemento;

      if (this.pedido.montajeSofa == true) {
        console.log('montajeSofa == true');
        precioFinalMontaje += this.pedido.montajeSofa_cantidad * importeMontajeSofa;
      }
      console.log('Precio final montaje (PAX + otros muebles + sofa)', precioFinalMontaje);
      if (this.pedido.montajeSillon == true) {
        console.log('montajeSillon == true');
        precioFinalMontaje += this.pedido.montajeSillon_cantidad * importeMontajeSillon;
      }
      console.log('Precio final montaje (PAX + otros muebles + sofa + sillon)', precioFinalMontaje);
      if (this.pedido.montajeComplemento == true) {
        console.log('montajeComplemento == true');
        precioFinalMontaje += this.pedido.montajeComplemento_cantidad * importeMontajeComplemento;
      }
      console.log('Precio final montaje (PAX + otros muebles + sofa + sillon + complemento)', precioFinalMontaje);

      this.pedido.precioFinalMontaje = precioFinalMontaje;
    }

    //Reciclaje
    let importeReciclaje = 0;
    const importe_colchon = this._centroService.centro.precios.usado.colchon * this.pedido.colchon_cantidad;
    const importe_sofa = this._centroService.centro.precios.usado.sofa * this.pedido.sofa_cantidad;
    const importe_mpeq = this._centroService.centro.precios.usado.mpeq * this.pedido.mpeq_cantidad;
    const importe_mgrande = this._centroService.centro.precios.usado.mgrande * this.pedido.mgrande_cantidad;

    if (this.pedido.reciclajeOption === true) {
      if (this.pedido.colchon === true) { importeReciclaje += importe_colchon; }
      if (this.pedido.sofa === true) { importeReciclaje += importe_sofa; }
      if (this.pedido.mpeq === true) { importeReciclaje += importe_mpeq; }
      if (this.pedido.mgrande === true) { importeReciclaje += importe_mgrande; }
    }

    console.log('importe factor compra:', this.pedido.importeCompraFactor );
    this.pedido.precioFinalReciclaje = importeReciclaje;
    const pt = this.pedido.precioDistancia;
    if (this._centroService.centro.orderConfiguration.opcion_cocina) {
      this.pedido.precioFinalTransporte = pt * (this.pedido.importeCompraFactor - 1);
    } else {
      this.pedido.precioFinalTransporte = pt * (this.pedido.importeCompraFactor);
    }

    if (this.pedido.inZone === 'reserva'){
      if (this._centroService.centro.orderConfiguration.opcion_cocina) {
        this.pedido.precioFinalTransporte = pt + ((this.pedido.importeCompraFactor - 2) * 35);
      } else {
        this.pedido.precioFinalTransporte = pt + ((this.pedido.importeCompraFactor - 1) * 35);
      }
    }

    if (this._centroService.centro.orderConfiguration.opcion_cocina) {
      // Centro con montaje de cocina
      if (this.pedido.importeCompraFactor < 2){
        // Opciones solo montaje cocina y solo montaje muebles
        if (this.pedido.importeCompraFactor === 1) {
          // Solo montaje cocina
          this.pedido.precioFinalTransporte = 0;
        }else{
          // Solo montaje muebles
          if (this.pedido.inZone === 'reserva') {

            const postalcode = this.datosUbicacionPedido.ubicacion.fullDireccion.postalCode;
            const precioPC = this._centroService.centro.postalcodes[this._centroService.currentZonaKey][postalcode];

            let precioreducido = precioPC * 0.5;
            if (precioreducido < 20 ) {
              precioreducido = 20;
            }
            this.pedido.precioFinalTransporte = precioreducido;
          }else{
            this.pedido.precioFinalTransporte = 20;
          }
        }
      }
    } else {
      if (this.pedido.importeCompraFactor === 0){
        // Opciones solo montaje cocina y solo montaje muebles

        if (this.pedido.inZone === 'reserva') {

          let postalcode = this.datosUbicacionPedido.ubicacion.fullDireccion.postalCode;
          let precioPC = this._centroService.centro.postalcodes[this._centroService.currentZonaKey][postalcode];

          let precioreducido = precioPC * 0.5;
          if (precioreducido < 20 ) {
            precioreducido = 20;
          }
          this.pedido.precioFinalTransporte = precioreducido;
        } else {
          this.pedido.precioFinalTransporte = 20;
        }
      }
    }

    // Cocinas
    if (this._centroService.centro.orderConfiguration.opcion_cocina) {

      // Medición
      let precioMedicion:number;
      if (this.pedido.cocina_medicion) {
        precioMedicion = this._centroService.centro.precios.montaje.cocina_med;
        this.pedido.precioFinalMontajeCocina = precioMedicion;
      }else{
        this.pedido.precioFinalMontajeCocina = 0;
      }

      // Metod
      this.pedido.precioFinalMontajeCocina += this.pedido.metros_metod * this._centroService.centro.precios.montaje.metod;

      // Knoxhult
      this.pedido.precioFinalMontajeCocina += this.pedido.metros_knoxhult * this._centroService.centro.precios.montaje.knoxhult;

      //Reciclaje
      if (this.pedido.metros_cocina_usada > 0 ){
        console.log('UPDATED RECICLJE COCINA');
        this.pedido.precioFinalReciclajeCocina = this.pedido.metros_cocina_usada * this._centroService.centro.precios.usado.cocina;
      }else{
        this.pedido.precioFinalReciclajeCocina = 0;
      }
    }


    this.pedido.precioFinal = this.pedido.precioFinalMontajeCocina + this.pedido.precioFinalTransporte
      + precioFinalMontaje + importeReciclaje + this.pedido.precioFinalReciclajeCocina;
  }

  /**
   * Obteniene de la base de datos las reservas del dia de un vanner
   * @param uidVanner
   * @returns {Observable<AngularFireAction<DataSnapshot>[]>}
   */
  getCurrentReservasOfVanner(uidVanner: any): any {
    return this.af.list(`/reservas/${uidVanner}/${this.pedido.fechaReserva.fecha.value}/`).valueChanges();
  }

  getCurrentReservasOfVanner2(uidVanner: any): any {
    return this.af.list(`/reservas/${uidVanner}/${this.pedido.fechaReserva.fecha.value}/`).snapshotChanges();
  }

  /**
   * Obtiene un pedido por su key
   * @param key
   * @returns {FirebaseObjectObservable<any>}
   */
  getPedidoByKey(key: string): any {
    return this.af.object(`/pedidos/${key}`).snapshotChanges();
  }

  /**
   * Obtiene los pedidos activos del usuario
   * @returns {AngularFireList<any>}
   */
  getPedidosActivos() {
    return this.af.list(`/pedidos_user_activos/${this._userService.userProfile.key}`).snapshotChanges();
  }

  /**
   * Obtine los pedudos finalizados del usuario
   * @returns {AngularFireList<any>}
   */
  getPedidosHistoria() {
    return this.af.list(`/pedidos_user_historia/${this._userService.userProfile.key}`).snapshotChanges();
  }

  /**
   * Obtinen el número de pedidos activos de un usuario
   * @param id
   * @returns {AngularFireList<T>|FirebaseListObservable<any[]>}
   */
  getNumberPedidos(uid: string) {
    return this.af.list(`/pedidos_user_activos/${uid}`).snapshotChanges().subscribe( (snapshots) => {
      this.numberPeddosActivos = snapshots.length;
    });
  }

}
