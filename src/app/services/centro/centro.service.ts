import { Injectable } from '@angular/core';
import {Centro} from '../../interfaces/centro.interface';

import * as firebase from "firebase/app";
import {AngularFireDatabase} from "angularfire2/database";


@Injectable()
export class CentroService {

  centros: any;
  centro: Centro;
  centrosDisponibles: any;
  currentZonaKey;

  constructor(private af: AngularFireDatabase) {
    this.centrosDisponibles = null;
  }

  getCentros(): any {

    let refe = this.af.database.ref('/centros');
    this.centros = this.af.list(refe, ref => ref.orderByChild('active').equalTo(true));
    return this.centros.valueChanges();
  }

}
