import { Injectable } from '@angular/core';
import {AngularFireDatabase} from 'angularfire2/database';

@Injectable()
export class ValoracionesService {

  constructor(private af: AngularFireDatabase) { }

  /**
   * Obtiene las valoraciones recibidas de un vanner
   * @param {string} key
   * @returns {any}
   */
  getValoracionesVannerByKey(key: string): any {
    return this.af.list(`/valoraciones_vanner/${key}`, ref => ref.orderByChild('date').startAt(0)).valueChanges();
  }


  /**
   * Obtiene las valoraciones enviadas por un usuario
   * @param {string} key
   * @returns {any}
   */
  getValoracionesUserByKey(key: string): any {
    return this.af.list(`/valoraciones_user/${key}`, ref => ref.orderByChild('date').startAt(0)).valueChanges();
  }

}
