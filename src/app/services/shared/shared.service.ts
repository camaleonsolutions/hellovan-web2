import { Injectable } from '@angular/core';
import {AngularFireAuth} from "angularfire2/auth";

@Injectable()
export class SharedService {

  constructor( public afs:AngularFireAuth) { }

}
