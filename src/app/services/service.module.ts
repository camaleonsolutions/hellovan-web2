import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {AngularFireAuthModule} from 'angularfire2/auth';

import {
  SharedService,
  SidebarService,
  LoginGuard,
  AuthService,
  UsuarioService,
  GlobalService,
  CentroService,
  PedidoService,
  ProgressPedidoService,
  VannerService,
  ChatService,
  ValoracionesService,
  NotificationsService,
  PagoService
} from './service.index';

@NgModule({
  imports: [
    CommonModule,
    AngularFireAuthModule,
  ],
  declarations: [],
  providers: [
    SharedService,
    SidebarService,
    AuthService,
    LoginGuard,
    UsuarioService,
    GlobalService,
    CentroService,
    PedidoService,
    ProgressPedidoService,
    VannerService,
    ChatService,
    ValoracionesService,
    NotificationsService,
    PagoService
  ]
})
export class ServiceModule { }
