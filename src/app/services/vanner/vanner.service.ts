import { Injectable } from '@angular/core';
import {AngularFireDatabase} from 'angularfire2/database';
import * as firebase from 'firebase';
import {Usuario} from '../../models/usuario.model';
import {Vanner} from '../../interfaces/vanner.interface';
import { GeoFire } from 'geofire';
import {CentroService} from '../centro/centro.service';

@Injectable()
export class VannerService {

  vanners: any;
  constructor(private af: AngularFireDatabase,
              private _centroService: CentroService) { }

  /**
   * Obtiene el perfil de un vanner
   * @param {string} key
   * @returns {AngularFireObject<any>}
   */
  getVanner(key: string) {
    return this.af.object(`/vanners/${key}`).snapshotChanges();
  }

  getVannerPlain(key: string) {
    return this.af.object(`/vanners/${key}`).valueChanges();
  }

  /**
   * Obtiene el listado de vanners
   */
  getVanners() {
    const vannersref = this.af.list('/vanners', ref => ref.orderByChild('rating').startAt(0));
    return vannersref.valueChanges();
  }



  setUrlImagesAllUsuarios() {
    const ref = this.af.list('/usuarios');
    const refUSer = ref.valueChanges().subscribe( (usuarios) => {
      console.log(usuarios);
      refUSer.unsubscribe();
      usuarios.forEach((usuario: Usuario) => {
        if (usuario.hasOwnProperty('photo')) {
          if (!usuario.hasOwnProperty('photoUrl')) {
            console.log('usuarios:', usuario.key);
            console.log(usuario.photo);
            firebase.storage().ref().child(`avatars/${usuario.photo}`).getDownloadURL()
              .then(url => {
                this.af.object(`/usuarios/${usuario.key}`).update({
                  photoUrl : url
                }).then();
              })
              .catch((error) => {
                return 'assets/images/default_avatar.png';
              });
          }
        }
      });
    });
  }
  setUrlImagesAllVanners() {

    const ref = this.af.list('/vanners');
    const refvanner = ref.valueChanges().subscribe( (vanners) => {
     // console.log(vanners);
      refvanner.unsubscribe();
      vanners.forEach((vanner: Vanner) => {


         //console.log('vanners:', vanner.key);
         // console.log(vanner.avatar);
          firebase.storage().ref().child(`vanners/${vanner.avatar}`).getDownloadURL()
            .then(url => {
              this.af.object(`/vanners/${vanner.key}`).update({
                avatar_url : url
              }).then();
            })
            .catch((error) => {
              return 'assets/images/default_avatar.png';
            });

        const van = vanner.van;

          console.log(vanner.key);
          console.log(vanner.van);
          firebase.storage().ref().child(`vanners/${van.main_foto}`).getDownloadURL()
            .then(url => {
              this.af.object(`/vanners/${vanner.key}`).update({
                van: {
                  main_foto: vanner.van.main_foto,
                  capacidad: vanner.van.capacidad,
                  capacidad_tipo: vanner.van.capacidad_tipo,
                  marca: vanner.van.marca,
                  matricula: vanner.van.matricula,
                  year: vanner.van.year,
                  main_foto_url : url
                }
              }).then();
            })
            .catch((error) => {
              return 'assets/images/default_avatar.png';
            });

      });
    });
  }

  /**
   * Obtiene la foto del avatar de firebase storage
   * @param {string} filename
   * @returns {Promise<string | T>}
   */
  getAvatar(filename: string) {

    return firebase.storage().ref().child(`vanners/${filename}`).getDownloadURL()
      .then(url => {
        return url;
      })
      .catch((error) => {
        return 'assets/images/default_avatar.png';
      });
  }

  /**
   * Obtiene la foto de la furgoneta del firebase storage
   * @param {string} filename
   * @returns {Promise<string | T>}
   */
  getPhoto(filename: string) {

    return firebase.storage().ref().child(`vanners/${filename}`).getDownloadURL()
      .then(url => {
        return url;
      })
      .catch((error) => {
        return 'assets/images/default_avatar.png';
      });
  }

  getVannersExpressR() {
    const firebaseRef = firebase.database().ref('geo');
    const geoFire = new GeoFire(firebaseRef);
    const ikeaPos = [this._centroService.centro.lat, this._centroService.centro.lng];
    const geoQuery = geoFire.query({
      center: ikeaPos,
      radius: 1
    });

    const vannersInRadiusExpressR = [];
    const self = this;

    const promise = new Promise( ( resolve => {
      const onReadyRegistration = geoQuery.on('ready', function() {
        console.log('GeoQuery has loaded and fired all other events for initial data', );
        onKeyEnteredRegistration.cancel();
        geoQuery.cancel();
        if (vannersInRadiusExpressR.length === 0) {
          console.log('No hay resultados');
        } else {
          console.log('Existen vanners en el área de Express-R');
        }
        resolve(vannersInRadiusExpressR);
      });
    }));


    const onKeyEnteredRegistration = geoQuery.on('key_entered', function(key, location, distance) {
      // console.log(key + " entered query at " + location + " (" + distance + " km from center)");
      const pos = {
        key: key,
        location: location,
        distance: distance
      };
      vannersInRadiusExpressR.push(pos);
    });

    return promise;
  }
}
