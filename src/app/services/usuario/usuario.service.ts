import { Injectable } from '@angular/core';
import {Usuario} from '../../interfaces/usuario.interface';
import {AngularFireDatabase} from 'angularfire2/database';
import * as firebase from 'firebase';
import {HeaderComponent} from '../../shared/header/header.component';
import {GlobalService} from "../shared/global.service";

@Injectable()
export class UsuarioService {

  usuario: any = {};
  userProfile: any = {};
  phoneCode: any = {
    code: 'ES',
    number: '34'
  };

  avatar: string;

  constructor( public af: AngularFireDatabase,  private globalService: GlobalService,) {
    console.log('Servico Usuarios listo');
    this.avatar = 'assets/images/default_avatar.png';
  }

  /**
   * Carga o actualiza la información del perfil del usuario almacenado en la base de datos
   * @param user
   */
  loadProfiles(user: any) {
    console.log(user);
    const usrProf = this.getUserprofile(user.uid).subscribe((res) => {
      console.log('USER SUBSCRIBED', JSON.stringify(res));
      if (res.payload.val() != null) {
        console.log('existe perfil');
        this.userProfile = res.payload.val();
        if (res.phoneCode != null) {
          this.phoneCode = res.phoneCode;
        }
        if (this.userProfile.provider === 'facebook.com') {
          const us = {
            photoUrl: user.providerData[0].photoURL
          };
          const userRef = this.af.object(`/usuarios/${ user.uid }`);
          userRef.update(us).then(() => {
            this.userProfile.photoUrl = user.providerData[0].photoURL;
          });
          this.avatar = res.payload.val().photoUrl;
        } else{
          this.avatar = res.payload.val().photoUrl;
        }
      } else {
        console.log('no existe perfil, lo registramos');
        if (user.providerData[0].providerId === 'facebook.com') {
          const str = user.displayName.split(' ');
          const first_name = str[0];
          const last_name = str[1];
          const us = {
            name: user.displayName,
            first_name: first_name,
            last_name: last_name,
            gender: null,
            email: user.providerData[0].email,
            phoneNumber: user.providerData[0].phoneNumber,
            phoneCode: {
              code: 'ES',
              number: 34
            },
            photoUrl: user.providerData[0].photoURL,
            provider: user.providerData[0].providerId,
            emailVerified: user.emailVerified,
            facebook_id: user.providerData[0].uid,
            key: user.uid
          };
          const userRef = this.af.object(`/usuarios/${ user.uid }`);
          userRef.update(us).then(() => {
             console.log('User registered!');
             this.loadProfiles(user);
          });
        }
      }
      usrProf.unsubscribe();

    });

    return usrProf;
  }

  /**
   * Obtiene el perfil de un usuario registrado
   * @param uid
   * @returns {FirebaseObjectObservable<any>}
   */
  getUserprofile(uid: string): any {
    return this.af.object(`/usuarios/${ uid }`).snapshotChanges();
  }

  /**
   * Guarda la información del usuario registrado con email en firebase
   * @param res
   * @param usuario
   * @returns {Promise<TResult2|TResult1>}
   */
  saveUserFromEmail(res, usuario): any {
    this.usuario = usuario;
    const userRef = this.af.object(`/usuarios/${ res.uid }`);
    return userRef.set(usuario);
  }

  /**
   * Obtiene la foto del avatar de firebase storage
   * @param {string} filename
   * @returns {Promise<string | T>}
   */
  getAvatar(filename: string) {

    return firebase.storage().ref().child(`avatars/${filename}`).getDownloadURL()
      .then(url => {
        return url;
      })
      .catch((error) => {
        return 'assets/images/default_avatar.png';
      });
  }

  updateProfile() {
    console.log(this.userProfile);
    const data = {
      first_name: this.userProfile.first_name,
      last_name: this.userProfile.last_name,
      email: this.userProfile.email,
      phoneNumber: this.userProfile.phoneNumber,
      phoneCode : {
        code: this.userProfile.phoneCode.code,
        number: this.userProfile.phoneCode.number
      }
    };

    return this.af.object(`usuarios/${this.userProfile.key}`).update(data);
  }

  /**
   * Envia email con enlace para el restablecimiento de la contraseña
   * @param {string} emailAddress
   * @returns {Promise<any>}
   */
  restablecer(emailAddress: string) {
    firebase.auth().languageCode = this.globalService.language;
    return firebase.auth().sendPasswordResetEmail(emailAddress);
  }

  verficar() {
    const user = firebase.auth().currentUser;
    return user.sendEmailVerification().then(function() {
      // Email sent
    }).catch(function(error) {
      // An error happened.
    });
  }




}
