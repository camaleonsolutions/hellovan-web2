import { Injectable } from '@angular/core';
import {AngularFireDatabase} from 'angularfire2/database';
import {UsuarioService} from './usuario/usuario.service';
import {Chat} from '../interfaces/chat.interface';
import * as firebase from 'firebase';


@Injectable()
export class ChatService {

  numberChats:number;
  constructor(private af: AngularFireDatabase,
              private _userService: UsuarioService) {
    this.numberChats = 0;
  }

  /**
   * Obtiene los chats de un vanner
   * @param {string} uid
   * @returns {Observable<any[]>}
   */
  getChatsByUid(uid: string) {
    return this.af.list(`/chat/rooms/${uid}`, ref => ref.orderByChild('date')).valueChanges();
  }

  /**
   * Crea el chat usuario la primera vez que se envia un mensaje
   * @param chat
   * @param {boolean} first
   * @returns {PromiseLike<any>}
   */
  createChatUser(chat: any) {
    return this.af.list(`chat/rooms/${chat.user}`).push(chat).then(
      (res) => {
        return res.key;
      });
  }

  /**
   * Obtiene las conversaciones de un usuario
   * @returns {Observable<SnapshotAction[]>}
   */
  getChats() {
    return this.af.list(`/chat/rooms/${this._userService.userProfile.key}`,
        ref => ref.orderByChild('date')).snapshotChanges();
  }

  getMessage(uid: string, chatKey: string) {
    return this.af.object(`chat/messages/${chatKey}/${uid}`).snapshotChanges();
  }

  getMessages(chat: Chat) {
    return this.af.list(`/chat/messages/${chat.key}`,
        ref => ref.orderByChild('date')
      .startAt(0)
      .limitToLast(100))
      .snapshotChanges();
  }

  createChatVanner(chat: any) {
    return this.af.object(`chat/rooms/${chat.vanner}/${chat.key}`).set({
      vanner: chat.vanner,
      user: chat.user,
      date: chat.date,
      pending: chat.pending,
      last_message_uid: chat.last_message_uid
    });
  }

  addMessage(uid: string, chat: any, msg: any) {
    const message = {
      type: 'user',
      sendBy: uid,
      to: chat.vanner,
      message: msg,
      date: firebase.database.ServerValue.TIMESTAMP
    };
    return this.af.list(`chat/messages/${chat.key}`).push(message).then(
      (data) => {
        //TODO Añadimos aqui el actualizar el último mensaje de chat
        return this.af.object(`chat/rooms/${chat.user}/${chat.key}/last_message_uid`).set(data.key).then(
          () => {
            return this.af.object(`chat/rooms/${chat.vanner}/${chat.key}/last_message_uid`).set(data.key).then(
              () => {
                //this._pushProvider.send_notification(chat, data.key, message);
                this.incrementPendings(chat);
              });
          }
        );
      }
    );
  }

  getNumberPendingsMessages(uid) {
    return this.af.list(`/chat/rooms/${uid}`).snapshotChanges().subscribe( (snapshots) => {
      let counter = 0;
      snapshots.forEach( (snapshot) => {
          counter += snapshot.payload.val().pending;
        });
      this.numberChats = counter;
    });
  }

  resetPendings(chat: any) {
    firebase.database().ref(`/chat/rooms/${chat.user}/${chat.key}/pending`).transaction( (number) => {
      return 0;
    }).then();
  }

  incrementPendings(chat: any) {
    firebase.database().ref(`/chat/rooms/${chat.vanner}/${chat.key}/pending`).transaction( (number) => {
      return number + 1;
    }).then();
  }
}
