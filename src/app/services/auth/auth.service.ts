import { Injectable } from '@angular/core';
import {AngularFireAuth} from 'angularfire2/auth';
import * as firebase from 'firebase/app';

@Injectable()
export class AuthService {

  constructor( private afs: AngularFireAuth) {

  }

  /**
   * Registro de usuario con email
   * @param forma
   * @returns {any}
   */
  registerWithEmail(forma: any): any {
    return firebase.auth().createUserWithEmailAndPassword(forma.value.correo, forma.value.password);
  }

  /**
   * Login con email del usuario
   * @param {string} email
   * @param {string} password
   * @param {boolean} recuerdame
   */
  loginWithEmail(email: string, password: string, recuerdame: boolean): any {

    if (recuerdame) {
      localStorage.setItem('email', email);
    } else {
      localStorage.removeItem('email');
    }
    return firebase.auth().signInWithEmailAndPassword(email, password);
  }

  /**
   * Login con Facebook del usuario
   * @returns {Promise<T>}
   */
  loginWithFacebook(): any {
    const provider = new firebase.auth.FacebookAuthProvider();
    firebase.auth().languageCode = 'es_ES';

    return firebase.auth().signInWithPopup(provider).then(function(result) {
      // This gives you a Facebook Access Token. You can use it to access the Facebook API.
      const token = result.credential.accessToken;
      // The signed-in user info.
      const user = result.user;

      console.log(user);
      console.log(token);

      // ...
    }).catch(function(error) {
      // Handle Errors here.
      const errorCode = error.code;
      const errorMessage = error.message;
      // The email of the user's account used.
      const email = error.email;
      // The firebase.auth.AuthCredential type that was used.
      const credential = error.credential;

      console.log(errorCode);
      console.log(errorMessage);

    });

  }

  /**
   * Envia por email link para cambiar contraseña
   * @param {string} email
   * @returns {Promise<any>}
   */
  resetPassword(email: string) {
    return firebase.auth().sendPasswordResetEmail(email);
  }

  /**
   * Cierra sesión del usuario
   */
  logout() {
    console.log('Logout');
    this.afs.auth.signOut().then();

  }

}
