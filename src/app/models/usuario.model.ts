export class Usuario {

  constructor(
    public email?: string,
    public name?: string,
    public first_name?: string,
    public last_name?: string,
    public gender?: string,
    public locale?: string,
    public phoneNumber?: string,
    public photoUrl?: string,
    public photo?: string,
    public facebook_id?: string,
    public provider?: string,
    public emailVerified?: boolean,
    public key?: string,
    public push?: any,
    public device?: any,
    public phoneCode?: any,
  ) {}
}
