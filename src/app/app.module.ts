import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {AngularFireModule} from 'angularfire2';

// Rutas
import {APP_ROUTES} from './app.routes';

// Modulos
import {PagesModule} from './pages/pages.module';

// Services
import {ServiceModule} from './services/service.module';

// Configs
import {environment} from '../environments/environment';

// Componentes
import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {SweetAlert2Module} from '@toverux/ngx-sweetalert2';
import {TranslateLoader, TranslateModule} from '@ngx-translate/core';
import {HttpClient, HttpClientModule} from '@angular/common/http';
import {TranslateHttpLoader} from '@ngx-translate/http-loader';
import {CommonModule} from "@angular/common";
import { GoogleplaceDirective } from './directives/googleplace.directive';
import {AngularFireDatabaseModule} from "angularfire2/database";
import { HomeComponent } from './site/home/home.component';
import {AgmCoreModule, GoogleMapsAPIWrapper} from "@agm/core";
import {NgxStripeModule} from "ngx-stripe";

export function createTranslateLoader(http: HttpClient) {
  return new TranslateHttpLoader(http, './assets/i18n/', '.json');
}

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    RegisterComponent,
    GoogleplaceDirective,
    HomeComponent,
  ],
  imports: [
    BrowserModule,
    CommonModule,
    HttpClientModule,
    AngularFireModule.initializeApp(environment.firebase),
    AngularFireDatabaseModule,
    SweetAlert2Module.forRoot({
      buttonsStyling: false,
      customClass: 'modal-content',
      confirmButtonClass: 'btn btn-primary',
      cancelButtonClass: 'btn'
    }),
    NgxStripeModule.forRoot(environment.stripePublicKey),
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyDuLmF7z-4K4SHi_sOWfpnCACAUq15r_SE',
      libraries: ["places"]
    }),

    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: createTranslateLoader,
        deps: [HttpClient]
      }
    }),
    APP_ROUTES,
    FormsModule,
    ReactiveFormsModule,
    PagesModule,
    ServiceModule,
  ],
  providers: [GoogleMapsAPIWrapper],
  bootstrap: [AppComponent]
})
export class AppModule { }
