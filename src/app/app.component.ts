import { Component } from '@angular/core';
import {TranslateService} from '@ngx-translate/core';
import {GlobalService} from './services/shared/global.service';
import {AngularFireAuth} from 'angularfire2/auth';
import {Router} from '@angular/router';
import {UsuarioService} from "./services/usuario/usuario.service";
import {HeaderComponent} from "./shared/header/header.component";
import {ChatService} from "./services/chat.service";
import {NotificationsService} from "./services/notifications/notifications.service";
import {PedidoService} from "./services/pedido/pedido.service";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {

  title = 'app';
  constructor (public translateService: TranslateService,
               public globalService: GlobalService,
               public afs: AngularFireAuth,
               public router: Router,
               public usuarioService: UsuarioService,
               public chatService: ChatService,
               public notificationsService: NotificationsService,
               public pedidosService: PedidoService) {

    this.setLanguage();
    this.initUserObserver();
  }

  /**
   * Observador de estado del usuario
   * Si cambia a logueado o a no logueado
   */
  initUserObserver() {
    this.afs.auth.onAuthStateChanged( (user) => {
      if (!user) {
        console.log('No logueado');
        this.router.navigate(['/login']);
      } else {
        console.log('Usuario logueado');
        this.router.navigate(['/dashboard']);

        this.usuarioService.loadProfiles(user);
        this.chatService.getNumberPendingsMessages(user.uid);
        this.notificationsService.getNumberNotifications(user.uid);
        this.pedidosService.getNumberPedidos(user.uid);
      }
    });
  }

  /**
   * Establece el lenguaje inicial de la app y lo guarda en localStorege
   */
  setLanguage() {
    if (localStorage.getItem('language')) {
      console.log('existe lenguaje',localStorage.getItem('language') );
      this.globalService.language = localStorage.getItem('language');
    } else {
      this.globalService.language = this.translateService.getBrowserLang();
    }
    this.translateService.setDefaultLang(this.globalService.language);
  }
}
