import {Component, OnInit, ViewChild} from '@angular/core';
import {PagesComponent} from '../pages.component';
import {TranslateService} from '@ngx-translate/core';
import {PedidoService, CentroService} from '../../services/service.index';
import {Router} from '@angular/router';
import {SwalComponent} from '@toverux/ngx-sweetalert2';

@Component({
  selector: 'app-confirmation',
  templateUrl: './confirmation.component.html',
  styleUrls: ['./confirmation.component.css']
})
export class ConfirmationComponent implements OnInit {

  @ViewChild('anularSwal') private anularSwal: SwalComponent;
  active: number;
  centro_name: string;
  centro: any;

  errorMessage = '';

  precioTransporte: number;
  precioDistancia: number;
  precioMontaje: number;
  precioReciclaje: number;
  compra: string;
  tipoServicio: string;

  compras: string[];

  constructor(public pages: PagesComponent,
              public translateService: TranslateService,
              public _centroService: CentroService,
              public _pedidoService: PedidoService,
              private router: Router) {
    this.translateService.get('CONFIRMATION.TITLE').subscribe((t: string) => {
      this.pages.title = t;
    });

    this.translateService.get(['CONFIRMATION.ONLY_ASSEMBLY', 'TICKET.ASSEMBLY_KITCHEN', 'FORM.SELECT_AMOUNT', 'GENERAL.OK', 'GENERAL.CANCEL']).subscribe((res) => {
      this.compras = [
        res['CONFIRMATION.ONLY_ASSEMBLY'],
        '0€ - 2.000€',
        '2.001€ - 3.500€',
        '3.501€ - 5.000€'
      ];
      if (this._centroService.centro.orderConfiguration.opcion_cocina) {
        this.compras = [
          res['CONFIRMATION.ONLY_ASSEMBLY'],
          res['TICKET.ASSEMBLY_KITCHEN'],
          '0€ - 2.000€',
          '2.001€ - 3.500€',
          '3.501€ - 5.000€'
        ];
      }
      this.compra = this.compras[ this._pedidoService.pedido.importeCompraFactor ];
    });
    this.centro = this._centroService.centro;
    this.centro_name = this.centro.name;
    this.active = 6;

    this.precioDistancia = this._pedidoService.pedido.precioDistancia;
    this.precioTransporte = this._pedidoService.pedido.precioFinalTransporte;
    this.precioMontaje = this._pedidoService.pedido.precioFinalMontaje;
    this.precioReciclaje = this._pedidoService.pedido.precioFinalReciclaje;

    if ( this._pedidoService.pedido.tipo === 0) {
      this.tipoServicio = 'Express';
    } else {
      this.translateService.get('DETALLE.RESERVE').subscribe((res: string) => {
        this.tipoServicio = res;
      });
    }
  }

  ngOnInit() {
  }

  actionPago() {
     this.router.navigate(['/pago']).then();
  }

  atras() {
    this.router.navigate((['/vannerDetalle', this._pedidoService.pedido.vanner])).then();
  }

  cancelarReserva() {
    const swal = this.anularSwal;
    this.translateService.get(
      ['TIPO.CANCEL_MESSAGE'])
      .subscribe( t => {
        const msg = t['TIPO.CANCEL_MESSAGE'];
        this.showAnular(msg, swal);
      });
  }

  showAnular(error: string, sAlert) {
    sAlert.text = error;
    console.log(error);
    sAlert.show().then();
  }

  confirmAnular() {
    this._pedidoService.inicializarPedido();
    this.router.navigate(['/dashboard']).then();
  }

}
