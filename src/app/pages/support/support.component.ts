import { Component, OnInit } from '@angular/core';
import {PagesComponent} from '../pages.component';
import {TranslateService} from '@ngx-translate/core';

@Component({
  selector: 'app-support',
  templateUrl: './support.component.html',
  styleUrls: ['./support.component.css']
})
export class SupportComponent implements OnInit {

  constructor(public pages: PagesComponent,
              public translateService: TranslateService) {
    this.translateService.get('SUPPORT.TITLE').subscribe((t: string) => {
      this.pages.title = t;
    });
  }

  ngOnInit() {
  }

}
