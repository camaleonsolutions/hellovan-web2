import {NgModule} from '@angular/core';
import {SharedModule} from '../shared/shared.module';
import {DashboardComponent} from './dashboard/dashboard.component';
import {VannersComponent} from './vanners/vanners.component';
import {ReservasComponent} from './reservas/reservas.component';
import {UsuariosComponent} from './usuarios/usuarios.component';
import {PagesComponent} from './pages.component';
import {PAGES_ROUTES} from './pages.routes';
import {TranslateModule} from "@ngx-translate/core";
import { MessagesComponent } from './messages/messages.component';
import { MiPerfilComponent } from './mi-perfil/mi-perfil.component';
import { NotificacionesComponent } from './notificaciones/notificaciones.component';
import { ValoracionesComponent } from './valoraciones/valoraciones.component';
import { SupportComponent } from './support/support.component';
import { TermsComponent } from './terms/terms.component';
import { ReservasActivasComponent } from './reservas-activas/reservas-activas.component';
import { ReservasEntregadasComponent } from './reservas-entregadas/reservas-entregadas.component';
import {CommonModule} from "@angular/common";
import { MapaComponent } from './mapa/mapa.component';
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import { FormComponent } from './form/form.component';
import {AngularFireDatabaseModule} from "angularfire2/database";
import { CentiroComponent } from './centiro/centiro.component';
import { TipoComponent } from './tipo/tipo.component';
import { TransportistasComponent } from './transportistas/transportistas.component';
import {SweetAlert2Module} from "@toverux/ngx-sweetalert2";
import { ConfirmationComponent } from './confirmation/confirmation.component';
import { FechaComponent } from './fecha/fecha.component';
import { VannerDetalleComponent } from './vanner-detalle/vanner-detalle.component';
import {StarRatingModule} from "angular-star-rating";
import { OpinionesVannerComponent } from './opiniones-vanner/opiniones-vanner.component';
import { PagoComponent } from './pago/pago.component';
import {InternationalPhoneModule} from "ng4-intl-phone";
import {AgmCoreModule, GoogleMapsAPIWrapper} from "@agm/core";
import {TextMaskModule} from "angular2-text-mask";


@NgModule({
  declarations: [
    PagesComponent,
    DashboardComponent,
    VannersComponent,
    ReservasComponent,
    UsuariosComponent,
    MessagesComponent,
    MiPerfilComponent,
    NotificacionesComponent,
    ValoracionesComponent,
    SupportComponent,
    TermsComponent,
    ReservasActivasComponent,
    ReservasEntregadasComponent,
    MapaComponent,
    FormComponent,
    CentiroComponent,
    TipoComponent,
    TransportistasComponent,
    ConfirmationComponent,
    FechaComponent,
    VannerDetalleComponent,
    OpinionesVannerComponent,
    PagoComponent
  ],
  exports: [
    DashboardComponent,
    VannersComponent,
    ReservasComponent,
    UsuariosComponent,
    MessagesComponent,
    MiPerfilComponent,
    NotificacionesComponent,
    ValoracionesComponent,
    SupportComponent,
    TermsComponent,
    ReservasActivasComponent,
    ReservasEntregadasComponent,
    MapaComponent,
    FormComponent,
    CentiroComponent,
    TipoComponent,
    TransportistasComponent,
    FechaComponent,
    VannerDetalleComponent,
    PagoComponent

  ],
  imports: [
    SharedModule,
    FormsModule,
    ReactiveFormsModule,
    AngularFireDatabaseModule,
    CommonModule,
    PAGES_ROUTES,
    TranslateModule,
    SweetAlert2Module,
    InternationalPhoneModule,
    AgmCoreModule,
    TextMaskModule,
    StarRatingModule.forRoot(),
  ]
})

export class PagesModule { }
