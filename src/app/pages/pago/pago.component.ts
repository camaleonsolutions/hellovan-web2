import {Component, OnInit, ViewChild} from '@angular/core';
import {PedidoService} from '../../services/pedido/pedido.service';
import {Router} from '@angular/router';
import {TranslateService} from '@ngx-translate/core';
import {CentroService} from '../../services/centro/centro.service';
import {PagesComponent} from '../pages.component';
import { StripeService, Elements, Element as StripeElement, ElementsOptions } from 'ngx-stripe';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {SwalComponent} from '@toverux/ngx-sweetalert2';
import {GlobalService, PagoService, VannerService} from '../../services/service.index';
import {Usuario} from '../../interfaces/usuario.interface';
import {Pedido} from '../../interfaces/pedido.interface';
import {Centro} from '../../interfaces/centro.interface';
import {Vanner} from '../../interfaces/vanner.interface';

@Component({
  selector: 'app-pago',
  templateUrl: './pago.component.html',
  styleUrls: ['./pago.component.css']
})
export class PagoComponent implements OnInit {


  @ViewChild('yeahSwal') private yeahSwal: SwalComponent;
  @ViewChild('loadingSwal') private loadingSwal: SwalComponent;
  @ViewChild('errorSwal') private errorSwal: SwalComponent;

  active: number;
  centro_name: string;
  elements: Elements;
  card: StripeElement;
  // optional parameters
  elementsOptions: ElementsOptions = {
    locale: 'es'
  };

  stripeTest: FormGroup;
  idioma: string;
  name: string;
  cardName: string;
  matricula: string;
  ref: string;
  vanner: any;
  usuario: Usuario;
  pedido: Pedido;
  centro: Centro;

  constructor(
              private fb: FormBuilder,
              public pages: PagesComponent,
              public translateService: TranslateService,
              public _pedidoService: PedidoService,
              public _centroService: CentroService,
              public router: Router,
              private stripeService: StripeService,
              private _vannerService: VannerService,
              private _pagoService: PagoService,
              public globalService: GlobalService) {
    this.active = 6;
    this.centro = this._centroService.centro;
    this.centro_name = this.centro.name;
    this.translateService.get('PAGO.TITLE').subscribe((t: string) => {
      this.pages.title = t;
    });
    this.idioma = this.globalService.language;
  }

  ngOnInit() {
    this.stripeTest = this.fb.group({
      name: ['', [Validators.required]]
    });

    this.stripeService.elements(this.elementsOptions)
      .subscribe(elements => {
        this.elements = elements;
        // Only mount the element the first time
        if (!this.card) {
          this.card = this.elements.create('card', {
            hidePostalCode: true,
            style: {
              base: {
                iconColor: '#666EE8',
                color: '#31325F',
                lineHeight: '40px',
                fontWeight: 300,
                fontFamily: '"Helvetica Neue", Helvetica, sans-serif',
                fontSize: '18px',
                '::placeholder': {
                  color: '#b6bec7'
                },
              },
              invalid: {
                color: '#fa755a',
                iconColor: '#fa755a'
              }
            }
          });
          this.card.mount('#card-element');
        }
      });
  }

  buy() {
    const name = this.stripeTest.get('name').value;
    if (this.stripeTest.valid) {
      this.stripeService
        .createToken(this.card, { name })
        .subscribe(result => {
          if (result.token) {
            // Use the token to create a charge or a customer
            // https://stripe.com/docs/charges

            const displayError = document.getElementById('card-errors');
            displayError.textContent = '';
            this.makePayment(result.token);
          } else if (result.error) {
            // Error creating the token
            console.log(result.error.message);
            const displayError = document.getElementById('card-errors');
            displayError.textContent = result.error.message;
          }
        });
    } else {
      const displayError = document.getElementById('card-errors');
      displayError.textContent = 'El nombre es requerido';
    }
  }

  makePayment(token: any) {
    const self = this;

    this.getDataVanner().then( () => {

      this.loadingSwal.showCancelButton = false;
      this.loadingSwal.showCloseButton = false;
      this.loadingSwal.showConfirmButton = false;
      this.loadingSwal.html = `<p>Procesando pago...</p>`;
      this.loadingSwal.show();

      const onSuccessPay = function(vanner: Vanner) {
        console.log('ON SUCCESS!!');
        self.loadingSwal.nativeSwal.close();
        self.showYeahAlert(vanner);
      };

      const onErrorPay = function(error: string) {
        console.error('ON ERROR!!');
        self.loadingSwal.nativeSwal.close();
        self.showErrorAlert(error);
      };

      this._pagoService.makeCardPayment(token, this.vanner, onSuccessPay, onErrorPay);
    });
  }

  getDataVanner() {
    return new Promise( resolve => {
      let idVanner = '';
      if (typeof this._pedidoService.pedido.vanner === 'string') {
        idVanner = this._pedidoService.pedido.vanner;
      } else {
        idVanner = this._pedidoService.pedido.vanner.key;
      }

      const vannerPromise = this._vannerService.getVanner(idVanner).subscribe(vanner => {
        console.log(vanner.payload.val());
        this.vanner = vanner.payload.val();
        if (this._pedidoService.pedido.tipo === 0) {
          // Express
          this._pedidoService.pedido.vanner = this.vanner;

        } else {
          // Reserva
          this._pedidoService.pedido.vanner = idVanner;
          if (this._pedidoService.pedido.hoy) {
            this._pedidoService.pedido.vanner = this.vanner;
          }
        }
        vannerPromise.unsubscribe();
        resolve();

      });
    });
  }

  showYeahAlert(vanner: Vanner) {
    console.log(vanner);
    this.ref = this._pedidoService.pedido.ref;
    this.translateService.get(['PAGO.COMPLETED_MESSAGE', 'PAGO.YOUR_VANNER', 'PEDIDOS.CAR_NUMBER']).subscribe((t: string) => {
      this.yeahSwal.imageUrl = 'assets/images/yeah_' + this.idioma + '.png';
      this.yeahSwal.imageWidth = 250;
      this.yeahSwal.html = `<p style="text-align: center;color: #4d4d4d;font-weight: 400;font-size: 1.3rem;">${t['PAGO.COMPLETED_MESSAGE']}</p>
          <p style="color: #01CC00;text-align: center;font-size: 1.5rem;font-weight: 600;">${this.ref}</p>
          <p style="color: #4d4d4d;text-align: center;font-size: 1.3rem;font-weight: 600;">${t['PAGO.YOUR_VANNER']}${ vanner.first_name } ${ vanner.last_name.charAt(0)}. <br> <span style="text-align: center;color: #4d4d4d;font-weight: 400;font-size: 1.3rem;">${ t['PEDIDOS.CAR_NUMBER']}: ${ vanner.van.matricula }</span></p>
`;
      this.yeahSwal.show().then();
    });

  }

  showErrorAlert(code: string) {
    let msg: string;
    if (code === 'Your card number is incorrect') {
      msg = 'PAGO.ERROR_CARD_INCORRECT';
    } else if (code === 'Your card\'s number is invalid') {
      msg = 'PAGO.ERROR_INCORRECT_NUMBER';
    } else if (code === 'Could not find payment information') {
      msg = 'PAGO.ERROR_NO_PAYMENT_INFO';
    } else if (code === 'Your card\'s security code is invalid') {
      msg = 'PAGO.ERROR_INVALID_CARD_SECURITY';
    } else if (code === 'Your card\'s expiration year is invalid') {
      msg = 'PAGO.ERROR_INVALID_CARD_EXPIRIRATION';
    } else if (code === 'Your card\'s expiration month is invalid') {
      msg = 'PAGO.ERROR_INCORRECT_MONTH';
    } else if (code === 'card_declined') {
      msg = 'PAGO.ERROR_CARD_DECLINED';
    } else if (code === 'incorrect_cvc') {
      msg = 'PAGO.ERROR_INVALID_CVV';
    } else if (code === 'expired_card') {
      msg = 'PAGO.ERROR_EXPIRED_CARD';
    } else if (code === 'incorrect_number') {
      msg = 'PAGO.ERROR_INCORRECT_NUMBER';
    } else if (code === 'invalid_expiry_year') {
      msg = 'PAGO.ERROR_INVALID_EXPIRY_YEAR';
    } else if (code === 'invalid_expiry_month') {
      msg = 'PAGO.ERROR_INVALID_EXPIRY_MONTH';
    } else if (code === 'invalid_number') {
      msg = 'PAGO.ERROR_INVALID_NUMBER';
    } else if (code === 'processing_error') {
      msg = 'PAGO.ERROR_PROCESSING';
    } else if (code === 'otros') {
      msg = 'PAGO.ERROR_OTHER';
    } else if (code === 'invalid_call') {
      msg = 'PAGO.ERROR_INVALID_CALL';
    } else if (code === 'empty_name') {
      msg = 'PAGO.ERROR_EMPTY_NAME';
    } else if (code === 'error_rangos') {
      msg = 'PAGO.ERROR_MESSAGE';
    }   else {
      msg = 'PAGO.ERROR_DEFAULT';
    }
    this.translateService.get(['GENERAL.ERROR', msg, 'GENERAL.OK']).subscribe( t => {
      this.errorSwal.text = t[msg];
      this.errorSwal.show().then();
    });
    console.error(msg);
  }

  confirmSuccess() {
    console.log('Cerramos alerta');
  }

}
