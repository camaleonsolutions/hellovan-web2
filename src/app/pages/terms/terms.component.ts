import { Component, OnInit } from '@angular/core';
import {legal} from "../../terminos/legal";
import {PagesComponent} from "../pages.component";
import {TranslateService} from '@ngx-translate/core';

@Component({
  selector: 'app-terms',
  templateUrl: './terms.component.html',
  styleUrls: ['./terms.component.css']
})
export class TermsComponent implements OnInit {

  legal: string;
  constructor( public pages: PagesComponent,
                public translateService: TranslateService,) {
    this.legal = legal.html;
    this.translateService.get("TERMS.TITLE").subscribe((t:string) => {
      this.pages.title = t;
    });

  }

  ngOnInit() {
  }

}
