import {Component, NgZone, OnInit} from '@angular/core';
import {PagesComponent} from '../pages.component';
import {TranslateService} from '@ngx-translate/core';
import {} from '@types/googlemaps';
import {Centro} from '../../interfaces/centro.interface';
import {CentroService} from '../../services/service.index';
import {Router} from '@angular/router';
import { ViewChild, ElementRef } from '@angular/core';
import {FormControl} from '@angular/forms';
import {PedidoService} from '../../services/pedido/pedido.service';

import {MapsAPILoader, LatLngLiteral, AgmMap, GoogleMapsAPIWrapper, LatLng} from '@agm/core';

// declare const google: any;


@Component({
  selector: 'app-mapa',
  templateUrl: './mapa.component.html',
  styleUrls: ['./mapa.component.css']
})
export class MapaComponent implements OnInit {

  active: number;
  centro: Centro;
  centro_name: string;
  zonas: any;
  tipo: string;
  icon_pin: string;
  currentDirection: string;
  currentDirectionDetail: string;
  currentFullAddress: any;
  inLimits: boolean;
  dirPlaceHolder: string;
  visible: boolean;
  polygons: any;
  geocoder: any;
  centerPos;
  start: any = 'IKEA Malaga';
  end: any;
  ruta: any;
  searchActive: boolean;

  @ViewChild(AgmMap) map: any;

  public searchControl: FormControl;
  public zoom: number;

  @ViewChild('search')
  public searchElementRef: ElementRef;

  title = 'My first AGM project';
  lat: number;
  lng: number;

  constructor(public pages: PagesComponent,
              public translateService: TranslateService,
              private router: Router,
              private _centroService: CentroService,
              private pedidoService: PedidoService,
              private mapsAPILoader: MapsAPILoader,
              private ngZone: NgZone, ) {

    this.translateService.get('MAPA.TITLE').subscribe((t: string) => {
      this.pages.title = t;
    });

    this.visible = true;
    this.active = 2;
    this.searchActive = false;
    this.centro = this._centroService.centro;
    if (this.centro == null) {
      this.router.navigate(['/dashboard']).then();
    } else {
      this.centro_name = this.centro.name;
      this.zonas = this.centro.zonas;
    }
    this.icon_pin = 'assets/images/mapa/pin_map_green@3x.png';
    this.dirPlaceHolder = 'Calle y número';
    this.inLimits = false;
  }

  ngOnInit() {
    this.initMap(this.centro);
    this.initPlaces();
  }

  initPlaces() {
    this.searchControl = new FormControl();

    //this.setCurrentPosition();
    //load Places Autocomplete
    this.mapsAPILoader.load().then(() => {
      const autocomplete = new google.maps.places.Autocomplete(this.searchElementRef.nativeElement, {
        types: ['address']
      });
      autocomplete.addListener('place_changed', () => {
        this.ngZone.run(() => {
          //get the place result
          const place: google.maps.places.PlaceResult = autocomplete.getPlace();

          //verify result
          if (place.geometry === undefined || place.geometry === null) {
            return;
          }

          //set latitude, longitude and zoom
          this.lat = place.geometry.location.lat();
          this.lng = place.geometry.location.lng();
          this.zoom = 16;
          console.log(place.geometry.location);
          this.getReversePosition(place.geometry.location, this.geocoder, this.polygons);
          this.searchActive = false;
          this.searchElementRef.nativeElement.value = '';
        });
      });
    });
  }

  initMap( centro: Centro ): void {
    this.mapsAPILoader.load().then(() => {
      console.log('google script loaded');
      this.geocoder = new google.maps.Geocoder();
    });

    this.lat = centro.zone.lat;
    this.lng = centro.zone.lng;
    this.zoom = centro.zone.zoom;

    this.polygons = [];
    new Promise(resolve => {
      for (const zona in this.zonas) {
        if (this.zonas.hasOwnProperty(zona)) {
          const color = (this.zonas[zona].type === 'express') ? '#119912' : '#65009a';
          const zonePoints: Array<LatLngLiteral> = [];
          for (const key in  this.zonas[zona].polygon) {
            if (this.zonas[zona].polygon.hasOwnProperty(key)) {
              zonePoints.push(<LatLngLiteral>this.zonas[zona].polygon[key]);
            }
          }

          const poly = this.map._mapsWrapper.createPolygon({
            paths: zonePoints,
            strokeColor: color,
            strokeOpacity: 0.01,
            strokeWeight: 1,
            fillColor: color,
            fillOpacity: 0.2,
            visible: true,
            }).then((polygon: any) => {
            this.polygons.push([zona, polygon, this.zonas[zona].type]);
          });
        }
      }
      resolve();
    }).then( () => {
      //this.visible = this.setVisibility(this.visible, this.polygons);
      //this.eventIdler(polygons);
    });
  }

  mapReady(map) {
    console.log('Ready map');
  }

  zoomChange(e) {
    this.visible = this.setVisibility(e);
    console.log(this.visible);
  }

  centerChange(e) {
    this.centerPos = e;
  }
  eventIdler() {
    console.log('idle');
    const self = this;
    this.map._mapsWrapper.getCenter().then( (centerPos) => {
      if (self.visible) {
        console.log('visible');
        let inLimits = false;
        self.polygons.forEach( (pol) => {
          const zona = pol[0];
          const result = self.map._mapsWrapper.containsLocation(centerPos, pol[1]);
          console.log(zona, result);
          if (result) {
            self.tipo = pol[2];
            self._centroService.currentZonaKey = self.zonas[zona].key;
            self.icon_pin = (pol[2] === 'express') ? 'assets/images/mapa/pin_map_green@3x.png' : 'assets/images/mapa/pin_map@3x.png';
            inLimits = true;
          }
        });
        if (!inLimits) {
          self.tipo = null;
          self.icon_pin = 'assets/images/mapa/pin_map_red@3x.png';
        }
      }
      self.getReversePosition(centerPos, this.geocoder, this.polygons);
    });
  }

  getReversePosition(centerPos: google.maps.LatLng, geocoder: google.maps.Geocoder, polygons) {
    const self = this;
    if (this.tipo != null) {
      console.log(this.tipo);
      this.geocoder.geocode({'location': centerPos}, function (results, status) {
        if (status.toString() === 'OK') {
          if (results[0]) {
            console.log(results[0]);
            const res: any = results[0].address_components;
            const direction: any = {
              administrativeArea: '',
              countryCode: '',
              countryName: '',
              locality: '',
              postalCode: '',
              subAdministrativeArea: '',
              subLocality: '',
              subThoroughfare: '',
              thoroughfare: ''
            };
            res.forEach( (r) => {
              // console.log(r);
              switch (r.types[0]) {
                case 'postal_code':
                  direction.postalCode = r.long_name;
                  break;
                case 'country':
                  direction.countryName = r.long_name;
                  direction.countryCode = r.short_name;
                  break;
                case 'administrative_area_level_1':
                  direction.administrativeArea = r.long_name;
                  break;
                case 'administrative_area_level_2':
                  direction.subAdministrativeArea = r.long_name;
                  break;
                case 'locality':
                  direction.locality = r.long_name;
                  break;
                case 'sublocality':
                  direction.subLocality = r.long_name;
                  break;
                case 'street_number':
                  direction.subThoroughfare = r.long_name;
                  break;
                case 'route':
                  direction.thoroughfare = r.long_name;
                  break;
              }
              self.currentFullAddress = direction;
              const thoroughfare = direction.thoroughfare;

              if (thoroughfare === null || thoroughfare === 'undefined' || thoroughfare === '' || thoroughfare === 'Unnamed Road') {
                self.currentDirection = null;
                self.dirPlaceHolder = 'Calle sin nombre o inválida';
                self.inLimits = false;
                return;
              } else {
                self.currentDirection = results[0].formatted_address;
                self.end = self.currentDirection;
                self.inLimits = true;
                self.calculateAndDisplayRoute();
              }
            });
          } else {
            this.currentDirection = null;
            this.dirPlaceHolder = 'Fuera de límites';
            self.inLimits = false;
          }
        }else {
          // No results
          self.currentDirection = null;
          self.dirPlaceHolder = 'Fuera de límites';
          self.inLimits = false;
        }
      });
    }
  }

  calculateAndDisplayRoute() {
    const self = this;
    const directionsService = new google.maps.DirectionsService;
    directionsService.route({
      origin: self.start,
      destination: self.end,
      travelMode: google.maps.TravelMode.DRIVING
    }, function(response, status) {
      if (status.toString() === 'OK') {
        self.ruta = response.routes[0];
      }
    });
  }


  private setVisibility(currentZoom) {
    let visible: boolean;
    if (currentZoom > 8) {
      visible = true;
    } else {
      visible = false;
    }
    this.polygons.forEach( (pol) => {
      pol[1].setVisible(visible);
    });
    return visible;
  }


  setCurrentPosition() {
    if ('geolocation' in navigator) {
      navigator.geolocation.getCurrentPosition((position) => {
        this.lat = position.coords.latitude;
        this.lng = position.coords.longitude;
        this.zoom = 12;
        this.getReversePosition(new google.maps.LatLng(this.lat, this.lng), this.geocoder, this.polygons);
      });
    }
  }

  atras() {
    this.router.navigate(['/dashboard']).then();
    this.visible = true;
  }

  siguiente() {
    if (!this.inLimits) {
      return;
    }

    this.pedidoService.datosUbicacionPedido = {
      ubicacion: {
        direction: this.currentDirection || null,
        detalleDirection: this.currentDirectionDetail || null,
        fullDireccion:  this.currentFullAddress || null,
        tipo: this.tipo
      },
      centro: this.centro,
      ruta: this.ruta
    };
    console.log(this.pedidoService.datosUbicacionPedido);
    this.router.navigate(['/form']).then();
  }



}





/*

  @ViewChild('search') public search: ElementRef;

  componentForm: any;
  autocomplete: any;
  active: number;
  centro: Centro;
  centro_name: string;
  showMenu: boolean;
  tipo: string;
  icon_pin: string;
  currentDirection: string;
  currentDirectionDetail: string;
  currentFullAddress: any;
  dirPlaceHolder: string;
  inLimits: boolean;
  start: any = 'IKEA Malaga';
  end: any;
  ruta: any;
  zonas: any;
  searchActive: boolean;
  visible: boolean;



  private map: any;
  constructor(public pages: PagesComponent,
              public translateService: TranslateService,
              private router: Router,
              private _centroService: CentroService,
              private pedidoService: PedidoService) {
    this.translateService.get('MAPA.TITLE').subscribe((t: string) => {
      this.pages.title = t;
    });
    this.active = 2;
    this.searchActive = false;
    this.centro = this._centroService.centro;
    if (this.centro == null) {
      this.router.navigate(['/dashboard']).then();
    } else {
      this.centro_name = this.centro.name;
      this.zonas = this.centro.zonas;
    }
    this.icon_pin = 'assets/images/mapa/pin_map_green@3x.png';
    this.dirPlaceHolder = 'Calle y número';
    this.inLimits = false;
    this.componentForm = {
      street_number: 'short_name',
      route: 'long_name',
      locality: 'long_name',
      administrative_area_level_1: 'short_name',
      country: 'long_name',
      postal_code: 'short_name'
    };
  }

  ngOnInit() {
    this.initMap(this.centro);
}

geatAddressOnChange(e, searchCtrl: FormControl) {
  console.log(e);
  console.log(searchCtrl.value);
}
getForwardPosition() {

}

atras() {
  this.router.navigate(['/dashboard']).then();
}

siguiente() {
  if (!this.inLimits) {
    return;
  }

  this.pedidoService.datosUbicacionPedido = {
    ubicacion: {
      direction: this.currentDirection || null,
      detalleDirection: this.currentDirectionDetail || null,
      fullDireccion:  this.currentFullAddress || null,
      tipo: this.tipo
    },
    centro: this.centro,
    ruta: this.ruta
  };
  console.log(this.pedidoService.datosUbicacionPedido);
  this.router.navigate(['/form']).then();
}

initMap(centro): void {
  const mapCanvas = document.getElementById('map');
const latLng = new google.maps.LatLng(centro.zone.lat, centro.zone.lng);
this.visible = true;
const polygons = [];

this.map = new google.maps.Map(mapCanvas, {
  center: latLng,
  zoom: centro.zone.zoom + 1,
  mapTypeControl: false,
  streetViewControl: false,
  fullscreenControl: false,
});

this.addMarker(centro, latLng, this.map);

new Promise(resolve => {
  for (const zona in this.zonas) {
    if (this.zonas.hasOwnProperty(zona)) {
      const color = (this.zonas[zona].type === 'express') ? '#119912' : '#65009a';
      const zonePoints = [];
      for (const key in  this.zonas[zona].polygon) {
        if (this.zonas[zona].polygon.hasOwnProperty(key)) {
          zonePoints.push(this.zonas[zona].polygon[key]);
        }
      }
      const polygon = new google.maps.Polygon({
        paths: zonePoints,
        strokeColor: color,
        strokeOpacity: 0.01,
        fillColor: color,
      });
      polygon.setMap(this.map);
      const p = [zona, polygon, this.zonas[zona].type];
      polygons.push(p);
    }
  }
  resolve();
}).then(() => {
  this.visible = this.setVisibility(this.visible, polygons);
  this.eventIdler(polygons);
});
}

eventIdler(polygons: any) {
  const geocoder = new google.maps.Geocoder;
  const self = this;
  this.map.addListener('idle', function() {
    const centerPos = self.map.getCenter();
    if (self.visible) {
      let inLimits = false;
      polygons.forEach( (pol) => {
        const zona = pol[0];
        const result = google.maps.geometry.poly.containsLocation(centerPos, pol[1]);
        if (result) {
          self.tipo = pol[2];
          self._centroService.currentZonaKey = self.zonas[zona].key;
          self.icon_pin = (pol[2] === 'express') ? 'assets/images/mapa/pin_map_green@3x.png' : 'assets/images/mapa/pin_map@3x.png';
          inLimits = true;
        }
      });
      if (!inLimits) {
        self.tipo = null;
        self.icon_pin = 'assets/images/mapa/pin_map_red@3x.png';
      }
    }
    self.getReversePosition(centerPos, geocoder, polygons);
  });
}

addMarker(centro: Centro, latLng, map) {
  // Añadimos el marker en la posición del centro
  const contentString = centro.name;
  const infowindow = new google.maps.InfoWindow({
    content: contentString
  });
  const marker = new google.maps.Marker({
    position: latLng,
    animation: google.maps.Animation.DROP,
    map: this.map
  });
  marker.addListener('click', function() {
    map.setZoom(centro.zone.zoom + 3);
    infowindow.open(map, marker);
    map.setCenter(marker.getPosition());
  });
  infowindow.open(map, marker);
  // Cerramos el info windows del marker a los 4 segundos
  setTimeout( () => {
    infowindow.close(map, marker);
  }, 4000);
}

setVisibility(visible: boolean, polygons: any) {
  const self = this;
  this.map.addListener('zoom_changed', function() {
    const currentZoom = self.map.getZoom();
    if (currentZoom > 8) {
      visible = true;
    } else {
      visible = false;
    }
    polygons.forEach( (pol) => {
      pol[1].setVisible(visible);
    });
  });
  return visible;
}

getReversePosition(centerPos: google.maps.LatLng, geocoder: google.maps.Geocoder, polygon) {
  // console.log('TIPO', this.tipo);
  const self = this;
  if (this.tipo != null) {
    geocoder.geocode({'location': centerPos}, function (results, status) {
      // console.log('STATUS: ' + status);
      if (status.toString() === 'OK') {
        if (results[0]) {
          // console.log(results[0]);
          const res: any = results[0].address_components;
          const direction: any = {
            administrativeArea: '',
            countryCode: '',
            countryName: '',
            locality: '',
            postalCode: '',
            subAdministrativeArea: '',
            subLocality: '',
            subThoroughfare: '',
            thoroughfare: ''
          };

          res.forEach( (r) => {
            // console.log(r);
            switch (r.types[0]) {
              case 'postal_code':
                direction.postalCode = r.long_name;
                break;
              case 'country':
                direction.countryName = r.long_name;
                direction.countryCode = r.short_name;
                break;
              case 'administrative_area_level_1':
                direction.administrativeArea = r.long_name;
                break;
              case 'administrative_area_level_2':
                direction.subAdministrativeArea = r.long_name;
                break;
              case 'locality':
                direction.locality = r.long_name;
                break;
              case 'sublocality':
                direction.subLocality = r.long_name;
                break;
              case 'street_number':
                direction.subThoroughfare = r.long_name;
                break;
              case 'route':
                direction.thoroughfare = r.long_name;
                break;
            }
          });


          // console.log(direction);

          self.currentFullAddress = direction;
          const thoroughfare = direction.thoroughfare;

          if (thoroughfare === null || thoroughfare === 'undefined' || thoroughfare === '' || thoroughfare === 'Unnamed Road') {
            self.currentDirection = null;
            self.dirPlaceHolder = 'Calle sin nombre o inválida';
            self.inLimits = false;
            return;
          } else {
            self.currentDirection = results[0].formatted_address;
            self.end = self.currentDirection;
            self.inLimits = true;
            self.calculateAndDisplayRoute();
          }

        } else {
          // No results
          this.currentDirection = null;
          this.dirPlaceHolder = 'Fuera de límites';
          this.inLimits = false;
        }
      }
    });
  } else {
    this.currentDirection = null;
    this.dirPlaceHolder = 'Fuera de límites';
  }

}

calculateAndDisplayRoute() {
  const self = this;
  const directionsService = new google.maps.DirectionsService;
  directionsService.route({
    origin: self.start,
    destination: self.end,
    travelMode: 'DRIVING'
  }, function(response, status) {
    if (status === 'OK') {
      self.ruta = response.routes[0];
    }
  });
}
 */
