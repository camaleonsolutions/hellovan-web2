import { Component, OnInit } from '@angular/core';
import {PagesComponent} from "../pages.component";
import {TranslateService} from '@ngx-translate/core';
import {NotificationsService} from "../../services/notifications/notifications.service";
import {Notification} from "../../interfaces/notification.interface";
import {PedidoService} from "../../services/pedido/pedido.service";
import {VannerService} from "../../services/vanner/vanner.service";

@Component({
  selector: 'app-notificaciones',
  templateUrl: './notificaciones.component.html',
  styleUrls: ['./notificaciones.component.css']
})
export class NotificacionesComponent implements OnInit {

  notificaciones: any[];
  showDetalle: boolean;
  currentDetalle:any;
  loading: boolean;

  constructor(public pages: PagesComponent,
              public translateService: TranslateService,
              public _notService: NotificationsService,
              public _pedidosService: PedidoService,
              public _vannerService: VannerService,) {
    this.translateService.get("NOTIFICATIONS.TITLE").subscribe((t:string) => {
      this.pages.title = t;
    });
    this.notificaciones = [];
    this.showDetalle = false;
    this.loading = false;
    this.initNotifications();
  }

  ngOnInit() {
  }

  /**
   * Incia la carga de notificaciones recibidas por el usuario
   */
  initNotifications() {
    this._notService.getNotificaciones().subscribe( (snapshots) => {
      this.notificaciones = [];
      snapshots.forEach( (snapshot) => {
        const not: Notification = {};
        if (snapshot.payload.val().tipo === 'abono') {

          this.translateService.get(['NOTIFICATIONS.ORDER_PAYMENT', 'NOTIFICATIONS.ORDER_PAYMENT_MESSAGE']).subscribe((t: string) => {
            not.title = t['NOTIFICATIONS.ORDER_PAYMENT'] + ' ' + snapshot.payload.val().ref;
            not.message = t['NOTIFICATIONS.ORDER_PAYMENT_MESSAGE'] + ' ' + snapshot.payload.val().ref;
          });

        } else if (snapshot.payload.val().tipo === 'reserva') {

          this.translateService.get(['NOTIFICATIONS.NEW_ORDER', 'NOTIFICATIONS.NEW_ORDER_RESERVE_MESSAGE']).subscribe((t: string) => {
            not.title = t['NOTIFICATIONS.NEW_ORDER'] + ' ' + snapshot.payload.val().ref;
            not.message = t['NOTIFICATIONS.NEW_ORDER_RESERVE_MESSAGE'];
          });

        } else if (snapshot.payload.val().tipo === 'ahora') {

          this.translateService.get(['NOTIFICATIONS.NEW_ORDER', 'NOTIFICATIONS.NEW_ORDER_EXPRESS_MESSAGE']).subscribe((t: string) => {
            not.title = t['NOTIFICATIONS.NEW_ORDER'] + ' ' + snapshot.payload.val().ref;
            not.message = t['NOTIFICATIONS.NEW_ORDER_EXPRESS_MESSAGE'];
          });

        } else if (snapshot.payload.val().tipo === 'valora') {
          this.translateService.get(['NOTIFICATIONS.RATING_VANNER', 'NOTIFICATIONS.RATING_VANNER_MESSAGE']).subscribe((t: string) => {
            not.title = t['NOTIFICATIONS.RATING_VANNER'];
            not.message = t['NOTIFICATIONS.RATING_VANNER_MESSAGE'];
          });
          not.id_vanner = snapshot.payload.val().id_vanner;
          not.usuario = snapshot.payload.val().usuario;
        }
        not.tipo = snapshot.payload.val().tipo;
        not.ref = snapshot.payload.val().ref;
        not.id_pedido = snapshot.payload.val().id_pedido;
        not.key = snapshot.key;
        this.notificaciones.push(not);

      });
      return this.notificaciones.reverse();
    });
  }

  /**
   * Muestra el detalle de una notificación cuando se pulsa sobre ella.
   * @param not
   */
  verDetalle(not: Notification) {
    this.showDetalle = true;
    this.loading = true;
    const refPedido = this._pedidosService.getPedidoByKey(not.id_pedido).subscribe((res) => {
      const pedido: any = res;
      let detailPage: any;
      let datos: any;
      let vanner_id: any;
      refPedido.unsubscribe();
      datos = {
        pedido: pedido.payload.val(),
        not: not
      };
      if (not.tipo === 'ahora') {

        detailPage = 'modalExpress';
        vanner_id = pedido.payload.val().vanner.key;
        this.openDetalle(detailPage, datos);

      } else if (not.tipo === 'reserva') {

        detailPage = 'modalReserva';
        vanner_id = pedido.payload.val().vanner;
        const ref = this._vannerService.getVanner(vanner_id).subscribe(( resVanner ) => {
          // console.log(res);
          datos.vanner = resVanner.payload.val();
          ref.unsubscribe();
          this.openDetalle(detailPage, datos);
        });

      } else if (not.tipo === 'valora') {

        if (pedido.tipo === 0) {
          vanner_id = pedido.payload.val().vanner.key;
        } else {
          vanner_id = pedido.payload.val().vanner;
        }
        this.openDetalle(detailPage, datos);
      }
    });
  }

  /**
   * Elimina una notificación al pulsar el botón de eliminar de la misma
   * @param not
   */
  deleteNotification(not: any) {
    this._notService.deleteNotification(not.key).then();
  }

  /**
   * Abre la pantalla de detalle según el tipo de notificacion
   * @param detailPage
   * @param datos
   */
  openDetalle(detailPage: any, datos: any) {
    console.log(detailPage, datos);
    this.currentDetalle = datos;
    this.loading = false;
  }

  /**
   * Cerramos la vista del detalle del pedido
   */
  closeDetalle(){
    this.currentDetalle = null;
    this.loading = false;
    this.showDetalle = false;
  }


}
