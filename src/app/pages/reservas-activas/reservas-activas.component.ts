import { Component, OnInit } from '@angular/core';
import {PagesComponent} from '../pages.component';
import {TranslateService} from '@ngx-translate/core';
import {PedidoService} from '../../services/pedido/pedido.service';
import {NotificationsService} from '../../services/notifications/notifications.service';
import {Notification} from '../../interfaces/notification.interface';
import {VannerService} from '../../services/vanner/vanner.service';
import {Pedido} from '../../interfaces/pedido.interface';

@Component({
  selector: 'app-reservas-activas',
  templateUrl: './reservas-activas.component.html',
  styleUrls: ['./reservas-activas.component.css']
})
export class ReservasActivasComponent implements OnInit {


  pedidos: Pedido[];
  showDetalle: boolean;
  currentDetalle: Pedido;
  loading: boolean;

  constructor(public pages: PagesComponent,
              public translateService: TranslateService,
              public _pedidosService: PedidoService,
              public _vannerService: VannerService) {
    this.translateService.get('PEDIDOS.TITLE').subscribe((t: string) => {
      this.pages.title = t;
    });
    this.pedidos = [];
    this.showDetalle = false;
    this.getReservasActivas();
  }

  ngOnInit() {
  }

  /**
   * Obtiene las reservas activas del usuario
   */
  getReservasActivas() {
    this.loading = true;
    this._pedidosService.getPedidosActivos().subscribe( snapshots => {
      this.pedidos = [];
      const tempPedidos = [];
      const promises = [];
      snapshots.forEach( (snapshot) => {
        console.log(snapshot.key);
        let ped: Pedido;
        const promise = new Promise( resolve => {
          const refPedido = this._pedidosService.getPedidoByKey(snapshot.key).subscribe( pedido => {
            ped = pedido.payload.val();
            console.log(ped);
            if (ped.tipo === 1) {
              const refVanner = this._vannerService.getVanner(ped.vanner).subscribe(vanner => {
                refVanner.unsubscribe();
                ped.vanner = vanner.payload.val();
                resolve();
              });
            } else {
              resolve();
            }
            refPedido.unsubscribe();
          });
        }).then( () => {
          tempPedidos.push(ped);
        });
        promises.push( promise );
      });

      Promise.all(promises).then( () => {
        this.pedidos = tempPedidos.reverse();
        this.loading = false;
      });
    });
  }

  /**
   * Muestra el detalle de una reserva activa
   * @param not
   */
  verDetalle(pedido: Pedido) {
    this.showDetalle = true;
    this.loading = true;
    this.currentDetalle = pedido;
    this.loading = false;
  }

  /**
   * Cerramos la vista del detalle del pedido
   */
  closeDetalle(){
    this.currentDetalle = null;
    this.loading = false;
    this.showDetalle = false;
  }

}
