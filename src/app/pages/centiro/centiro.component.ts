import { Component, OnInit } from '@angular/core';
import {PagesComponent} from '../pages.component';
import {TranslateService} from '@ngx-translate/core';
import {PedidoService, CentroService} from '../../services/service.index';
import {Router} from '@angular/router';
import {UsuarioService} from "../../services/usuario/usuario.service";

@Component({
  selector: 'app-centiro',
  templateUrl: './centiro.component.html',
  styleUrls: ['./centiro.component.css']
})
export class CentiroComponent implements OnInit {

  active: number;
  centro_name: string;
  pedido:any;
  centro:any;

  constructor(public pages: PagesComponent,
              public translateService: TranslateService,
              public _pedidoService: PedidoService,
              public _centroService: CentroService,
              public _userService: UsuarioService,
              public router: Router) {
    this.translateService.get('CENTIRO.TITLE').subscribe((t: string) => {
      this.pages.title = t;
    });
    this.centro_name = this._centroService.centro.name;
    this.active = 3;
    this.pedido = this._pedidoService.pedido;
    this.centro = this._centroService.centro;

  }

  ngOnInit() {
  }

  atras() {
    this.router.navigate(['/form']);
  }

  siguiente() {
    if (!this._userService.userProfile.phoneNumber){
      console.error("NECESITAMOS AÑADIR LA OPCION DE SELECCIONAR EL TELEFONO SI NO LO TIENE EL USUARIO EN EL PERFIL");
      return;
    }

    if (this._pedidoService.datosUbicacionPedido.tipo === 'express'){
      if (this._pedidoService.pedido.montajeOption === true){
        this._pedidoService.pedido.tipo = 1;
        this.router.navigate(['/transportistas']);
      }else{
        this.router.navigate(['/tipo']);
      }
    }else{
      this._pedidoService.pedido.tipo = 1;
      this.router.navigate(['/transportistas']);
    }


  }

}
