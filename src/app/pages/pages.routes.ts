import {RouterModule, Routes} from '@angular/router';
import {PagesComponent} from './pages.component';
import {DashboardComponent} from './dashboard/dashboard.component';
import {LoginGuard} from '../services/guards/login.guard';
import {MessagesComponent} from './messages/messages.component';
import {MiPerfilComponent} from './mi-perfil/mi-perfil.component';
import {NotificacionesComponent} from './notificaciones/notificaciones.component';
import {ValoracionesComponent} from './valoraciones/valoraciones.component';
import {TermsComponent} from './terms/terms.component';
import {SupportComponent} from './support/support.component';
import {ReservasActivasComponent} from './reservas-activas/reservas-activas.component';
import {ReservasEntregadasComponent} from './reservas-entregadas/reservas-entregadas.component';
import {MapaComponent} from './mapa/mapa.component';
import {FormComponent} from './form/form.component';
import {CentiroComponent} from './centiro/centiro.component';
import {TipoComponent} from './tipo/tipo.component';
import {TransportistasComponent} from './transportistas/transportistas.component';
import {FechaComponent} from "./fecha/fecha.component";
import {VannerDetalleComponent} from "./vanner-detalle/vanner-detalle.component";
import {OpinionesVannerComponent} from "./opiniones-vanner/opiniones-vanner.component";
import {ConfirmationComponent} from "./confirmation/confirmation.component";
import {PagoComponent} from "./pago/pago.component";

const pagesRoutes: Routes = [

  { path: '',
    component: PagesComponent,
    canActivate: [LoginGuard],
    children: [
      { path: 'dashboard', component: DashboardComponent},
      { path: 'miPerfil', component: MiPerfilComponent },
      { path: 'messages', component: MessagesComponent },
      { path: 'terminos', component: TermsComponent },
      { path: 'support', component: SupportComponent },
      { path: 'reservasActivas', component: ReservasActivasComponent },
      { path: 'reservasEntregadas', component: ReservasEntregadasComponent },
      { path: 'mapa', component: MapaComponent },
      { path: 'form', component: FormComponent },
      { path: 'centiro', component: CentiroComponent },
      { path: 'tipo', component: TipoComponent },
      { path: 'fecha', component: FechaComponent },
      { path: 'transportistas', component: TransportistasComponent },
      { path: 'vannerDetalle/:id', component: VannerDetalleComponent },
      { path: 'notificaciones', component: NotificacionesComponent },
      { path: 'notificaciones/:id', component: MiPerfilComponent },
      { path: 'valoraciones', component: ValoracionesComponent },
      { path: 'opinionesVanner/:id', component: OpinionesVannerComponent },
      { path: 'confirmation', component: ConfirmationComponent },
      { path: 'pago', component: PagoComponent },
      { path: '', redirectTo: '/dashboard', pathMatch: 'full' }
    ]
  }
];

export const PAGES_ROUTES = RouterModule.forChild( pagesRoutes );
