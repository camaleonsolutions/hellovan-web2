import {Component, OnInit, ViewChild} from '@angular/core';
import {PagesComponent} from '../pages.component';
import {TranslateService} from '@ngx-translate/core';
import {CentroService} from '../../services/service.index';
import {PedidoService} from '../../services/service.index';
import {UsuarioService} from '../../services/service.index';
import * as moment from 'moment';
import {Router} from '@angular/router';
import {SwalComponent} from '@toverux/ngx-sweetalert2';

@Component({
  selector: 'app-tipo',
  templateUrl: './tipo.component.html',
  styleUrls: ['./tipo.component.css']
})
export class TipoComponent implements OnInit {

  @ViewChild('errorSwal') private errorSwal: SwalComponent;
  @ViewChild('anularSwal') private anularSwal: SwalComponent;
  @ViewChild('infoSwal') private infoSwal: SwalComponent;
  active: number;
  centro_name: string;
  centro: any;
  errorMessage = '';
  infoMessage = '';

  constructor(public pages: PagesComponent,
              public translateService: TranslateService,
              public _centroService: CentroService,
              public _pedidoService: PedidoService,
              public _userService: UsuarioService,
              public router: Router) {

    this.translateService.get('TIPO.TITLE').subscribe((t: string) => {
      this.pages.title = t;
    });

    this.centro = this._centroService.centro;
    this.centro_name = this.centro.name;
    this.active = 4;

  }

  ngOnInit() {
  }

  serviceSelection(service: string) {
    const swal = this.infoSwal;
    if (service === 'express') {
      this.showInfo('Servicio disponible en APP, recomendable usar desde tu tienda IKEA al realizar su compra, muchas gracias', swal);
    } else {
      if (!this._userService.userProfile.phoneNumber) {
        console.error('TODO: DEBEMOS INTEGRAR EL CHEQUEO DEL TELEFONO DEL USUARIO EN EL PERFIL.');
        //return;
      }
      this._pedidoService.pedido.tipo = 1;
      this.router.navigate(['/transportistas']).then();
    }


  }

  showError(error: string, sAlert) {
    sAlert.text = error;
    console.log(error);
    sAlert.show().then();
  }

  showAnular(error: string, sAlert) {
    sAlert.text = error;
    console.log(error);
    sAlert.show().then();
  }

  showInfo(msg: string, sAlert) {
    sAlert.text = msg;
    sAlert.show().then();
  }

  cancelarReserva() {
    const swal = this.anularSwal;
    this.translateService.get(
      ['TIPO.CANCEL_MESSAGE'])
      .subscribe( t => {
        const msg = t['TIPO.CANCEL_MESSAGE'];
        this.showAnular(msg, swal);
      });
  }

  confirmAnular() {
    this._pedidoService.inicializarPedido();
    this.router.navigate(['/dashboard']).then();
  }
  confirmSuccess() {
    console.log('OK success');
  }

  confirmInfo() {
    console.log('OK info');
  }
}
