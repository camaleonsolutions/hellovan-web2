import { Component, OnInit } from '@angular/core';
import {PagesComponent} from '../pages.component';
import {TranslateService} from '@ngx-translate/core';
import {CentroService, VannerService, PedidoService, UsuarioService, ChatService, ValoracionesService} from '../../services/service.index';
import {ActivatedRoute, Router} from '@angular/router';
import * as moment from 'moment';
import {Chat} from '../../interfaces/chat.interface';
import {Location} from "@angular/common";

@Component({
  selector: 'app-vanner-detalle',
  templateUrl: './vanner-detalle.component.html',
  styleUrls: ['./vanner-detalle.component.css']
})
export class VannerDetalleComponent implements OnInit {

  active: number;
  centro_name: string;
  centro: any;

  // Perfil Vanner
  vanner: any;
  key: any;
  fecha_alta: any;
  supervanner: boolean;
  valoraciones_msg: any;
  languages: any;
  private sub: any;

  // Valoraciones
  rating:any = 0;
  calidad:any = 0;
  entrega:any = 0;
  montaje:any = 0;
  limpieza:any = 0;
  opiniones:any[];

  constructor(public pages: PagesComponent,
              public translateService: TranslateService,
              public _centroService: CentroService,
              public _pedidoService: PedidoService,
              public _userService: UsuarioService,
              public _chatService: ChatService,
              public _valoracionesService: ValoracionesService,
              public route: ActivatedRoute,
              public router: Router,
              public _location: Location,
              private _vannersService: VannerService, ) {
    this.translateService.get('DETALLE.TITLE').subscribe((t: string) => {
      this.pages.title = t;
    });

    this.centro = this._centroService.centro;
    this.centro_name = this.centro.name;
    this.active = 5;

    this.sub = this.route.params.subscribe(params => {
      this.key = params['id'];
      console.log(this.key);
      this.getValoraciones(this.key);
      this.getVannerData();

    });
  }


  ngOnInit() {
  }

  errorHandler(event) {
    console.debug(event);
    event.target.src = "assets/images/default_avatar.png";
  }

  getValoraciones(key: string) {
    console.log("GET VALORACIONES");
    this._valoracionesService.getValoracionesVannerByKey(key).subscribe((res) => {
      this.opiniones = res;
      this.opiniones.reverse();
    });
  }

  getVannerData() {
    console.log('Get vanner data');
    this._vannersService.getVanner(this.key).subscribe((res) => {

      this.vanner = res.payload.val();
      this.fecha_alta = moment(this.vanner.fecha_alta).format('ll');
      this.languages = new Array();
      // Languages
      if (res.payload.val().languages != null) {
        for (const l in res.payload.val().languages) {
          if (res.payload.val().languages.hasOwnProperty(l)) {
            this.languages.push(l);
          }
        }
      }

      if (this.vanner.num_opiniones > 0) {
        this.supervanner = this.calculateSuper(this.vanner);
        this.translateService.get('DETALLE.NUM_RATING_MESSAGE', {value : this.vanner.num_opiniones}).subscribe((result: string) => {
          this.valoraciones_msg = result;
        });
      } else {
        this.rating = 0;
      }

      this.configureRatings();
    });
  }

  configureRatings() {
    if (this.vanner.num_opiniones > 0){
      this.rating =  ((this.vanner.rating_calidad +  this.vanner.rating_entrega + this.vanner.rating_montaje + this.vanner.rating_limpieza) / this.vanner.num_opiniones) / 4;

      if (this.rating >= 4.95 ){
        this.rating = 5.0;
      }else if (this.rating >= 4.9 && this.rating < 4.95){
        this.rating = 4.9;
      }

      this.calidad = this.vanner.rating_calidad / this.vanner.num_opiniones;
      if (this.calidad >= 4.95 ){
        this.calidad = 5.0;
      }else if (this.calidad >= 4.9 && this.calidad < 4.95){
        this.calidad = 4.9;
      }
      this.entrega = this.vanner.rating_entrega / this.vanner.num_opiniones;
      if (this.entrega >= 4.95 ){
        this.entrega = 5.0;
      }else if (this.entrega >= 4.9 && this.entrega < 4.95){
        this.entrega = 4.9;
      }
      this.montaje = this.vanner.rating_montaje / this.vanner.num_opiniones;
      if (this.montaje >= 4.95 ){
        this.montaje = 5.0;
      }else if (this.montaje >= 4.9 && this.montaje < 4.95){
        this.montaje = 4.9;
      }
      this.limpieza = this.vanner.rating_limpieza / this.vanner.num_opiniones;
      if (this.limpieza >= 4.95 ){
        this.limpieza = 5.0;
      }else if (this.limpieza >= 4.9 && this.limpieza < 4.95){
        this.limpieza = 4.9;
      }
    }
  }
  calculateSuper(vanner: any) {
    if (vanner.num_opiniones >= 10 && vanner.entregas >= 10) {
      const r = ((vanner.rating_calidad +  vanner.rating_entrega + vanner.rating_montaje + vanner.rating_limpieza) / vanner.num_opiniones) / 4;
      return r >= 4.95;
    }
    return false;
  }

  reservar() {
    // console.log("Reservar");
    this._pedidoService.pedido.vanner = this.key;
    this.router.navigate(['/confirmation']).then();
  }


  openChat(vannerKey: string) {
    /*
    // this.loading = true;
    const chat: Chat = {
      vanner: vannerKey,
      user: this._userService.userProfile.key,
      date: new Date().getTime(),
      pending: 0,
      last_message_uid: '0',
      datos_usuario: null,
      datos_vanner: null
    };
    console.log('VANNER-KEY', vannerKey);
    const promise = this._chatService.getChatsByUid(this._userService.userProfile.key).subscribe((res) => {

      const resultado = res;
      let currentChat: any = null;
      resultado.forEach((item) => {
        if (item.vanner === vannerKey) {
          // existe previo
          console.log('existe este chat');
          currentChat = item;
          currentChat.key = item.key;  //Comprobar si obtenemos la key
        }
      });

      if (currentChat != null && currentChat !== 'undefined') {

        const vanPromise = this._vannersService.getVanner(currentChat.vanner).subscribe((vanner) => {
          currentChat.datos_vanner = vanner;

          const userPromise = this._userService.getUserprofile(currentChat.user).subscribe((user) => {
            currentChat.datos_usuario = user;
            userPromise.unsubscribe();
            console.log(currentChat);
            localStorage.setItem('chatData', JSON.stringify(currentChat));
            this.router.navigate(['/messages']).then();
          });
          vanPromise.unsubscribe();
        });

      } else {
        this._chatService.createChatUser(chat).then((key) => {
          console.log(chat);
          chat.key = key;

          const vPromise = this._vannersService.getVanner(chat.vanner).subscribe((vanner) => {
            chat.datos_vanner = vanner;
            if (vPromise != null) {
              vPromise.unsubscribe();
            }
            const userPromise = this._userService.getUserprofile(chat.user).subscribe((user) => {
              chat.datos_usuario = user;
              if (userPromise !== 'undefined') {
                userPromise.unsubscribe();
              }

              console.log(chat);
              localStorage.setItem('chatData', JSON.stringify(chat));
              this.router.navigate(['/messages']).then();

            });

          });
        });
      }

      promise.unsubscribe();
    });
*/
  }

  atras() {
    this._location.back();
  }

}
