import { Component, OnInit } from '@angular/core';
import {PagesComponent} from '../pages.component';
import {TranslateService} from '@ngx-translate/core';
import {CentroService, PedidoService, UsuarioService} from '../../services/service.index';
import {Router} from '@angular/router';
import * as moment from 'moment';
import {GlobalService} from '../../services/shared/global.service';
import {Location} from '@angular/common';

@Component({
  selector: 'app-fecha',
  templateUrl: './fecha.component.html',
  styleUrls: ['./fecha.component.css']
})
export class FechaComponent implements OnInit {

  active: number;
  centro_name: string;
  centro: any;
  fecha: number;
  fechas: string[];
  ff: any[];
  optionHora: any;
  hora = 0;
  rangoHorasCompletos: string[] = [];
  rangoHora: any;
  showAceptar: boolean;
  hoyMismo = false;
  rangoHoras: string[] = [];
  rango: string[];

  constructor(public pages: PagesComponent,
              public translateService: TranslateService,
              public _centroService: CentroService,
              public _pedidoService: PedidoService,
              public _globalService: GlobalService,
              public router: Router,
              public _location: Location) {

    this.translateService.get('FECHA.TITLE').subscribe((t: string) => {
      this.pages.title = t;
    });
    this.translateService.get(['RANGOS.RANGO_0', 'RANGOS.RANGO_1', 'RANGOS.RANGO_2', 'RANGOS.RANGO_3'])
      .subscribe(t => {
      this.rangoHoras = [
        t['RANGOS.RANGO_0'], t['RANGOS.RANGO_1'], t['RANGOS.RANGO_2'], t['RANGOS.RANGO_3']
      ];
      this.rangoHorasCompletos = this.rangoHoras;
    });
    moment.locale(this._globalService.language);
    this.centro = this._centroService.centro;
    this.centro_name = this.centro.name;
    this.active = 4;
    this.optionHora = 'cualquier';
    this._pedidoService.pedido.hoy = false;
    this.rango = this.rangoHoras;
    this.fechas = this.getOpenDeliveryDays();
    if (this.fechas.length > 2) {
      this.fecha = 1;
    } else {
      this.fecha = 0;
    }
  }

  ngOnInit() {
  }

  /**
   * Obtenemos los días disponibles para las reservas del calendario del centro
   * @returns {string[]}
   */
  getOpenDeliveryDays() {
    const dates: string[] = [];
    this.ff = [];
    const calendarioCentro = Object.keys(this.centro.closed_calendar).map(function(key) {
      return key;
    });
    const hoy = moment().format( 'DD-MM-YYYY');
    if (calendarioCentro.indexOf(hoy) < 0) {
      const ahora = moment();
      const startTime = moment('10:00', 'HH:mm');
      const endTime = moment('22:00', 'HH:mm');
      const amIBetween = ahora.isBetween(startTime , endTime);
      if (amIBetween) {
        this.translateService.get(['FECHAS.HOY_ANTES']).subscribe(t => {
          dates.push(t['FECHAS.HOY_ANTES']);
        });
        this.ff.push(hoy);
      }
    }
    let date2: any = moment();
    let date: any = moment();
    for (let i = 1; i <= 2; i++) {
      let ok = 0;
      let count = 1;
      while (ok < 1) {
        if (i === 1) {
          date = moment().add(count, 'days').format('DD-MM-YYYY');
        } else {
          date = date2.add(1, 'days').format('DD-MM-YYYY');
        }
        if (calendarioCentro.indexOf(date) < 0) {
          ok = 1;
          date2 = moment(date, 'DD-MM-YYYY');
          const f = date2.format('ddd D MMMM');
          dates.push(f);
          this.ff.push(date2.format('DD-MM-YYYY'));
        }
        count++;
      }
    }
    return dates;
  }

  /**
   * Limpia las horas pasadas para el selector
   */
  limpiaHorasPasadas() {

    this.rangoHoras = [];
    const ahora = moment();
    const ma1Start = moment('10:00', 'HH:mm');
    const ma1End = moment('13:00', 'HH:mm');
    const inMan1 = ahora.isBetween(ma1Start , ma1End);
    if (inMan1) {
      console.log('En mañana 1');
      this.translateService.get(['RANGOS.RANGO_0', 'RANGOS.RANGO_1', 'RANGOS.RANGO_2', 'RANGOS.RANGO_3']).subscribe(t => {
        this.rangoHoras = [
          t['RANGOS.RANGO_0'], t['RANGOS.RANGO_1'], t['RANGOS.RANGO_2'], t['RANGOS.RANGO_3']
        ];
      });
    }
    const ma2Start = moment('13:00', 'HH:mm');
    const ma2End = moment('16:00', 'HH:mm');
    const inMan2 = ahora.isBetween(ma2Start , ma2End);
    if (inMan2) {
      console.log('En mañana 2');
      this.translateService.get(['RANGOS.RANGO_0', 'RANGOS.RANGO_1', 'RANGOS.RANGO_2', 'RANGOS.RANGO_3']).subscribe(t => {
        this.rangoHoras = [
          t['RANGOS.RANGO_1'], t['RANGOS.RANGO_2'], t['RANGOS.RANGO_3']
        ];
      });
    }
    const tarde1Start = moment('16:00', 'HH:mm');
    const tarde1End = moment('19:00', 'HH:mm');
    const inTarde1 = ahora.isBetween(tarde1Start , tarde1End);
    if (inTarde1) {
      console.log('En tarde 1');
      this.translateService.get(['RANGOS.RANGO_0', 'RANGOS.RANGO_1', 'RANGOS.RANGO_2', 'RANGOS.RANGO_3']).subscribe(t => {
        this.rangoHoras = [
          t['RANGOS.RANGO_2'], t['RANGOS.RANGO_3']
        ];
      });
    }
    const tarde2Start = moment('19:00', 'HH:mm');
    const tarde2End = moment('22:00', 'HH:mm');
    const inTarde2 = ahora.isBetween(tarde2Start , tarde2End);
    if (inTarde2) {
      console.log('En tarde 2');
      this.translateService.get(['RANGOS.RANGO_0', 'RANGOS.RANGO_1', 'RANGOS.RANGO_2', 'RANGOS.RANGO_3']).subscribe(t => {
        this.rangoHoras = [
          t['RANGOS.RANGO_3']
        ];
      });
    }
  }
  /**
   * Cuando seleccionamos un día en el selector
   * @param event
   */
  changeSelectFecha(e) {
    const event = Number(e);
    if (this.ff.length === 3) {
      console.log('tres');
      if (event === 0) {
        this.hoyMismo = true;
        this._pedidoService.pedido.hoy = true;
        this.optionHora = 'hora';
        this.limpiaHorasPasadas();
      } else {
        this.hoyMismo = false;
        this.rangoHoras = this.rangoHorasCompletos;
        this.optionHora = 'cualquier';
        this._pedidoService.pedido.hoy = false;
      }
    }
  }

  /**
   * Eventos de selector de horas
   * @param event
   */
  changeSelectHora(event) {
    console.log('change select', JSON.stringify(event));
  }

  /**
   * Pulsamos el boton siguiente
   */
  siguiente() {
    const fecha = this.ff[this.fecha];
    let h = this.hora;
    if (this.rangoHoras.length === 3) {
      h +=  1;
    } else if (this.rangoHoras.length === 2) {
      h += 2;
    } else if (this.rangoHoras.length === 1) {
      h += 3;
    }

    this._pedidoService.pedido.fechaReserva = {
      fecha: {
        text: this.fechas[this.fecha],
        value: fecha
      },
      hora: this.optionHora,
      rangoHora: {
        value: h,
        text: this.rango[h],
      }
    };
    this.router.navigate(['/transportistas']).then();
  }

  atras() {
    this._location.back();
  }

}
