import { Component, OnInit } from '@angular/core';
import {PagesComponent} from "../pages.component";
import {TranslateService} from '@ngx-translate/core';
import {UsuarioService, ValoracionesService, CentroService} from "../../services/service.index";
import {Usuario} from "../../models/usuario.model";

@Component({
  selector: 'app-valoraciones',
  templateUrl: './valoraciones.component.html',
  styleUrls: ['./valoraciones.component.css']
})
export class ValoracionesComponent implements OnInit {

  usuario: Usuario;
  valoraciones: any[];

  constructor(public pages: PagesComponent,
              public translateService: TranslateService,
              public _centroService: CentroService,
              public _userService: UsuarioService,
              public _valoracionesService: ValoracionesService) {
    this.usuario = this._userService.userProfile;
    this.valoraciones = [];
    this.translateService.get("RATINGS.TITLE").subscribe((t:string) => {
      this.pages.title = t;
    });
    this.initValoraciones();
  }

  ngOnInit() {
  }

  /**
   * Inciam la carga de las valoraciones emitidas por el usuario
   */
  initValoraciones() {
    this._valoracionesService.getValoracionesUserByKey(this.usuario.key).subscribe((res) => {
      this.valoraciones = [];
      res.forEach( (valoracion) => {
        this.valoraciones.push(valoracion.payload.val());
      });
    });
  }

}
