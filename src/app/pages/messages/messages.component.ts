import { AfterViewInit, Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import {PagesComponent} from '../pages.component';
import {TranslateService} from '@ngx-translate/core';
import {ChatService} from '../../services/service.index';
import {Chat} from '../../interfaces/chat.interface';
import {VannerService} from '../../services/vanner/vanner.service';
import {UsuarioService} from '../../services/usuario/usuario.service';
declare function initChat();

import * as moment from 'moment';

@Component({
  selector: 'app-messages',
  templateUrl: './messages.component.html',
  styleUrls: ['./messages.component.css']
})
export class MessagesComponent implements OnInit, AfterViewInit {

  chats: Chat[];
  chat: Chat;
  messages: any[];
  message = '';
  first_message: boolean;
  procesing: boolean;

  @ViewChild('chatlist') chatlist: ElementRef;
  constructor(public pages: PagesComponent,
              public translateService: TranslateService,
              public _chatService: ChatService,
              public _vannerService: VannerService,
              public _userService: UsuarioService,
              ) {

    this.translateService.get('MESSAGES.TITLE').subscribe((t: string) => {
      this.pages.title = t;
    });
    this.first_message = false;
    this.procesing = true;
    this.getChats();
  }

  ngOnInit() {
  }

  ngAfterViewInit() {
    initChat();
    this.scrollToBottom();
  }

  scrollToBottom(): void {
    setTimeout( () => {
      console.log('scroll');
      const elements = document.getElementsByClassName('last');
      if (elements.length === 0) {
        return;
      }
      elements[0].scrollIntoView();
      this.procesing = false;
    }, 300);
  }

  /**
   * Obtenemos las conversaciones
   */
  getChats() {
    const getCHatPromise = this._chatService.getChats().subscribe( (snapshots) => {
      this.chats = [];
      const localChatsData = [];
      const proms = [];
      snapshots.forEach( (snapshot) => {
        if (snapshot.payload.val().last_message_uid !== 0) {
          const pr = new Promise( resolve => {
            const chat: Chat = {};
            const promises = [];
            const lastmessagePromise = new Promise( resolve0 => {
              const lmp = this._chatService.getMessage(snapshot.payload.val().last_message_uid, snapshot.key).subscribe((msg) => {
                console.log(JSON.stringify(msg));
                chat.date = snapshot.payload.val().date;
                chat.key = snapshot.key;
                chat.last_message_uid = snapshot.payload.val().last_message_uid;
                chat.last_message = msg;
                chat.user = snapshot.payload.val().user;
                chat.pending = snapshot.payload.val().pending;
                chat.vanner = snapshot.payload.val().vanner;
                lmp.unsubscribe();
                const vannerPromise = new Promise(resolve1 => {
                  const vp = this._vannerService.getVanner(chat.vanner).subscribe((vanner) => {
                    chat.datos_vanner = vanner;
                    vp.unsubscribe();
                    this._vannerService.getAvatar(vanner.payload.val().avatar).then( (url) => {
                      chat.vanner_avatar = url;
                      resolve1();
                    });
                  });
                });

                promises.push(vannerPromise);

                const userPromise = new Promise( resolve2 => {
                  const up = this._userService.getUserprofile(chat.user).subscribe((user) => {
                    chat.datos_usuario = user;
                    up.unsubscribe();
                    resolve2();
                  });
                });
                promises.push(userPromise);

                Promise.all(promises).then( () => {
                  resolve0(); // lmp
                });

              });
            }).then( () => {
              localChatsData.push(chat);
              resolve(); // pr
            });
          });
          proms.push(pr);
        }
      });
      Promise.all(proms).then( () => {
        console.log('Chats completed!!');
        this.chats = localChatsData.reverse();
        getCHatPromise.unsubscribe();
        this.openChat(this.chats[0]);
      });
    });
  }

  /**
   * Abrimos el chat de la conversacion en curso
   * @param {Chat} chat
   */
  openChat(chat: Chat) {
    chat.pending = 0;
    this._chatService.resetPendings(chat);
    this.chat = chat;
    this.processMessages();

  }

  processMessages() {
    this.procesing = true;
    console.log('PROCESS MESSAGES');
    console.log(this.chat);
    this._chatService.getMessages(this.chat).subscribe((messages) => {
      console.log(messages[0].payload.val());
      this.messages = [];
      for (let i = 0; i < messages.length; i++) {
        console.log('MESSAGE: -> ', messages[i]);
        const msg = messages[i];
        let msgDt: any = moment(msg.payload.val().date).format('YYYY-MM-DD');
        msgDt = moment(msgDt, ['YYYY-MM-DD']);
        let prevMsg;
        let prevMsgDt: any;

        const m: any = {
          showDate: true,
          type: 'user',
          position: 'left',
          body: 'demo',
          timestamp: 0
        };

        if ( i  === 0) {
          m.showDate = true;
        } else {
          prevMsg = messages[i - 1];
          prevMsgDt = moment(prevMsg.payload.val().date).format('YYYY-MM-DD');
          prevMsgDt = moment(prevMsgDt, ['YYYY-MM-DD']);

          if (msgDt.isSame(prevMsgDt, 'day')) {
            m.showDate = false;
          } else {
            m.showDate = true;
          }
        }

        if (msg.payload.val().type === 'user') {
          m.position = 'right';
        } else {
          m.position = 'left';
        }
        m.body = msg.payload.val().message;
        m.timestamp = msg.payload.val().date;
        this.messages.push(m);
      }
      initChat();
      this.scrollToBottom();
    });
  }

  sendMessage() {

    if (this.message === '') {
      return;
    }
    this.addMessage('right', this.message);
    this.message = '';
  }

  /**
   * Añade mensaje a Base de datos
   * @param position
   * @param msg
   */
  addMessage(position, msg) {
    if (this.first_message === true) {
      this._chatService.createChatVanner(this.chat).then( () => {
        this.first_message = false;
        this.chat.last_message_uid = '1';
       this._chatService.addMessage(this.chat.user, this.chat, msg);
      });
    } else {
      console.log(this.chat);
      this._chatService.addMessage(this.chat.user, this.chat, msg).then( () => {
        initChat();
        this.scrollToBottom();
      });
    }
  }
}
