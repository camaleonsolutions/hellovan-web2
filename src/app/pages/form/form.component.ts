import { Component, OnInit } from '@angular/core';
import {PagesComponent} from '../pages.component';
import {TranslateService} from '@ngx-translate/core';
import {PedidoService} from '../../services/pedido/pedido.service';
import {CentroService} from '../../services/centro/centro.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.css']
})
export class FormComponent implements OnInit {

  franjas: any;
  tr: any[];
  importePax: any;
  paxOption: boolean;

  mueblesOption: boolean;
  active: number;
  compra = 0;
  importeMontaje: any = '';

  datosPedido: any[];
  precioFinal: number;
  total_sofa: number;
  total_sillon: number;
  total_complemento: number;

  total_sofa_usado: number;
  total_colchon_usado: number;
  total_mgrande_usado: number;
  total_mpeq_usado: number;

  total_medicion: number;
  metros_metod: number;
  metros_knoxhult: number;
  importe_metod: number;
  importe_knoxhult: number;

  metros_cocina: number;

  centro_name: string;

  constructor(public pages: PagesComponent,
              public translateService: TranslateService,
              public _pedidoService: PedidoService,
              public _centroService: CentroService,
              public router: Router) {

    this.mueblesOption = false;
    this.paxOption = false;
    this.active = 3;
    this.centro_name = this._centroService.centro.name;
    this.translateService.get('TICKET.TITLE').subscribe((t: string) => {
      this.pages.title = t;
    });
    this.compra = 1;
    this.translateService.get(['CONFIRMATION.ONLY_ASSEMBLY', 'COCINAS.ASSEMBLY_KITCHEN']).subscribe((res) => {
      this.tr = res;
      this.franjas = [
        {description: res['CONFIRMATION.ONLY_ASSEMBLY']},
        {description: '0€ - 2.000€'},
        {description: '2.001€ - 3.500€'},
        {description: '3.501€ - 5.000€'},
      ];
      if (this._centroService.centro.orderConfiguration.opcion_cocina) {
        this.franjas = [
          {description: res['CONFIRMATION.ONLY_ASSEMBLY']},
          {description: res['COCINAS.ASSEMBLY_KITCHEN']},
          {description: '0€ - 2.000€'},
          {description: '2.001€ - 3.500€'},
          {description: '3.501€ - 5.000€'}
        ];
        this.compra = 2;
      }
    });


    this._pedidoService.inicializarPedido();
    this._pedidoService.pedido.inZone = this._pedidoService.datosUbicacionPedido.ubicacion.tipo;
    this._pedidoService.pedido.importeCompraFactor = this.compra;
    this._pedidoService.updatePedido();

    console.log('IN ZONE:', this._pedidoService.pedido.inZone);
    console.log(this._pedidoService.datosUbicacionPedido.ubicacion.tipo);

    this.total_sofa = this._centroService.centro.precios.montaje.sofa;
    this.total_sillon = this._centroService.centro.precios.montaje.sillon;
    this.total_complemento = this._centroService.centro.precios.montaje.complemento;

    this.total_sofa_usado = this._centroService.centro.precios.usado.sofa;
    this.total_colchon_usado = this._centroService.centro.precios.usado.colchon;
    this.total_mgrande_usado = this._centroService.centro.precios.usado.mgrande;
    this.total_mpeq_usado = this._centroService.centro.precios.usado.mpeq;

    this.total_medicion = this._centroService.centro.precios.montaje.cocina_med;


  }

  ngOnInit() {

  }

  /**
   * Selector importe de compra
   * @param {number} selectedValue
   * @param event
   */
  changeSelect(selectedValue: number) {
    console.log('change select', selectedValue);
    this.compra = Number(selectedValue);
    this._pedidoService.pedido.importeCompraFactor = this.compra;
    this._pedidoService.opcionSoloMontaje = this.compra === 0;
    if (this._centroService.centro.orderConfiguration.opcion_cocina && this.compra === 1) {
      this.resetAllMontajes();
      this.resetAllUsados();
    } else {
      this.resetAllCocinas();
    }
    this.update();
  }

  update() {
    this._pedidoService.updatePedido();
  }

  cantidadSofaChange(event: any) {
    this._pedidoService.pedido.sofa_cantidad = Number(event);
    this.total_sofa_usado = this._centroService.centro.precios.usado.sofa * Number(event);
    this.update();
  }

  cantidadColchonChange(event) {
    this._pedidoService.pedido.colchon_cantidad = Number(event);
    this.total_colchon_usado = this._centroService.centro.precios.usado.colchon * Number(event);
    this.update();
  }

  cantidadMueblePequeChange(event: any) {
    // console.log(event);
    this._pedidoService.pedido.mpeq_cantidad = Number(event);
    this.total_mpeq_usado = this._centroService.centro.precios.usado.mpeq * Number(event);
    this.update();
  }
  cantidadMuebleGrandeChange(event: any) {
    // console.log(event);
    this._pedidoService.pedido.mgrande_cantidad = Number(event);
    this.total_mgrande_usado = this._centroService.centro.precios.usado.mgrande * Number(event);
    this.update();
  }

  cantidadMontajeComplementoChange(event) {
    console.log(event);
    this._pedidoService.pedido.montajeComplemento_cantidad = Number(event);
    this.total_complemento = this._centroService.centro.precios.montaje.complemento * event;
    this.update();
  }

  cantidadMontajeSofaChange(event) {
    console.log(event);
    this._pedidoService.pedido.montajeSofa_cantidad = Number(event);
    this.total_sofa = this._centroService.centro.precios.montaje.sofa * Number(event);
    this.update();
  }

  cantidadMontajeSillonChange(event) {
    console.log(event);
    this._pedidoService.pedido.montajeSillon_cantidad = Number(event);
    this.total_sillon = this._centroService.centro.precios.montaje.sillon * Number(event);
    this.update();
  }

  /**
   * Al cambiar el check de montaje
   */
  montajeOptionChange() {
    console.log(this._pedidoService.pedido.montajeOption);
    if (!this._pedidoService.pedido.montajeOption) {
      this.resetAllMontajes();
      this.update();
    }
  }

  /**
   * Al cambiar el check de usados
   */
  usadoOptionChange() {
    console.log(this._pedidoService.pedido.reciclajeOption);
    if (!this._pedidoService.pedido.reciclajeOption) {
      this.resetAllUsados();
      this.update();
    }
  }

  /**
   * Resetea los valores del formulario de montajes
   */
  resetAllMontajes() {
    this._pedidoService.pedido.montajeOption = false;
    this.resetSofas();
    this.resetComplementos();
    this.resetOtrosMuebles();
    this.resetPax();
  }

  /**
   * Resetea los valores del formulario de usados
   */
  resetAllUsados() {
    this._pedidoService.pedido.reciclajeOption = false;
    this._pedidoService.pedido.colchon = false;
    this._pedidoService.pedido.sofa = false;
    this._pedidoService.pedido.mpeq = false;
    this._pedidoService.pedido.mgrande = false;
    this.total_mgrande_usado = this._centroService.centro.precios.usado.mgrande;
    this.total_mpeq_usado = this._centroService.centro.precios.usado.mpeq;
    this.total_sofa_usado = this._centroService.centro.precios.usado.sofa;
    this.total_colchon_usado = this._centroService.centro.precios.usado.colchon;

  }

  resetAllCocinas() {
    this._pedidoService.pedido.cocina_medicion = false;
    this._pedidoService.pedido.metros_knoxhult = 0;
    this._pedidoService.pedido.metros_metod = 0;
    this.metros_metod = null;
    this.metros_knoxhult = null;
    this.metros_cocina = null;
    this._pedidoService.pedido.metros_cocina_usada = null;
  }

  /**
   * Resetea los valores del formulario complementos
   */
  resetComplementos() {
    this._pedidoService.pedido.montajeComplemento_cantidad = 1;
    this._pedidoService.pedido.montajeComplemento = false;
    this.total_complemento = this._centroService.centro.precios.montaje.complemento;
  }

  /**
   * Resetea los valores del formulario sofas
   */
  resetSofas() {
    this._pedidoService.pedido.montajeSofa_cantidad = 1;
    this._pedidoService.pedido.montajeSofa = false;
    this.total_sofa = this._centroService.centro.precios.montaje.sofa;

    this._pedidoService.pedido.montajeSillon_cantidad = 1;
    this._pedidoService.pedido.montajeSillon = false;
    this.total_sillon = this._centroService.centro.precios.montaje.sillon;
  }

  /**
   * Resetea los valores del formulario otros muebles
   */
  resetOtrosMuebles() {
    this._pedidoService.pedido.precioMontaje = 0;
    this.mueblesOption = false;
    this.importeMontaje = null;
  }

  /**
   * Resetea los valores del formulario pax
   */
  resetPax() {
    this._pedidoService.pedido.precioPax = 0;
    this.importePax = null;
    this.paxOption = false;
  }

  /**
   * Calcula importe monteje
   * @param event
   */
  calculateMontaje(event) {
    console.log(event);
    const value: number = Number(event.target.value);
    if (value === 0) {
      this._pedidoService.pedido.precioMontaje = 0;
      this.importeMontaje = null;
    } else {
      this._pedidoService.pedido.precioMontaje = value;
      this.importeMontaje = value;
    }
    this.update();
  }

  /**
   * Calcula importe PAX
   * @param event
   */
  calculatePax(event) {
    const value: number = Number(event.target.value);
    if (value === 0) {
      this._pedidoService.pedido.precioPax = 0;
      this.importePax = null;
    } else {
      this._pedidoService.pedido.precioPax = value;
      this.importePax = value;
    }

    this.update();
  }

  calculateMetod(event) {
    let value: number;
    if (event.target.value == null || event.target.value < 0) {
      value = 0;
    } else {
      value = event.target.value;
    }
    this._pedidoService.pedido.metros_metod = value;
    this.update();
  }

  calculateKnoxhult(event) {
    let value: number;
    if (event.target.value == null || event.target.value < 0) {
      value = 0;
    } else {
      value = event.target.value;
    }
    this._pedidoService.pedido.metros_knoxhult = value;
    this.update();
  }

  calculateUsadoCocina(event) {
    let value: number;
    if (event.target.value == null || event.target.value < 0) {
      value = 0;
    } else {
      value = event.target.value;
    }
    this._pedidoService.pedido.metros_cocina_usada = value;
    this.update();
  }

  atras() {
    this.router.navigate(['/mapa']).then();
  }

  siguiente() {
    if (this.importeMontaje === 0 || this.importeMontaje === '' || this.importeMontaje === null) {
      if (this._pedidoService.pedido.montajeSofa === false &&
        this._pedidoService.pedido.montajeSillon === false &&
        this._pedidoService.pedido.montajeComplemento === false) {

        this._pedidoService.pedido.precioMontaje = '';
        this._pedidoService.pedido.montajeOption = false;
        this.importeMontaje = null;
      } else {
        this.importeMontaje = 0;
      }
      this.update();
    }

    if (this._pedidoService.pedido.colchon === false &&
      this._pedidoService.pedido.sofa === false &&
      this._pedidoService.pedido.electrodomestico === false) {
      this._pedidoService.pedido.reciclajeOption = false;
    }

    if (this._pedidoService.datosUbicacionPedido.ubicacion.tipo === 'express') {
      if (this._pedidoService.pedido.montajeOption === true) {
        this._pedidoService.pedido.tipo = 1;
        this.router.navigate(['/transportistas']);
      } else {
        this.router.navigate(['/tipo']);
      }
    } else {
      this._pedidoService.pedido.tipo = 1;
      this.router.navigate(['/transportistas']);
    }
  }
}
