import { Component, OnInit } from '@angular/core';
import {PagesComponent} from '../pages.component';
import {TranslateService} from '@ngx-translate/core';
import {CentroService, VannerService} from '../../services/service.index';
import {Router} from "@angular/router";
declare function initCustom();

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {

  active = 1;
  centros: any;
  selectedCentro: number;
  isActiveCenter: boolean;

  constructor(public pages: PagesComponent,
              public translateService: TranslateService,
              private _centroService: CentroService,
              private _vannerService: VannerService,
              public router: Router) {
    this.translateService.get('HOME.TITLE').subscribe((t: string) => {
      this.pages.title = t;
    });
    this.centros = null;
    this.getCentros();
  }

  ngOnInit() {
    initCustom();
  }

  /**
   * Obtenemos los centros
   */
  getCentros() {
    if (this.centros == null) {
      this._centroService.centrosDisponibles = [];
      const centrosRef = this._centroService.getCentros().subscribe( (centros) => {
        for (const c in centros) {
          this._centroService.centrosDisponibles.push(centros[c]);
          this.centros = this._centroService.centrosDisponibles.reverse();
        }
        centrosRef.unsubscribe();
      });
    }
  }

  /**
   * Función llamada cuando se selecciona un centro
   * @param centro
   */
  itemSelected(centro) {
    console.log(centro);
    this._centroService.centro = centro;
    this.router.navigate(['/mapa']); // TODO
    //this.router.navigate(['/pago']);
  }

  setUrlsUsuarios() {
    this._vannerService.setUrlImagesAllUsuarios();
  }
  setUrlsVanners() {
    this._vannerService.setUrlImagesAllVanners();
  }

}
