import {Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import {PagesComponent} from '../pages.component';
import {TranslateService} from '@ngx-translate/core';
import {AuthService, UsuarioService} from '../../services/service.index';
import {Usuario} from '../../models/usuario.model';
import {NotificationsService} from '../../services/service.index';
import {Notification} from '../../interfaces/notification.interface';
import {PedidoService} from '../../services/service.index';
import {VannerService} from '../../services/service.index';
import {ValoracionesService} from '../../services/service.index';
import {ActivatedRoute} from '@angular/router';
import {validateEmail} from '../../utiles/utiles';
import * as firebase from 'firebase';
@Component({
  selector: 'app-mi-perfil',
  templateUrl: './mi-perfil.component.html',
  styleUrls: ['./mi-perfil.component.css']
})
export class MiPerfilComponent implements OnInit {

  usuario: Usuario;
  currentUser: any;
  avatar: string;
  correo: string;
  model: any;
  errorPhone: boolean;
  errorPhoneCode: boolean;
  updated: boolean;
  updating: boolean;
  valoraciones: any[];
  activeTab: number;
  sending: boolean;
  send: boolean;
  sendVerification: boolean;
  sendingVerification: boolean;

  @ViewChild('notifications') notificationTab: ElementRef;
  constructor(private route: ActivatedRoute,
              public pages: PagesComponent,
              public translateService: TranslateService,
              public _userService: UsuarioService,
              public _authService: AuthService,
              public notificationsService: NotificationsService) {

    this.translateService.get('PROFILE.TITLE').subscribe((t: string) => {
      this.pages.title = t;
    });
    this.activeTab = 0;
    this.sending = false;
    this.send = false;
    this.sendingVerification = false;
    this.sendVerification = false;
    this.currentUser = firebase.auth().currentUser;

    this.route.params.subscribe(params => {
      if (Number(params['id']) > 0) {
        this.activeTab += Number(params['id']);
      }
    });

    this.correo = '';
    this.updated = false;
    this.updating = false;
    this.errorPhone = false;
    this.errorPhoneCode = false;
    this.usuario = this._userService.userProfile;
    this.avatar = 'assets/images/default_avatar.png';
    this.model = {
      nombre: this.usuario.first_name,
      apellidos: this.usuario.last_name,
      phoneNumber : '+' + String(this.usuario.phoneCode.number) + String(this.usuario.phoneNumber),
      email: this.usuario.email
    };
  }

  ngOnInit() {
  }

  /**
   * Actualiza la información de perfil desde el formulario
   */
  actualizar() {
    console.log(this.model.phoneNumber);
    let pn;
    if (this.model.phoneNumber === ('+' + String(this.usuario.phoneCode.number) + String(this.usuario.phoneNumber))) {
      console.log('igual');
      pn = [String(this.usuario.phoneNumber), String(this.usuario.phoneCode.number), String(this.usuario.phoneCode.code)];
    } else {
      pn = this.model.phoneNumber;
    }
    console.log(pn[0]);
    console.log(pn[1]);
    console.log(pn[2]);
    if (pn[0].length <= 0 ) {
      this.errorPhone = true;
      return;
    } else {
      this.errorPhone = false;
    }
    this.updating = true;
    this.usuario.first_name = this.model.nombre;
    this.usuario.last_name = this.model.apellidos;
    this.usuario.email = this.model.email;
    this.usuario.phoneNumber = pn[0];
    this.usuario.phoneCode.code = pn[2].toUpperCase();
    this.usuario.phoneCode.number = Number(pn[1]);

    console.log(this.usuario);
    this._userService.updateProfile().then( () => {
      console.log('updated User data!!');
      this.updated = true;
      setTimeout( () => {
        this.updated = false;
        this.updating = false;
      }, 1000);
    });

  }

  /**
   * Envía email para restablecer la contraseña del usuario
   */
  restablecer() {
    this.sending = true;
    if (validateEmail(this.correo)) {

      this._userService.restablecer(this.correo).then( () => {
        console.log('Email enviado');
        this.send = true;
        setTimeout( () => {
          this.send = false;
          this.sending = false;
        }, 1000);

      }).catch( error =>  {
        console.log('Email error', error);
      });
    } else {
      this.sending = false;
      console.log('Error validación');
    }
  }

  verficarEmail() {
    console.log('Send email verification');
    const self = this;
    this._userService.verficar().then(function() {
      self.sendVerification = true;
      setTimeout( () => {
        self.sendVerification = false;
        self.sendingVerification = false;
        self._authService.logout();
      }, 1000);
    }).catch(function(error) {
      // An error happened.
    });
  }

  cerrarSesion(){
    this._authService.logout();
  }
}
