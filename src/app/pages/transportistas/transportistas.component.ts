import { Component, OnInit } from '@angular/core';
import {PagesComponent} from '../pages.component';
import {TranslateService} from '@ngx-translate/core';
import {CentroService, PedidoService, VannerService} from '../../services/service.index';
import {Router} from '@angular/router';
import {GlobalService} from '../../services/shared/global.service';
import {Location} from '@angular/common';
import * as moment from 'moment';
import {Vanner} from '../../interfaces/vanner.interface';

@Component({
  selector: 'app-transportistas',
  templateUrl: './transportistas.component.html',
  styleUrls: ['./transportistas.component.css']
})
export class TransportistasComponent implements OnInit {

  active: number;
  centro_name: string;
  centro: any;
  vanners: any[];
  rate = 4.3;
  rating = 0;

  // Fechas
  fecha: number;
  fechas: string[];
  ff: any[];
  optionHora: any;
  hora = 0;
  rangoHorasCompletos: string[] = [];
  rangoHora: any;
  showAceptar: boolean;
  hoyMismo = false;
  rangoHoras: string[] = [];
  rango: string[];
  loading: boolean;
  esMontajeCocina: boolean;

  constructor(public pages: PagesComponent,
              public translateService: TranslateService,
              public _centroService: CentroService,
              public _pedidoService: PedidoService,
              public _vannersService: VannerService,
              public _globalService: GlobalService,
              public router: Router,
              public _location: Location) {

    this.translateService.get('TRANSPORTISTAS.TITLE').subscribe((t: string) => {
      this.pages.title = t;
    });


    // Vanners
    this.centro = this._centroService.centro;
    this.centro_name = this.centro.name;
    this.active = 5;

    this.esMontajeCocina = (this._pedidoService.pedido.precioFinalMontajeCocina > 0) ? true : false;

    // Fechas
    moment.locale(this._globalService.language);
    this.optionHora = 'cualquier';
    this._pedidoService.pedido.hoy = false;
    this.hoyMismo = false;
    this._pedidoService.pedido.hoy = this.hoyMismo;
    this.initRangos();
    this.loading = true;
  }

  initRangos() {
    this.translateService.get(['RANGOS.RANGO_0', 'RANGOS.RANGO_1', 'RANGOS.RANGO_2', 'RANGOS.RANGO_3'])
      .subscribe(t => {
        this.rangoHoras = [
          t['RANGOS.RANGO_0'], t['RANGOS.RANGO_1'], t['RANGOS.RANGO_2'], t['RANGOS.RANGO_3']
        ];
        this.rangoHorasCompletos = this.rangoHoras;
        this.rango = this.rangoHoras;
        this.fechas = this.getOpenDeliveryDays();
        this.fecha = 0;
        this.filterVanner(this._pedidoService.pedido.hoy);
      });
  }

  ngOnInit() {
  }

  atras() {
    this._location.back();
  }



  /***************************
   * FECHAS
   ***************************/

  /**
   * Obtenemos los días disponibles para las reservas del calendario del centro
   * @returns {string[]}
   */
  getOpenDeliveryDays() {
    const dates: string[] = [];
    this.ff = [];

    const calendarioCentro = Object.keys(this.centro.closed_calendar).map(function(key) {
      return key;
    });

    const hoy = moment().format( 'DD-MM-YYYY');
    if (calendarioCentro.indexOf(hoy) < 0) {
      const ahora = moment();
      const startTime = moment('10:00', 'HH:mm');
      const endTime = moment('22:00', 'HH:mm');
      const amIBetween = ahora.isBetween(startTime , endTime);

      if (amIBetween) {
        this.translateService.get(['FECHAS.HOY_ANTES']).subscribe(t => {
          dates.push(t['FECHAS.HOY_ANTES']);
        });
        this.hoyMismo = true;
        this._pedidoService.pedido.hoy = this.hoyMismo;
        this.ff.push(hoy);
      } else {
        this.hoyMismo = false;
        this._pedidoService.pedido.hoy = this.hoyMismo;
      }
    }
    let date2: any = moment();
    let date: any = moment();
    let numeroDias = 2;
    if (this._pedidoService.pedido.precioFinalMontajeCocina > 0) {
      // Es cocina
      numeroDias = 7;
    }
    for (let i = 1; i <= numeroDias; i++) {
      let ok = 0;
      let count = 1;
      while (ok < 1) {
        if (i === 1) {
          date = moment().add(count, 'days').format('DD-MM-YYYY');
        } else {
          date = date2.add(1, 'days').format('DD-MM-YYYY');
        }
        if (calendarioCentro.indexOf(date) < 0) {
          ok = 1;
          date2 = moment(date, 'DD-MM-YYYY');
          const f = date2.format('ddd D MMMM');
          dates.push(f);
          this.ff.push(date2.format('DD-MM-YYYY'));
        }
        count++;
      }
    }
    return dates;
  }

  /**
   * Limpia las horas pasadas para el selector
   */
  limpiaHorasPasadas() {

    this.rangoHoras = [];
    const ahora = moment();
    const ma1Start = moment('10:00', 'HH:mm');
    const ma1End = moment('13:00', 'HH:mm');
    const inMan1 = ahora.isBetween(ma1Start , ma1End);
    if (inMan1) {
      console.log('En mañana 1');
      this.translateService.get(['RANGOS.RANGO_0', 'RANGOS.RANGO_1', 'RANGOS.RANGO_2', 'RANGOS.RANGO_3']).subscribe(t => {
        this.rangoHoras = [
          t['RANGOS.RANGO_0'], t['RANGOS.RANGO_1'], t['RANGOS.RANGO_2'], t['RANGOS.RANGO_3']
        ];
      });
    }
    const ma2Start = moment('13:00', 'HH:mm');
    const ma2End = moment('16:00', 'HH:mm');
    const inMan2 = ahora.isBetween(ma2Start , ma2End);
    if (inMan2) {
      console.log('En mañana 2');
      this.translateService.get(['RANGOS.RANGO_0', 'RANGOS.RANGO_1', 'RANGOS.RANGO_2', 'RANGOS.RANGO_3']).subscribe(t => {
        this.rangoHoras = [
          t['RANGOS.RANGO_1'], t['RANGOS.RANGO_2'], t['RANGOS.RANGO_3']
        ];
      });
    }
    const tarde1Start = moment('16:00', 'HH:mm');
    const tarde1End = moment('19:00', 'HH:mm');
    const inTarde1 = ahora.isBetween(tarde1Start , tarde1End);
    if (inTarde1) {
      console.log('En tarde 1');
      this.translateService.get(['RANGOS.RANGO_0', 'RANGOS.RANGO_1', 'RANGOS.RANGO_2', 'RANGOS.RANGO_3']).subscribe(t => {
        this.rangoHoras = [
          t['RANGOS.RANGO_2'], t['RANGOS.RANGO_3']
        ];
      });
    }
    const tarde2Start = moment('19:00', 'HH:mm');
    const tarde2End = moment('22:00', 'HH:mm');
    const inTarde2 = ahora.isBetween(tarde2Start , tarde2End);
    if (inTarde2) {
      console.log('En tarde 2');
      this.translateService.get(['RANGOS.RANGO_0', 'RANGOS.RANGO_1', 'RANGOS.RANGO_2', 'RANGOS.RANGO_3']).subscribe(t => {
        this.rangoHoras = [
          t['RANGOS.RANGO_3']
        ];
      });
    }
  }
  /**
   * Cuando seleccionamos un día en el selector
   * @param event
   */
  changeSelectFecha(e) {
    const event = Number(e);
    if (this.ff.length >= 3) {
      if (event === 0) {
        this.hoyMismo = true;
        this._pedidoService.pedido.hoy = true;
        //this.optionHora = 'hora';
        //this.limpiaHorasPasadas();
      } else {
        this.hoyMismo = false;
        this._pedidoService.pedido.hoy = false;
        this.rangoHoras = this.rangoHorasCompletos;
        this.optionHora = 'cualquier';

      }
    }
    this.filterVanner(this._pedidoService.pedido.hoy);
  }

  applyFechas() {
    if (this.hoyMismo) {
      console.log('Hoy mismo seleccionado');
    } else {
      const hoy = moment().format('e');

      //console.log(hoy);
      let addDias = 1;
      if (Number(hoy) === 5) {
        addDias = 2;
      }
      const fecha = this.ff[this.fecha];
      let h = this.hora;
      if (this.rangoHoras.length === 3) {
        h +=  1;
      } else if (this.rangoHoras.length === 2) {
        h += 2;
      } else if (this.rangoHoras.length === 1) {
        h += 3;
      }

      const fechaReserva = {
        fecha: {
          text: this.fechas[this.fecha],
          value: fecha
        },
        hora: this.optionHora,
        rangoHora: {
          value: h,
          text: this.rango[h],
        }
      };
      console.log(JSON.stringify(fechaReserva)); 1;
      this._pedidoService.pedido.fechaReserva = fechaReserva;
    }
  }

  /**
   * Eventos de selector de horas
   * @param event
   */
  changeSelectHora(event) {
    this.filterVanner(this._pedidoService.pedido.hoy);
  }

  /**
   * Eventos de selector de horas
   * @param event
   */
  changeSelectFranja(event) {
    this.filterVanner(this._pedidoService.pedido.hoy);
  }


  /***************************
   * VANNERS
   ***************************/


  /**
   * Inicializamos los filtros dependiendo de si el pedido es Espress-R / Reservas
   * @param {boolean} tipo
   */
  filterVanner( tipo: boolean) {
    console.log(this.esMontajeCocina);
    if (this.esMontajeCocina) {
      console.log('montaje cocina filter');
      (tipo) ? this.filterVannerCocinasExpressR() : this.filterVannerCocinas();
    } else {
      console.log('reserva hoy:', tipo);
      (tipo) ? this.filterVannerExpressR() : this.filterVannerReserva();
    }

  }

  filterVannerCocinas() {
    this.vanners = [];
    this.loading = true;
    console.log('FILTER VANNER COCINAS');
    return this.getVanners().subscribe((snapshots) => {
      this.vanners = [];
      const van = [];
      let disponible = false;
      const pedido = this._pedidoService.pedido;
      snapshots.forEach( (snapshot: Vanner) => {
        if (snapshot.status === true && snapshot.montador_cocina) {
          if (snapshot.centro === this._centroService.centro.key) {
            let inZone = false;
            for (const zona in snapshot.zonas) {
              if (zona === this._centroService.currentZonaKey) {
                inZone = true;
                break;
              }
            }
            if (inZone) {
              if (pedido.tipo == 1) {
                console.log('Pedido tipo  = 1');
                snapshot.disponible = !snapshot.completo;
                // Calculamos el rating
                snapshot.rating = this.calculateRating(snapshot);
                if (snapshot.rating >= 4.95) {
                  snapshot.ratingshow = 5.0;
                } else if (snapshot.rating >= 4.9 && snapshot.rating < 4.95) {
                  snapshot.ratingshow = 4.9;
                } else {
                  snapshot.ratingshow = snapshot.rating;
                }
                snapshot.supervanner = this.calculateSuper(snapshot);
                snapshot.nuevo = this.calculateNuevo(snapshot);
                snapshot.langs = [];
                //Languages
                if (snapshot.languages != null) {
                  console.log('LANG:', snapshot.languages);
                  for (const l in snapshot.languages) {
                    if (snapshot.languages.hasOwnProperty(l)) {
                      snapshot.langs.push(l.toUpperCase());
                    }
                  }
                } else {
                  snapshot.languages = null;
                }

                van.push(snapshot);
              }
            }
          }
        }
      });

      let filter = [ '-supervanner', '-rating', '-num_opiniones'];
      if (this._pedidoService.pedido.montajeOption == true) {
        filter =  ['-rating', '-supervanner', '-num_opiniones'];
      }
      this.vanners = van.sort(this.dynamicSortMultiple(filter));

      this.vanners.forEach( (snapshot) => {
        const hora = this._pedidoService.pedido.fechaReserva.hora;
        // Obtenemos las reservas del dia de entrega del pedido
        if (snapshot.completo === true) {
          disponible = false;
          snapshot.disponible = disponible;
        } else {
          this._pedidoService.getCurrentReservasOfVanner(snapshot.key).subscribe((rangos) => {

            const rangoDisponible = this.checkDisponibilidad(rangos, hora, snapshot);
            // console.log(JSON.stringify(rangoDisponible));
            disponible = rangoDisponible != null;
            snapshot.disponible = disponible;
          });
        }
      });
      this.loading = false;
    });
  }

  filterVannerCocinasExpressR() {
    this.vanners = [];
    this.loading = true;
    console.log('Express-R Cocinas list');
    const van = [];
    const disponible = true;
    const pedido = this._pedidoService.pedido;

    return this.getVannersExpressR().then( (res) => {
      // console.log('RESULT VANNERS EXPRESS-R', JSON.stringify(res));
      this._vannersService.getVanners().subscribe( (snapshots) => {
        this.vanners = [];
        snapshots.forEach( (snapshot: Vanner) => {
          if (snapshot.status === true && snapshot.montador_cocina) {
            if (snapshot.centro === this._centroService.centro.key) {
              let inZone = false;
              for (const zona in snapshot.zonas) {
                if (zona === this._centroService.currentZonaKey) {
                  inZone = true;
                  break;
                }
              }
              if (inZone) {
                for (const r in res) {
                  if (res.hasOwnProperty(r)) {
                    if (snapshot.key === res[r].key) {
                      // console.log(snapshot.key);
                      snapshot.disponible = !snapshot.completo;
                      snapshot.rating = this.calculateRating(snapshot);
                      if (snapshot.rating >= 4.95 ) {
                        snapshot.ratingshow = 5.0;
                      } else if (snapshot.rating >= 4.9 && snapshot.rating < 4.95) {
                        snapshot.ratingshow = 4.9;
                      } else {
                        snapshot.ratingshow = snapshot.rating;
                      }
                      snapshot.supervanner = this.calculateSuper(snapshot);
                      snapshot.langs = [];
                      //Languages
                      if (snapshot.languages != null) {
                        for (const l in snapshot.languages) {
                          if (snapshot.languages.hasOwnProperty(l)) {
                            snapshot.langs.push(l.toUpperCase());
                          }
                        }
                      }
                      if (pedido.montajeOption == false) {
                        if ( this._pedidoService.opcionSoloMontaje === false) {
                          van.push(snapshot);
                        } else {
                          if (snapshot.montaje === true) {
                            van.push(snapshot);
                          }
                        }
                      } else {
                        van.push(snapshot);
                      }
                    }
                  }
                }
              }
            }
          }
        });

        let filter = [ '-parking', 'montaje', '-supervanner', '-rating', '-num_opiniones'];
        if (this._pedidoService.pedido.montajeOption === true || this._pedidoService.opcionSoloMontaje === true) {
          filter =  ['-parking', '-rating', '-supervanner', '-num_opiniones'];
        }
        this.vanners = van.sort(this.dynamicSortMultiple(filter));
      });
      this.loading = false;
    });
  }


  filterVannerExpressR() {
    this.vanners = [];
    this.loading = true;
    console.log('Express-R list');
    console.log('FILTER VANNER EXPRESS');
    const van = [];
    const disponible = true;
    const pedido = this._pedidoService.pedido;

    return this.getVannersExpressR().then( (res) => {
      // console.log('RESULT VANNERS EXPRESS-R', JSON.stringify(res));
      this._vannersService.getVanners().subscribe( (snapshots) => {
        this.vanners = [];
        snapshots.forEach( (snapshot: Vanner) => {
          if (snapshot.status === true && !snapshot.montador_cocina) {
            if (snapshot.centro === this._centroService.centro.key) {
              let inZone = false;
              for (const zona in snapshot.zonas) {
                if (zona === this._centroService.currentZonaKey) {
                  inZone = true;
                  break;
                }
              }
              if (inZone) {
                for (const r in res) {
                  if (res.hasOwnProperty(r)) {
                    if (snapshot.key === res[r].key) {
                      // console.log(snapshot.key);
                      snapshot.disponible = !snapshot.completo;
                      snapshot.rating = this.calculateRating(snapshot);
                      if (snapshot.rating >= 4.95 ) {
                        snapshot.ratingshow = 5.0;
                      } else if (snapshot.rating >= 4.9 && snapshot.rating < 4.95) {
                        snapshot.ratingshow = 4.9;
                      } else {
                        snapshot.ratingshow = snapshot.rating;
                      }
                      snapshot.supervanner = this.calculateSuper(snapshot);
                      snapshot.langs = [];
                      //Languages
                      if (snapshot.languages != null) {
                        for (const l in snapshot.languages) {
                          if (snapshot.languages.hasOwnProperty(l)) {
                            snapshot.langs.push(l.toUpperCase());
                          }
                        }
                      }
                      if (pedido.montajeOption == false) {
                        if ( this._pedidoService.opcionSoloMontaje === false) {
                          van.push(snapshot);
                        } else {
                          if (snapshot.montaje === true) {
                            van.push(snapshot);
                          }
                        }
                      } else {
                        van.push(snapshot);
                      }
                    }
                  }
                }
              }
            }
          }
        });

        let filter = [ '-parking', 'montaje', '-supervanner', '-rating', '-num_opiniones'];
        if (this._pedidoService.pedido.montajeOption === true || this._pedidoService.opcionSoloMontaje === true) {
          filter =  ['-parking', '-rating', '-supervanner', '-num_opiniones'];
        }
        this.vanners = van.sort(this.dynamicSortMultiple(filter));
      });
      this.loading = false;
    });
  }

  filterVannerReserva(): any {
    this.vanners = [];
    this.loading = true;
    this.applyFechas();
    return this.getVanners().subscribe((snapshots) => {
      const van = [];
      this.vanners = [];
      let disponible = false;
      const pedido = this._pedidoService.pedido;
      snapshots.forEach( (snapshot: Vanner) => {
        if (snapshot.status === true) {
          if (snapshot.centro === this._centroService.centro.key) {
            let inZone = false;
            for (const zona in snapshot.zonas) {
              if (zona === this._centroService.currentZonaKey) {
                inZone = true;
                break;
              }
            }
            if (inZone) {
              if (pedido.tipo === 1) {
                snapshot.rating = this.calculateRating(snapshot);
                if (snapshot.rating >= 4.95) {
                  snapshot.ratingshow = 5.0;
                } else if (snapshot.rating >= 4.9 && snapshot.rating < 4.95) {
                  snapshot.ratingshow = 4.9;
                } else {
                  snapshot.ratingshow = snapshot.rating;
                }
                snapshot.supervanner = this.calculateSuper(snapshot);
                snapshot.nuevo = this.calculateNuevo(snapshot);
                snapshot.langs = [];
                // Languages
                if (snapshot.languages != null) {
                  console.log('LANG:', snapshot.languages);
                  for (const l in snapshot.languages) {
                    if (snapshot.languages.hasOwnProperty(l)) {
                      snapshot.langs.push(l.toUpperCase());
                    }
                  }
                } else {
                  snapshot.languages = null;
                }

                // TODO Comprobar la franja horaria del pedido

                if (pedido.montajeOption === false) {
                  if ( this._pedidoService.opcionSoloMontaje == false) {
                    van.push(snapshot);
                  } else {
                    if (snapshot.montaje == true) {
                      van.push(snapshot);
                    }
                  }
                } else {
                  if (snapshot.montaje === true) {
                    van.push(snapshot);
                  }
                }
              }
            }
          }
        }
      });

      let filter = [ 'montaje', '-supervanner', '-rating', '-num_opiniones'];
      if (this._pedidoService.pedido.montajeOption == true || this._pedidoService.opcionSoloMontaje == true) {
        filter =  ['-rating', '-supervanner', '-num_opiniones'];
      }
      this.vanners = van.sort(this.dynamicSortMultiple(filter));

      this.vanners.forEach( (snapshot) => {
        const hora = this._pedidoService.pedido.fechaReserva.hora;
        // Obtenemos las reservas del dia de entrega del pedido
        if (snapshot.completo === true) {
          disponible = false;
          snapshot.disponible = disponible;
        } else {
          this._pedidoService.getCurrentReservasOfVanner2(snapshot.key).subscribe(( rangos ) => {
            const rangoDisponible = this.checkDisponibilidad(rangos, hora, snapshot);
            disponible = rangoDisponible != null;
            snapshot.disponible = disponible;
          });
        }
      });

      console.log('finalizado');
     this.loading = false;
    });
  }

  /**
   * Obtenemos los vanners para listado de Express-R
   * @returns {Promise<any>}
   */
  getVannersExpressR() {
    return this._vannersService.getVannersExpressR();
  }
  /**
   * Abre el detalle de un vanner
   * @param snapshot
   */
  openVanner(snapshot: any) {
    if (snapshot.disponible === true) {
      console.log(snapshot.key);
      this.router.navigate(['/vannerDetalle', snapshot.key]).then();
    }
  }

  /**
   * Obtenemos los vanners de la base de datos
   * @returns {any}
   */
  getVanners() {
    return this._vannersService.getVanners();
  }
  /**
   * Calculamos el rating del vanner
   * @param vanner
   * @returns {number}
   */
  calculateRating(vanner: any): number {
    if (vanner.num_opiniones > 0) {
      this.rating = ((vanner.rating_calidad +  vanner.rating_entrega + vanner.rating_montaje + vanner.rating_limpieza) / vanner.num_opiniones) / 4;
    } else {
      this.rating = 0;
    }
    return this.rating;
  }

  /**
   * Calcula si un vanner es SuperVanner
   * @param vanner
   * @returns {boolean}
   */
  calculateSuper(vanner: any): boolean {
    if (vanner.num_opiniones >= 20 && vanner.entregas >= 40 ) {
      const r = ((vanner.rating_calidad +  vanner.rating_entrega + vanner.rating_montaje + vanner.rating_limpieza) / vanner.num_opiniones) / 4;
      return r >= 4.95;
    }
    return false;
  }

  /**
   * Calcula si el vanner es nuevo
   * @param vanner
   * @returns {boolean}
   */
  calculateNuevo( vanner: any ) {
    return vanner.entregas < 40;
  }

  /**
   * Utilidad para ordenar un array por varios criterios
   * @param arg
   * @returns {(obj1, obj2) => number}
   */
  dynamicSortMultiple(arg) {
    function dynamicSort(property) {
      let sortOrder = 1;
      if (property[0] === '-') {
        sortOrder = -1;
        property = property.substr(1);
      }
      return function (a, b) {
        const result = (a[property] < b[property]) ? -1 : (a[property] > b[property]) ? 1 : 0;
        return result * sortOrder;
      };
    }
    const props = arg;
    return function (obj1, obj2) {
      let i = 0, result = 0;
      const numberOfProperties = props.length;
      while (result === 0 && i < numberOfProperties) {
        result = dynamicSort(props[i])(obj1, obj2);
        i++;
      }
      return result;
    };
  }

  checkDisponibilidad(rangos: any, tipo: any, vanner: any): any {
    const rango = this._pedidoService.pedido.fechaReserva.rangoHora.value;
    const fecha = this._pedidoService.pedido.fechaReserva.fecha.value;
    if (tipo === 'hora') {
      let key = rango;
      for (const r of rangos) {
        if (r.key === String(rango)) {
          key = null;
          break;
        }
      }
      if (key == null) {
        return null;
      } else {
        // Esta disponible reserva
        // Comprobamos calendario
        if ('calendar' in vanner) {
          const calendario = vanner.calendar;
          if (calendario[fecha] != null) {
            if (calendario[fecha][rango] != null) {
              return null;
            } else {
              return rango;
            }
          } else {
            return rango;
          }

        } else {
          return rango;
        }

      }
    } else if (tipo === 'mañana') {
      let key = 0;
      let reservado: any[];
      reservado = [];
      rangos.forEach( (rang) => {
        if (rang.key <= 1) {
          reservado.push(Number(rang.key));
        }
      });
      if (reservado.length === 2) {
        // Ocupado
        key = null;
      } else {
        if ('calendar' in vanner) {
          if (reservado.length === 0) {
            const calendario = vanner.calendar;
            const fecha1 = this._pedidoService.pedido.fechaReserva.fecha.value;
            if (calendario[fecha1] != null) {
              // Sin reservas comprobamos pero con calendario
              if (calendario[fecha1][0] != null) {
                if (calendario[fecha1][1] != null) {
                  // Mañana ocupada
                  key = null;
                } else {
                  key = 1;
                }
              } else {
                key = 0;
              }
            } else {
              key = 0;
            }
          } else if (reservado.length === 1) {
            rangos.forEach( (rang) => {
              if (rang.key === 0) { key = 1; } else if (rang.key === 1) { key = 0; }
              const calendario = vanner.calendar;
              const fecha2 = this._pedidoService.pedido.fechaReserva.fecha.value;
              if (calendario[fecha2] != null && calendario[fecha2][key] != null) {
                key = null;
              }
            });
          }
        } else {
          // No existe calendario
          // Cogemos el primero libre que no esta reservado
          if (reservado.length === 0) { key = 0; } else if (reservado.length === 1) {
            rangos.forEach( (rang) => {
              if (rang.key === 0) {
                key = 1;
              } else if (rang.key === 1) {
                key = 0;
              }
            });
          } else {
            key = 0;
          }
        }
      }
      return key;

    } else if (tipo === 'tarde') {
      let key = 2;
      let reservado: any[];
      reservado = [];
      rangos.forEach( (rang) => {
        if (rang.key >= 2) { reservado.push(Number(rang.key)); }
      });
      if (reservado.length === 2) {
        // Ocupado
        key = null;
      } else {
        if ('calendar' in vanner) {
          if (reservado.length === 0) {
            const calendario = vanner.calendar;
            const fecha3 = this._pedidoService.pedido.fechaReserva.fecha.value;
            if (calendario[fecha3] != null) {
              // Sin reservas comprobamos pero con calendario
              if (calendario[fecha3][2] != null) {
                if (calendario[fecha3][3] != null) {
                  // Tarde ocupada
                  key = null;
                } else {
                  key = 3;
                }
              } else {
                key = 2;
              }
            } else {
              key = 2;
            }
          } else if (reservado.length === 1) {
            rangos.forEach( (rang) => {
              if (rang.key === 2) {
                key = 3;
              } else if (rang.key === 3) {
                key = 2;
              }
              const calendario = vanner.calendar;
              const fecha4 = this._pedidoService.pedido.fechaReserva.fecha.value;
              if (calendario[fecha4] != null) {
                if (calendario[fecha4][key] != null) {
                  key = null;
                }
              }
            });
          }
        } else {
          // No existe calendario
          // Cogemos el primero libre que no esta reservado
          if (reservado.length === 0) {
            key = 2;
          } else if (reservado.length === 1) {
            rangos.forEach( (rang) => {
              if (rang.key === 2) {
                key = 3;
              } else if (rang.key === 3) {
                key = 2;
              }
            });
          } else {
            key = 2;
          }
        }
      }
      return key;

    } else if (tipo === 'cualquier') {

      let key = null;
      if (rangos.length > 0) {
        if (rangos.length === 4) {
          key = null;
          return key;
          // Comprobamos calendario aqui
        } else {
          const keys = [];
          for (const r of rangos) {

            keys.push(r.key);
          }
          if (keys.indexOf('0') >= 0) {
          } else {
            key = 0;
            if ('calendar' in vanner) {
              const calendario = vanner.calendar;
              const fecha5 = this._pedidoService.pedido.fechaReserva.fecha.value;
              if (calendario[fecha5] != null) {
                if (calendario[fecha5][key] == null) {
                  return key;
                }
              } else {
                return key;
              }
            } else {
              return key;
            }
          }
          if (keys.indexOf('1') >= 0) {
          } else {
            key = 1;
            if ('calendar' in vanner) {
              const calendario = vanner.calendar;
              const fecha6 = this._pedidoService.pedido.fechaReserva.fecha.value;
              if (calendario[fecha6] != null) {
                if (calendario[fecha6][key] == null) {
                  return key;
                }
              } else {
                return key;
              }
            } else {
              return key;
            }
          }

          if (keys.indexOf('2') >= 0) {
          } else {
            key = 2;
            if ('calendar' in vanner) {
              const calendario = vanner.calendar;
              const fecha7 = this._pedidoService.pedido.fechaReserva.fecha.value;
              if (calendario[fecha7] != null) {
                if (calendario[fecha7][key] == null) {
                  return key;
                }
              } else {
                return key;
              }
            } else {
              return key;
            }
          }

          if (keys.indexOf('3') >= 0) {
          } else {
            key = 3;
            if ('calendar' in vanner) {
              const calendario = vanner.calendar;
              const fecha8 = this._pedidoService.pedido.fechaReserva.fecha.value;
              if (calendario[fecha8] != null) {
                if (calendario[fecha8][key] == null) {
                  return key;
                }
              } else {
                return key;
              }
            } else {
              return key;
            }
          }
          key = null;
          return key;
        }

      } else {
        // Comprobamos con calendario vanner
        if ('calendar' in vanner) {
          const calendario = vanner.calendar;
          const fecha9 = this._pedidoService.pedido.fechaReserva.fecha.value;
          if (calendario[fecha9] != null) {
            if (calendario[fecha9][0] == null) {
              return 0;
            } else if (calendario[fecha9][1] == null) {
              return 1;
            } else if (calendario[fecha9][2] == null) {
              return 2;
            } else if (calendario[fecha9][3] == null) {
              return 3;
            } else {
              return null;
            }
          } else {
            return 0;
          }
        } else {
          return 0;
        }
      }
    }
  }

}
