import { Component, OnInit } from '@angular/core';
import {AuthService} from '../../services/service.index';
import {Router} from '@angular/router';
import {TranslateService} from '@ngx-translate/core';
import {GlobalService} from '../../services/shared/global.service';
import {UsuarioService} from '../../services/usuario/usuario.service';
import {Observable} from 'rxjs/Observable';
import {observable} from 'rxjs/symbol/observable';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styles: []
})
export class HeaderComponent implements OnInit {

  iconVisible: boolean;
  currentIdioma: string;
  userProfile: any;
  avatar: string;
  constructor( public authService: AuthService,
               public router: Router,
               private translate: TranslateService,
               private globalService: GlobalService,
               public userService: UsuarioService) {
    this.iconVisible = false;
    this.userProfile = null;
    this.currentIdioma = this.globalService.language || 'es';
  }

  ngOnInit() {
    console.log('ngOninit');
  }

  logout() {
    this.authService.logout();
   // this.router.navigate(['/login']);
  }

  showIconLogo() {
    console.log('visible icon');
    this.iconVisible = !this.iconVisible;
  }

  setIdioma(code) {
    this.translate.setDefaultLang(code);
    this.globalService.language = code;
    localStorage.setItem('language', code);
    this.currentIdioma = code;
  }

}
