import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'app-precio-bar',
  templateUrl: './precio-bar.component.html',
  styleUrls: ['./precio-bar.component.css']
})
export class PrecioBarComponent implements OnInit {

  @Input('precio') precio: number;
  @Input('title') title: string;
  constructor() {
    this.precio = 0;
    this.title = 'Titulo';
  }

  ngOnInit() {
  }

}
