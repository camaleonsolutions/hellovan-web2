import { Component, OnInit } from '@angular/core';
import {AuthService} from "../../services/auth/auth.service";
import {UsuarioService} from "../../services/usuario/usuario.service";
import {ChatService} from "../../services/chat.service";
import {NotificationsService} from "../../services/notifications/notifications.service";
import {PedidoService} from "../../services/pedido/pedido.service";

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styles: []
})
export class SidebarComponent implements OnInit {

  constructor( private authService: AuthService,
               public userService: UsuarioService,
               public chatService: ChatService,
               public notificationsService: NotificationsService,
               public pedidosService: PedidoService
  ) {

  }

  ngOnInit() {
  }

  logout(){
    this.authService.logout();
  }

}
