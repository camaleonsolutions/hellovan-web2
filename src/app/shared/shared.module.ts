import {NgModule} from "@angular/core";
import {RouterModule} from "@angular/router";
import {CommonModule} from "@angular/common";

import {PagenotfoundComponent} from "../pages/pagenotfound/pagenotfound.component";
import {HeaderComponent} from "./header/header.component";
import {SidebarComponent} from "./sidebar/sidebar.component";
import {BreadcrumbsComponent} from "./breadcrumbs/breadcrumbs.component";
import {FooterComponent} from "./footer/footer.component";
import {TranslateModule} from "@ngx-translate/core";
import {StepsComponent} from "./steps/steps.component";
import {PrecioBarComponent} from "./precio-bar/precio-bar.component";
import {NamePipe} from "../pipes/name.pipe";
import {FileUrlPipe} from "../pipes/file-url.pipe";
import {MomentRelativePipe} from "../pipes/moment-relative.pipe";
import {ChatDatePipe} from "../pipes/chat-date.pipe";
import {EllipsisPipe} from "../pipes/ellipsis.pipe";

@NgModule({
  imports: [
    RouterModule,
    CommonModule,
    TranslateModule,
  ],
  declarations: [
    PagenotfoundComponent,
    HeaderComponent,
    SidebarComponent,
    BreadcrumbsComponent,
    FooterComponent,
    StepsComponent,
    PrecioBarComponent,
    NamePipe,
    FileUrlPipe,
    MomentRelativePipe,
    ChatDatePipe,
    EllipsisPipe,

  ],
  exports: [
    PagenotfoundComponent,
    HeaderComponent,
    SidebarComponent,
    BreadcrumbsComponent,
    FooterComponent,
    StepsComponent,
    PrecioBarComponent,
    NamePipe,
    FileUrlPipe,
    MomentRelativePipe,
    ChatDatePipe,
    EllipsisPipe

  ]
})
export class SharedModule { }
