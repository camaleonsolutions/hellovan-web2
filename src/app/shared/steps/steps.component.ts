import {Component, Input, OnChanges, OnInit, SimpleChange} from '@angular/core';

@Component({
  selector: 'app-steps',
  templateUrl: './steps.component.html',
  styleUrls: ['./steps.component.css']
})
export class StepsComponent implements OnInit, OnChanges {

  @Input() active: number;

  subtitle: string;

  selected1: boolean;
  selected2: boolean;
  selected3: boolean;
  selected4: boolean;
  selected5: boolean;
  selected6: boolean;
  completed1: boolean;
  completed2: boolean;
  completed3: boolean;
  completed4: boolean;
  completed5: boolean;
  completed6: boolean;

  constructor() {
    this.active = 1;
    this.selected1 = true;
    this.selected2 = false;
    this.selected3 = false;
    this.selected4 = false;
    this.selected5 = false;
    this.selected6 = false;
    this.completed1 = false;
    this.completed2 = false;
    this.completed3 = false;
    this.completed4 = false;
    this.completed5 = false;
    this.completed6 = false;
  }

  ngOnChanges(changes: {[propKey: string]: SimpleChange}) {
    for (let propName in changes) {
      let changedProp = changes[propName];
      let to = JSON.stringify(changedProp.currentValue);
      if (changedProp.isFirstChange()) {
        console.log(`Initial value of ${propName} set to ${to}`);
      } else {
        let from = JSON.stringify(changedProp.previousValue);
        console.log(`${propName} changed from ${from} to ${to}`);
      }
    }
    this.setStep(this.active);
  }

  ngOnInit() {
    console.log(this.active);
    this.setStep(this.active);
  }

  setStep(step){
    this.selected1 = false;
    this.selected2 = false;
    this.selected3 = false;
    this.selected4 = false;
    this.selected5 = false;
    this.selected6 = false;
    this.completed1 = false;
    this.completed2 = false;
    this.completed3 = false;
    this.completed4 = false;
    this.completed5 = false;
    this.completed6 = false;

    switch (step) {
      case 1:
        this.selected1 = true;
        this.subtitle = 'Seleccionar un establecimiento';
        break;
      case 2:
        this.selected2 = true;
        this.completed1 = true;

        this.subtitle = 'Seleccionar la dirección de entrega en el mapa';
        break;
      case 3:
        this.selected3 = true;
        this.completed1 = true;
        this.completed2 = true;

        this.subtitle = 'Seleccionar las opciones del ticket de compra';
        break;
      case 4:
        this.selected4 = true;
        this.completed1 = true;
        this.completed2 = true;
        this.completed3 = true;

        this.subtitle = 'Seleccionar el tipo de entrega';
        break;
      case 5:
        this.selected5 = true;
        this.completed1 = true;
        this.completed2 = true;
        this.completed3 = true;
        this.completed4 = true;
        break;
      case 6:
        this.selected6 = true;
        this.completed1 = true;
        this.completed2 = true;
        this.completed3 = true;
        this.completed4 = true;
        this.completed5 = true;
        break;
    }
  }

}
