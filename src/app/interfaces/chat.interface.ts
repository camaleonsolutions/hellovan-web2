export interface Chat {
  date?:number,
  last_message_uid?: string,
  last_message?:any,
  datos_usuario?:any,
  datos_vanner?:any,
  user?: string,
  vanner?:string,
  pending?:number,
  key?:string
  user_avatar?:string;
  vanner_avatar?:string;
}
