export interface Notification {
  tipo?:string,
  ref?: string,
  id_pedido?: string,
  id_vanner?:string,
  date?: string,
  key?: string,
  title?:string,
  message?:string,
  usuario?:any
}
