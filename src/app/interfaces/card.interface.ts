export interface Card {
  number:string,
  token: string,
  name: string,
  type: string,
}
