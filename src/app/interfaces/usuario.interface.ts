
export interface Usuario {
  name?: string;
  first_name?: string;
  last_name?: string;
  gender?: string;
  locale?: string;
  email?: string;
  phoneNumber?: string;
  photoUrl?: string;
  photo?: string;
  photo_url?:string;
  facebook_id?: string;
  provider?: string;
  emailVerified?: boolean;
  key?: string;
  push?: any;
  device?: any;
  phoneCode?: any;

}
