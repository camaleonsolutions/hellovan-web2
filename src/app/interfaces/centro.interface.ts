export interface Centro {
  active?: boolean;
  closed_calendar?: any;
  closed_express_calendar?: any;
  company?: string;
  counter?: number;
  default_closing?: number;
  default_opening?: number;
  key?: string;
  lat?: number;
  lng?: number;
  name?: string;
  openinghours?: any;
  postalcodes?: any;
  ubicacion?: any;
  zone?: any;
  zonas?: any;
  code?: any;
  orderConfiguration?: any;
  precios?: {
    montaje: any,
    usado: any
  };
}
