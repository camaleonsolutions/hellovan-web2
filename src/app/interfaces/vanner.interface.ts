
export interface Vanner {
  status?: boolean;
  tipo: number;
  rating: number;
  ratingshow?: number;
  avatar?: string;
  avatar_hi?: string;
  avatar_url?: string;
  van: any;
  languages?: any;
  langs?: any;
  montaje: boolean;
  disponible?: boolean;
  supervanner?: boolean;
  centro?: any;
  zonas?: any;
  nuevo?: boolean;
  key?: string;
  montador_cocina?: boolean;
  completo?: boolean;
  first_name?: string;
  last_name?: string;
  calendar?: any;
}
