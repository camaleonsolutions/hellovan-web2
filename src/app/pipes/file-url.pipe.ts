import {Inject, Pipe, PipeTransform} from '@angular/core';
import {FirebaseApp} from 'angularfire2';
import * as firebase from 'firebase';
@Pipe({
  name: 'fileUrl'
})
export class FileUrlPipe implements PipeTransform {

  constructor (@Inject(FirebaseApp) private fb: firebase.app.App ) {}

  transform(value: string, ...args): any {
    let folder = 'vanners';
    let defecto = 'assets/images/default_avatar.png';
    if (args[0] === 'avatar') {
      folder = 'avatars';
    }
    return this.fb.storage().ref().child(`${folder}/${value}`).getDownloadURL()
      .then(url => {
        return url;
      })
      .catch((error) => {
        return defecto;
      });
  }

}
