import { Pipe, PipeTransform } from '@angular/core';
import * as moment from 'moment';
import 'moment/locale/es';

@Pipe({
  name: 'momentRelative'
})
export class MomentRelativePipe implements PipeTransform {

  transform(value: string, args?: any): any {
    let fecha = moment(value).startOf('day').fromNow();

    return fecha;
  }

}
