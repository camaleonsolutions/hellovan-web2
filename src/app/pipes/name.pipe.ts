import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'name'
})
export class NamePipe implements PipeTransform {

  transform(value: any, args?: any): any {
    let usuario:any = value;
    let first_name = usuario.first_name;
    let last_name:string = usuario.last_name;
    let initial:string = String(last_name).charAt(0);
    return `${ first_name} ${initial}.`;
  }

}
