// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

export const environment = {
  production: false,
  app_version: '1.3.33',
  // Stripe
  stripeServer: 'https://www.hellovan.es/api/',
  url_charge : 'charge_v2_test.php',
  stripePublicKey : 'pk_test_NaE90fKI3tH17QPGgWGxEy8e',

  protocol: 'localhost',

  firebase: {
    apiKey: 'AIzaSyD3KTDUUdzRTXimO5-SF8mwkrm8avLwDn4',
    authDomain: 'hellovan-test.firebaseapp.com',
    databaseURL: 'https://hellovan-test.firebaseio.com',
    projectId: 'hellovan-test',
    storageBucket: 'hellovan-test.appspot.com',
    messagingSenderId: '411956115018'
  }
};
