export const environment = {
  production: true,
  app_version: '1.3.33',

  // Stripe
  stripeServer: 'https://www.hellovan.es/api/',
  url_charge : 'charge_v3.php',
  stripePublicKey : 'pk_live_5ilpHghhEZyNPyrYW0K1hRSL',

  protocol: 'https',

  firebase: {
    apiKey: 'AIzaSyDP33R47kd3oRrISwQrsunhk3vt8-yAszU',
    authDomain: 'hellovan-7be7a.firebaseapp.com',
    databaseURL: 'https://hellovan-7be7a.firebaseio.com',
    projectId: 'hellovan-7be7a',
    storageBucket: 'hellovan-7be7a.appspot.com',
    messagingSenderId: '416189418032'
  }

};
